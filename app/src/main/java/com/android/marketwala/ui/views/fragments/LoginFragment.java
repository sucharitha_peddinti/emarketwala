package com.android.marketwala.ui.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentLoginBinding;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.models.UserResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;

public class LoginFragment extends Fragment implements AppListener {

    FragmentLoginBinding binding;
    AppViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        View view = binding.getRoot();

        binding.createTVID.setOnClickListener(v -> FragmentUtilities.replaceFragment(getActivity(), new RegisterFragment(), AppConstants.SCREEN_REGISTER));

        binding.forgotTVID.setOnClickListener(v -> FragmentUtilities.replaceFragment(getActivity(), new ForgotPasswordFragment(), AppConstants.SCREEN_REGISTER));

        binding.closeIMVID.setOnClickListener(v -> getActivity().onBackPressed());

        binding.loginBtn.setOnClickListener(v -> {
            if (Utilities.isNetworkAvailable(getActivity())) {
                validations();
            } else {
                Utilities.noInternet(getActivity());
            }
        });

        return view;
    }

    private void validations() {
        String name = binding.nameETID.getText().toString();
        String password = binding.passwordETID.getText().toString();

        if (TextUtils.isEmpty(name)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter username email-id or password");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter password");
            return;
        }

        UserResponse userResponse = new UserResponse();
        userResponse.setApi_key(AppConstants.API_TOKEN);
        userResponse.setUsername(name);
        userResponse.setPswrd(password);
        callApi(userResponse);
    }

    private void callApi(UserResponse userResponse) {
        viewModel = new AppViewModel(this);
        viewModel.registration(userResponse, "0");
        viewModel.getUserResponseLiveData().observe(getActivity(), registrationResponse -> {

            if (registrationResponse.isStatus()) {
                UserLoginData loginData = registrationResponse.getResponse();
                Utilities.saveUserData(getActivity(), loginData);
                MySharedPreferences.setPreference(getActivity(), AppConstants.LOGGED_IN_STATUS, AppConstants.LOGGED_IN_VALUE);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();

            } else {
                Utilities.customSnackBar(getActivity(), binding.mainRLID, registrationResponse.getMessage());
            }
        });
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Utilities.showSimpleProgressDialog(getActivity(), null, loadingMsg, false);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Utilities.removeSimpleProgressDialog();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {
        // nothing here
    }
}
