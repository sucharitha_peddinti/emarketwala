package com.android.marketwala.ui.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductsResponse implements Serializable{

    /* "Status": true,
    "Message": "Success",
    "Response": [
        {
            "product_id": "15",
            "product_num": "P6866",
            "product_name": "Awallaki Pressed",
            "short_description": null,
            "product_image": "https://emarketwala.com/uploads/products/1628495738R.jpg",
            "quanty": "250gms",
            "weight": "250 gms",
            "total_price": "50",
            "offer_price": "40",
            "offer_percentage": "20"*/

    private String api_key;
    private String subcat_id;
    private String search_word;


    private boolean Status;
    private String Message;
    private ArrayList<ProductsData> Response = new ArrayList<>();

    public String getSearch_word() {
        return search_word;
    }

    public void setSearch_word(String search_word) {
        this.search_word = search_word;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }


    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<ProductsData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<ProductsData> response) {
        Response = response;
    }

    public static class ProductsData implements Serializable {
        private String product_id;
        private String product_num;
        private String product_name;
        private String short_description;
        private String product_image;
        private String quanty;
        private String weight;
        private int total_price;
        private int offer_price;
//        private int final_amount;
        private String offer_percentage;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_num() {
            return product_num;
        }

        public void setProduct_num(String product_num) {
            this.product_num = product_num;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getQuanty() {
            return quanty;
        }

        public void setQuanty(String quanty) {
            this.quanty = quanty;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public int getTotal_price() {
            return total_price;
        }

        public void setTotal_price(int total_price) {
            this.total_price = total_price;
        }

        public int getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(int offer_price) {
            this.offer_price = offer_price;
        }

        public String getOffer_percentage() {
            return offer_percentage;
        }

        public void setOffer_percentage(String offer_percentage) {
            this.offer_percentage = offer_percentage;
        }

//        public int getFinal_amount() {
//            return final_amount;
//        }
//
//        public void setFinal_amount(int final_amount) {
//            this.final_amount = final_amount;
//        }
    }
}
