package com.android.marketwala.ui.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Utilities.startAnimation(this);

        final Handler handler = new Handler();
        handler.postDelayed(this::LaunchApp, 10);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utilities.finishAnimation(this);
    }


    public void LaunchApp() {

        new Handler().postDelayed(
                () -> {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();

                },
                100
        );
    }

}