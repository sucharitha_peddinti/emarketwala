package com.android.marketwala.ui.views.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentHomeBinding;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.HomeSlidersResponse;
import com.android.marketwala.ui.models.OffersResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.HomeMainBannersAdapter;
import com.android.marketwala.ui.views.adapters.ItemsAdapter;
import com.android.marketwala.ui.views.adapters.OffersAdapter;
import com.android.marketwala.ui.views.adapters.TopCategoriesAdapter;
import com.android.marketwala.ui.views.adapters.TypesAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements AppListener, HomeMainBannersAdapter.BannersClickListener, TypesAdapter.TypesClick, TopCategoriesAdapter.TypesClick, ItemsAdapter.ItemClick, OffersAdapter.OfferClick {

    FragmentHomeBinding binding;
    AppViewModel viewModel;
    HomeMainBannersAdapter adapter;
    HomeSlidersResponse response;

    @Override
    public void onAttach(@NonNull Context context) {
        try {


            super.onAttach(context);
            AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
            new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View view = binding.getRoot();
        MainActivity.isFood = false;
        viewModel = new AppViewModel(this);
        response = new HomeSlidersResponse();
        response.setApi_key(AppConstants.API_TOKEN);

        callData();

        return view;
    }


    private void callData() {

        if (Utilities.isNetworkAvailable(getActivity())) {
            loadHomeData();
        } else {
            Utilities.noInternet(getActivity());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    private void loadHomeData() {
        viewModel.homeSliders(response);
        viewModel.getHomeSlidersResponse().observe(getActivity(), homeSlidersResponse -> {
            if (homeSlidersResponse.isStatus()) {
                ArrayList<HomeSlidersResponse.SlidersData> slidersData = homeSlidersResponse.getResponse();
                if (slidersData.size() > 0) {
                    adapter = new HomeMainBannersAdapter(slidersData, this);
                    binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                    binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                    binding.imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                    binding.imageSlider.setIndicatorSelectedColor(Color.WHITE);
                    binding.imageSlider.setIndicatorUnselectedColor(Color.GRAY);
                    binding.imageSlider.setScrollTimeInSec(3);
                    binding.imageSlider.setAutoCycle(true);
                    binding.imageSlider.startAutoCycle();
                    binding.imageSlider.setSliderAdapter(adapter);
                }
            }
        });


        viewModel.homeCategoriesResponse(response);
        viewModel.getHomeCategoriesResponseLiveData().observe(getActivity(), homeCategoriesResponse -> {

            if (homeCategoriesResponse != null) {
                if (homeCategoriesResponse.isStatus()) {
                    ArrayList<HomeCategoriesResponse.Categories> categories = homeCategoriesResponse.getResponse();
                    TypesAdapter adapter = new TypesAdapter(categories, this);
                    binding.typesRCID.setAdapter(adapter);
                    TopCategoriesAdapter categoriesAdapter = new TopCategoriesAdapter(getActivity(), categories, this);
                    binding.topCateRCID.setAdapter(categoriesAdapter);
                }
            }
        });

        viewModel.homeOffers(response);
        viewModel.getOffersResponseLiveData().observe(getActivity(), offersResponse -> {
            if (offersResponse.isStatus()) {
                ArrayList<OffersResponse.OffersData> offersData = offersResponse.getResponse();
                if (offersData.size() > 0) {
                    OffersAdapter adapter = new OffersAdapter(offersData, this);
                    binding.offersRCID.setAdapter(adapter);
                }
            }
        });
    }


    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        binding.mainCLID.setVisibility(View.GONE);
        binding.progresshome.setVisibility(View.VISIBLE);
        //binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        binding.mainCLID.setVisibility(View.VISIBLE);
        binding.progresshome.setVisibility(View.GONE);
        // binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Utilities.showToast(getActivity(), msg);
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onBannersClickListener(HomeSlidersResponse.SlidersData slidersData) {

    }

    @Override
    public void onCategoryItemClick(HomeCategoriesResponse.Categories categoriesData) {

        SubCategoriesFragment fragment = new SubCategoriesFragment().getData(categoriesData.getCategory_id());
        FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_SUB_CATEGORIES);
    }


    @Override
    public void onResume() {
        super.onResume();
        //nothing do it
        //callData();
    }

    @Override
    public void onTopCategoryItemClick(HomeCategoriesResponse.Categories categoriesData) {
        ItemsAdapter adapter = new ItemsAdapter(categoriesData.getProducts(), this);
        binding.itemsRCID.setAdapter(adapter);
    }

    @Override
    public void onItemClick(HomeCategoriesResponse.Products itemsData) {
        ProductDetailsFragment fragment = new ProductDetailsFragment().getData(itemsData.getProduct_id(), true);
        FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_PRODUCT_DETAILS);
    }

    @Override
    public void onOfferItemClick(OffersResponse.OffersData typesData) {

    }
}
