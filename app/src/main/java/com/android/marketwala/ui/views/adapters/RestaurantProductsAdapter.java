package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.RestProductsListItemBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.models.RestaurantItemsResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class RestaurantProductsAdapter extends RecyclerView.Adapter<RestaurantProductsAdapter.ProductsVH> {

    ArrayList<RestaurantItemsResponse.RestaurantsData> itemsData = new ArrayList<>();

    ProductItemClick click;
    List<CartItems> cartData;

    public RestaurantProductsAdapter(ArrayList<RestaurantItemsResponse.RestaurantsData> itemsData, ProductItemClick click, List<CartItems> cartData) {
        this.itemsData = itemsData;
        this.click = click;
        this.cartData = cartData;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rest_products_list_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        RestaurantItemsResponse.RestaurantsData data = itemsData.get(position);


        for (int j = 0; j < cartData.size(); j++) {
            if (cartData.get(j).getProduct_id().equals(data.getItem_id())) {
                holder.binding.cartQty.setText("" + cartData.get(j).getCartQty());
                holder.binding.addToCartBtn.setText("Update");
            } else {
                holder.binding.addToCartBtn.setText("Add");
            }
        }


        String offer = itemsData.get(position).getFull_price_offer();
        if (offer != null && !offer.equals("")) {
            holder.binding.itemQtyTVID.setVisibility(View.VISIBLE);
        } else {
            holder.binding.itemQtyTVID.setVisibility(View.GONE);
        }
        final int[] cartQty = {Integer.parseInt(holder.binding.cartQty.getText().toString())};
        Log.e("item_cart_qty==>", "" + cartQty[0]);


        holder.binding.getRoot().setOnClickListener(v -> click.onProductItemClick(itemsData.get(position)));
        holder.binding.itemTitleTVID.setText(data.getItem_name());
        holder.binding.itemQtyTVID.setText(" \u20b9 " + data.getFull_price());
        holder.binding.itemPriceTVID.setText("\u20b9 " + data.getFull_price_offer());
        Utilities.strikeText(holder.binding.itemQtyTVID);


        Glide.with(holder.binding.productImageID.getContext())
                .load(data.getItem_image())
                .placeholder(R.drawable.logo_green)
                .into(holder.binding.productImageID);


        holder.binding.cartIncBtn.setOnClickListener(v -> {
            cartQty[0]++;
            holder.binding.cartQty.setText("" + cartQty[0]);
        });

        holder.binding.cartDecBtn.setOnClickListener(v -> {
            if (cartQty[0] > 0) {
                cartQty[0]--;
            }
            holder.binding.cartQty.setText("" + cartQty[0]);
        });

        holder.binding.addToCartBtn.setOnClickListener(v -> {
            String cartsQty = holder.binding.cartQty.getText().toString();
            if (cartsQty.equals("0")) {
                Utilities.showToast((Activity) holder.binding.cartQty.getContext(), "Select Quantity");
            } else {
                click.onAddToCartClicked(data, cartsQty);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public interface ProductItemClick {
        void onProductItemClick(RestaurantItemsResponse.RestaurantsData itemsData);

        void onAddToCartClicked(RestaurantItemsResponse.RestaurantsData itemsData, String qty);
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        RestProductsListItemBinding binding;

        public ProductsVH(@NonNull RestProductsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
