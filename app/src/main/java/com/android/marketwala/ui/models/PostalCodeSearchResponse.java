package com.android.marketwala.ui.models;

public class PostalCodeSearchResponse {

    /*"api_key":"25eeb7fd-d796-fe2a-359a-60507ad6ce6d",
"pincode":"563125"*/

    private String api_key;
    private String pincode;

    /*"Status": true,
    "Message": "Success",
    "Response": "Congrats,we are Delivering in this location",
    "code": 1*/

    private boolean Status;
    private String Message;
    private String Response;
    private String code;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
