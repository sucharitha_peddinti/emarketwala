package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.ProductsListItemBinding;
import com.android.marketwala.databinding.RestaurantsListItemBinding;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.RestaurantsResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.ProductsVH> {

    ArrayList<RestaurantsResponse.RestaurantsData> itemsData = new ArrayList<>();

    RestaurantsItemClick click;

    public RestaurantsAdapter(ArrayList<RestaurantsResponse.RestaurantsData> itemsData, RestaurantsItemClick click) {
        this.itemsData = itemsData;
        this.click = click;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.restaurants_list_item, parent, false));
    }

    @SuppressLint({"SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        RestaurantsResponse.RestaurantsData data = itemsData.get(position);

        holder.binding.getRoot().setOnClickListener(v -> click.onRestaurantsItemClick(itemsData.get(position)));


        holder.binding.restNameTVID.setText(data.getName());
        holder.binding.restSubNameTVID.setText(data.getTitle());
        holder.binding.locationTVID.setText(data.getLocation());


        Glide
                .with(holder.binding.restPicID.getContext())
                .load(data.getBanner_image())
                .into(holder.binding.restPicID);


        if (data.getStatus().equals("0")) {
            holder.binding.closedBgLLID.setBackgroundColor(holder.binding.closedBgLLID.getContext().getResources().getColor(R.color.rest_closed));
            holder.binding.restStatusTVID.setVisibility(View.VISIBLE);
            holder.binding.closedImage.setVisibility(View.VISIBLE);
        } else {
            holder.binding.closedBgLLID.setBackgroundColor(holder.binding.closedBgLLID.getContext().getResources().getColor(R.color.white));
            holder.binding.restStatusTVID.setVisibility(View.GONE);
            holder.binding.closedImage.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        RestaurantsListItemBinding binding;

        public ProductsVH(@NonNull RestaurantsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface RestaurantsItemClick {
        void onRestaurantsItemClick(RestaurantsResponse.RestaurantsData itemsData);
    }
}
