package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.OffersListItemBinding;
import com.android.marketwala.ui.models.OffersResponse;

import java.util.ArrayList;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.TypesVH> {

    ArrayList<OffersResponse.OffersData> typesData = new ArrayList<>();

    OfferClick click;

    public OffersAdapter(ArrayList<OffersResponse.OffersData> typesData, OfferClick click) {
        this.typesData = typesData;
        this.click = click;
    }

    @NonNull
    @Override
    public TypesVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TypesVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.offers_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TypesVH holder, int position) {
        holder.binding.setOfferData(typesData.get(position));
        holder.binding.getRoot().setOnClickListener(v -> click.onOfferItemClick(typesData.get(position)));

    }

    @Override
    public int getItemCount() {
        return typesData.size();
    }

    public static class TypesVH extends RecyclerView.ViewHolder {

        OffersListItemBinding binding;

        public TypesVH(@NonNull OffersListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OfferClick {
        void onOfferItemClick(OffersResponse.OffersData typesData);
    }
}
