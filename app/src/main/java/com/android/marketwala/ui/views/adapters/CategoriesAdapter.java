package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.SubCateListItemBinding;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.SubCategoriesResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ItemsVH> {

    ArrayList<HomeCategoriesResponse.Categories> itemsData = new ArrayList<>();

    ItemClick click;

    public CategoriesAdapter(ArrayList<HomeCategoriesResponse.Categories> itemsData, ItemClick click) {
        this.itemsData = itemsData;
        this.click = click;
    }

    @NonNull
    @Override
    public ItemsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.sub_cate_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsVH holder, int position) {

        holder.binding.getRoot().setOnClickListener(v -> click.onItemClick(itemsData.get(position)));

        holder.binding.titleTVID.setText(itemsData.get(position).getCategory_name());


        Glide
                .with(holder.binding.imageID.getContext())
                .load(itemsData.get(position).getCat_image())
                .placeholder(R.drawable.logo_green)
                .error(R.drawable.logo_green)
                .into(holder.binding.imageID);

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public static class ItemsVH extends RecyclerView.ViewHolder {

        SubCateListItemBinding binding;

        public ItemsVH(@NonNull SubCateListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface ItemClick {
        void onItemClick(HomeCategoriesResponse.Categories itemsData);
    }
}
