package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class SubCategoriesResponse {

    /*"Status": true,
    "Message": "Success",
    "Response"*/
    private boolean Status;
    private String Message;
    private ArrayList<SubCategoriesData> Response = new ArrayList<>();

    /*"api_key":"25eeb7fd-d796-fe2a-359a-60507ad6ce6d",
"category_id":"15"*/

    private String api_key;
    private String category_id;

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<SubCategoriesData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<SubCategoriesData> response) {
        Response = response;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public static class SubCategoriesData {
        /* "subcat_id": "2",
            "category_id": "15",
            "subcat_name": "Other Fruits",
            "subcat_image": "https://emarketwala.com/uploads/sub_categories/1631452410download.jpg",
            "status": "1",
            "created_date": "2021-04-02",
            "updated_date": "2021-09-12 09:13:30"*/

        private String subcat_id;
        private String category_id;
        private String subcat_name;
        private String subcat_image;
        private String status;
        private String created_date;
        private String updated_date;

        public String getSubcat_id() {
            return subcat_id;
        }

        public void setSubcat_id(String subcat_id) {
            this.subcat_id = subcat_id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getSubcat_name() {
            return subcat_name;
        }

        public void setSubcat_name(String subcat_name) {
            this.subcat_name = subcat_name;
        }

        public String getSubcat_image() {
            return subcat_image;
        }

        public void setSubcat_image(String subcat_image) {
            this.subcat_image = subcat_image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
