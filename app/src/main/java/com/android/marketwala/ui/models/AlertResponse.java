package com.android.marketwala.ui.models;

public class AlertResponse {


    private String api_key;
    private boolean Status;
    private String Message;
    private AlertMsgResponse Response;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public AlertMsgResponse getResponse() {
        return Response;
    }

    public void setResponse(AlertMsgResponse response) {
        Response = response;
    }

    public static class AlertMsgResponse {

        private String id;
        private String content;
        private String status;
        private String updated_date;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }

}


