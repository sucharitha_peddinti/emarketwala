package com.android.marketwala.ui.cartDB;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = CartItems.class,exportSchema = false,version = 2)
public abstract class MarketWalaDB extends RoomDatabase {

    private static final String DATA_BASE_NAME="market_wala";
    private static MarketWalaDB instance;

    public static synchronized MarketWalaDB getInstance(Context context){

        if (instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),MarketWalaDB.class,DATA_BASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }

    public abstract CartDAO cartDAO();
}
