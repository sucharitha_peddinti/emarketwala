package com.android.marketwala.ui.models;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

public class StaticData {
    private int id;
    private String title;
    private int image;

    public StaticData(int id, String title, int image) {
        this.id = id;
        this.title = title;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }



    @BindingAdapter("image")
    public static void setImage(AppCompatImageView image,int url){
        Glide.with(image.getContext()).load(url).into(image);
    }
}
