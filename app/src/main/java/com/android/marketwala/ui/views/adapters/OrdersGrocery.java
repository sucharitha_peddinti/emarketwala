package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.MyOrderListItemBinding;
import com.android.marketwala.ui.models.MyOrdersPOJO;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class OrdersGrocery extends RecyclerView.Adapter<OrdersGrocery.ProductsVH> {

    List<MyOrdersPOJO.MyOrderProducts> itemsData = new ArrayList<>();
    Activity activity;
    ItemClick click;

    public OrdersGrocery(Activity activity, List<MyOrdersPOJO.MyOrderProducts> itemsData, ItemClick click) {
        this.itemsData = itemsData;
        this.activity = activity;
        this.click = click;

    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.my_order_list_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        MyOrdersPOJO.MyOrderProducts data = itemsData.get(position);
        holder.binding.nameTVID.setText(data.getName());
        holder.binding.quantityTVID.setText("Quantity : " + data.getQuantity());
        holder.binding.priceTVID.setText("\u20b9 " + data.getPrice());
        holder.binding.createdDateTVID.setText(data.getCreated_date());
        holder.binding.deliveryStatusTVID.setText(data.getDelivery_status());

        Glide.with(activity).load(data.getImage()).error(R.drawable.logo_green)
                .placeholder(R.drawable.logo_green).
                into(holder.binding.imageID);

        Log.e("image", "" + data.getImage());
        if (position == itemsData.size() - 1) {
            holder.binding.viewID.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public interface ItemClick {
        void onItemCancel(MyOrdersPOJO.MyOrderProducts itemData);
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        MyOrderListItemBinding binding;

        public ProductsVH(@NonNull MyOrderListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
