package com.android.marketwala.ui.views.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.RestFragmentProductsBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.models.RestaurantItemsResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.RestaurantProductsAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

public class RestaurantItemsFragment extends Fragment implements AppListener, RestaurantProductsAdapter.ProductItemClick {

    RestFragmentProductsBinding binding;
    AppViewModel viewModel;
    String id;
    ArrayList<RestaurantItemsResponse.RestaurantsData> dataArrayList = new ArrayList<>();
    List<CartItems> cartData = new ArrayList<>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    public RestaurantItemsFragment getData(String id) {
        RestaurantItemsFragment fragment = new RestaurantItemsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getString("key");
        }
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.rest_fragment_products, container, false);
        View view = binding.getRoot();

        binding.titleTVID.setText("All Products");
        viewModel = new AppViewModel(this);

        RestaurantItemsResponse response = new RestaurantItemsResponse();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setRestaurant_id(id);
        MainActivity.isFood = true;


        CartViewModel cartVM = new CartViewModel(getActivity(), true);
        cartVM.cartData();
        cartVM.getGetCartItems().observe(getActivity(), cartItems -> {
            cartData = cartItems;
        });

        viewModel.restaurantItemsData(response);
        viewModel.getRestaurantItemsResponseLiveData().observe(getActivity(), response1 -> {
            if (response1 != null) {
                dataArrayList = response1.getResponse();
                if (dataArrayList.size() > 0) {
                    binding.productsRCID.setVisibility(View.VISIBLE);
                    binding.progressBarProduct.setVisibility(View.GONE);


                    binding.noData.noDataViewLLID.setVisibility(View.GONE);
                    binding.mainLLID.setVisibility(View.VISIBLE);

                    RestaurantProductsAdapter adapter = new RestaurantProductsAdapter(dataArrayList, this, cartData);
                    binding.productsRCID.setAdapter(adapter);
                } else {
                    setErrorViews();
                }
            } else {
                setErrorViews();
            }
        });


        return view;
    }

    private void setErrorViews() {
        binding.productsRCID.setVisibility(View.GONE);
        binding.progressBarProduct.setVisibility(View.GONE);
        binding.noData.noDataViewLLID.setVisibility(View.VISIBLE);
        binding.mainLLID.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.progressBarProduct.setVisibility(View.VISIBLE);
            // binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();
            binding.noData.noDataViewLLID.setVisibility(View.GONE);
            binding.mainLLID.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.progressBarProduct.setVisibility(View.GONE);
            // binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();
            binding.noData.noDataViewLLID.setVisibility(View.GONE);
            binding.mainLLID.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onProductItemClick(RestaurantItemsResponse.RestaurantsData itemsData) {

    }

    @Override
    public void onAddToCartClicked(RestaurantItemsResponse.RestaurantsData itemsData, String qty) {
        CartItems items = new CartItems(0, "0", Utilities.getDeviceID(getActivity()), AppConstants.PRODUCTS_REST, itemsData.getItem_name(), itemsData.getItem_image(), qty, String.valueOf(itemsData.getFull_price_offer()), String.valueOf(itemsData.getFull_price_offer()), itemsData.getItem_id(), id, itemsData.getItem_name());
        CartViewModel viewModel = new CartViewModel(getActivity());
        viewModel.insert(items);
        Log.e("cart", itemsData.getFull_price_offer());
        Utilities.customSnackBar(getActivity(), binding.mainID, "Item added to cart successfully");
    }
}
