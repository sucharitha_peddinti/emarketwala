package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class ProductDetailsResponse {

    /*

    "api_key":"25eeb7fd-d796-fe2a-359a-60507ad6ce6d",
"product_id":"15"

      "Status": true,
    "Message": "Success",
    "Response":
*/

    private boolean Status;
    private String Message;
    private ProductDetailsData Response;

    private String api_key;
    private String product_id;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ProductDetailsData getResponse() {
        return Response;
    }

    public void setResponse(ProductDetailsData response) {
        Response = response;
    }

    public static class ProductDetailsData {

        /*
     "product_id": "15",
        "product_num": "P6866",
        "category_id": "14",
        "subcat_id": "10",
        "product_name": "Awallaki Pressed",
        "short_description": null,
        "full_description": null,
        "product_image": "https://emarketwala.com/uploads/products/1628495738R.jpg",
        "slider_images": [
            "https://emarketwala.com/uploads/products/1628495738rice_flakes_paneri.jpg",
            "https://emarketwala.com/uploads/products/1628403699download.jpg"
        ],
        "quanty": "250gms",
        "weight": "250 gms",
        "total_price": "50",
        "offer_price": "40",
        "offer_percentage": "20",
        "status": "1",
        "created_date": "2021-08-08",
        "updated_date": "2021-08-09 03:55:38"*/


        private String product_id;
        private String product_num;
        private String category_id;
        private String subcat_id;
        private String product_name;
        private String short_description;
        private String full_description;
        private String product_image;
        private ArrayList<String> slider_images;
        private String quanty;
        private String weight;
        private String total_price;
        private String offer_price;
        private String final_amount;
        private String offer_percentage;
        private String status;
        private String created_date;
        private String updated_date;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_num() {
            return product_num;
        }

        public void setProduct_num(String product_num) {
            this.product_num = product_num;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getSubcat_id() {
            return subcat_id;
        }

        public void setSubcat_id(String subcat_id) {
            this.subcat_id = subcat_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getFinal_amount() {
            return final_amount;
        }

        public void setFinal_amount(String final_amount) {
            this.final_amount = final_amount;
        }

        public String getFull_description() {
            return full_description;
        }

        public void setFull_description(String full_description) {
            this.full_description = full_description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public ArrayList<String> getSlider_images() {
            return slider_images;
        }

        public void setSlider_images(ArrayList<String> slider_images) {
            this.slider_images = slider_images;
        }

        public String getQuanty() {
            return quanty;
        }

        public void setQuanty(String quanty) {
            this.quanty = quanty;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getOffer_percentage() {
            return offer_percentage;
        }

        public void setOffer_percentage(String offer_percentage) {
            this.offer_percentage = offer_percentage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
