package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.databinding.OrdersMainListItemBinding;
import com.android.marketwala.ui.models.MyOrdersPOJO;

import java.util.ArrayList;
import java.util.List;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ProductsVH> implements OrdersGrocery.ItemClick {

    List<MyOrdersPOJO.MyOrdersResponse> itemsData = new ArrayList<>();
    Activity activity;
    GroceryOrdersClick click;

    public MyOrdersAdapter(Activity activity, List<MyOrdersPOJO.MyOrdersResponse> itemsData, GroceryOrdersClick click) {
        this.itemsData = itemsData;
        this.activity = activity;
        this.click = click;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.orders_main_list_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        try {
            MyOrdersPOJO.MyOrdersResponse data = itemsData.get(position);

            holder.binding.orderNumberTVID.setText(data.getOrder_number());
            holder.binding.orderAmountTVID.setText("\u20b9 " + data.getOrder_amount());
            holder.binding.orderPaymentTVID.setText(data.getPayment_type());
            OrdersGrocery as = new OrdersGrocery(activity, data.getProduct_details(), this);
            holder.binding.dataRCID.setAdapter(as);

            String delStatus = data.getProduct_details().get(0).getDelivery_status();

            if (delStatus.equals("1") || delStatus.equals("0")) {
                holder.binding.cancelBtn.setVisibility(View.VISIBLE);
                holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_cancel));
                holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.red));
                holder.binding.cancelBtn.setText("Cancel");
            } else if (delStatus.equals(AppConstants.ORDER_SHIPPED)) {
                holder.binding.cancelBtn.setVisibility(View.VISIBLE);
                holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_shipped));
                holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.yellow));
                holder.binding.cancelBtn.setText("Shipped");
            } else if (delStatus.equals(AppConstants.ORDER_CANCELLED)) {
                holder.binding.cancelBtn.setVisibility(View.VISIBLE);
                holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_cancelled));
                holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.white));
                holder.binding.cancelBtn.setText("Cancelled");
            } else if (delStatus.equals(AppConstants.ORDER_DELIVERED)) {
                holder.binding.cancelBtn.setVisibility(View.VISIBLE);
                holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_deliverd));
                holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.white));
                holder.binding.cancelBtn.setText("Delivered");
            }

            Log.e("order_number-->", "" + data.getProduct_details().get(0).getOrder_number());

            holder.binding.cancelBtn.setOnClickListener(v -> {
                if (delStatus.equals("1") || delStatus.equals("0")) {
                    click.onGroceryCancel(data.getProduct_details().get(0).getOrder_number());
                }
            });

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    @Override
    public void onItemCancel(MyOrdersPOJO.MyOrderProducts itemData) {
        click.onGroceryCancel(itemData.getOrder_number());
    }

    public interface GroceryOrdersClick {
        void onGroceryCancel(String id);
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        OrdersMainListItemBinding binding;

        public ProductsVH(@NonNull OrdersMainListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
