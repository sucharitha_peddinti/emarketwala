package com.android.marketwala.ui.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentRegisterBinding;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.models.UserResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;

public class RegisterFragment extends Fragment implements AppListener {

    FragmentRegisterBinding binding;
    AppViewModel viewModel;
    boolean oneChecked = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);

        View view = binding.getRoot();

        binding.closeIMVID.setOnClickListener(v -> getActivity().onBackPressed());

        binding.checkbox.setChecked(false);

        binding.signInBtn.setOnClickListener(v -> {

            if (Utilities.isNetworkAvailable(getActivity())) {

                validations();


            } else {
                Utilities.noInternet(getActivity());
            }


        });


        return view;
    }

    private void validations() {
        String name = binding.nameETID.getText().toString();
        String email = binding.emailETID.getText().toString();
        String mobile = binding.mobilrETID.getText().toString();
        String password = binding.passwordETID.getText().toString();
        String confirmPassword = binding.confirmPasswordETID.getText().toString();
        String pincode = binding.pincodeETID.getText().toString();
        String state = binding.stateETID.getText().toString();
        String city = binding.cityETID.getText().toString();
        String address = binding.addressETID.getText().toString();

        if (TextUtils.isEmpty(name)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter username");
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter email-id");
            return;
        }
        if (TextUtils.isEmpty(mobile)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter Phone Number");
            return;
        }
        if (TextUtils.isEmpty(address)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter Address");
            return;
        }
        if (TextUtils.isEmpty(pincode)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter Pincode");
            return;
        }
        if (TextUtils.isEmpty(state)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter State");
            return;
        }
        if (TextUtils.isEmpty(city)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter City");
            return;
        }


        if (!Utilities.isValidEmail(email)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter valid email-id");
            return;
        }


        if (TextUtils.isEmpty(password)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Enter password");
            return;
        }

        if (!binding.checkbox.isChecked()) {
            binding.checkbox.setChecked(false);
            Utilities.customSnackBar(getActivity(), binding.checkbox, "Please accept Terms and Conditions...");
            return;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Confirm password");
            return;
        }

        if (!password.matches(confirmPassword)) {
            Utilities.customSnackBar(getActivity(), binding.mainRLID, "Password doesn't matched");
            return;
        }

        UserResponse userResponse = new UserResponse();
        userResponse.setApi_key(AppConstants.API_TOKEN);
        userResponse.setName(name);
        userResponse.setEmail_id(email);
        userResponse.setMobile_number(mobile);
        userResponse.setPswrd(password);
        userResponse.setAddress(address);
        userResponse.setPincode(pincode);
        userResponse.setCity(city);
        userResponse.setState(state);

        callApi(userResponse);

    }

    private void callApi(UserResponse userResponse) {
        viewModel = new AppViewModel(this);
        viewModel.registration(userResponse, "1");
        viewModel.getUserResponseLiveData().observe(getActivity(), registrationResponse -> {

            if (registrationResponse.isStatus()) {


                UserLoginData loginData = registrationResponse.getResponse();
                Utilities.saveUserData(getActivity(), loginData);
                MySharedPreferences.setPreference(getActivity(), AppConstants.LOGGED_IN_STATUS, AppConstants.LOGGED_IN_VALUE);

                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();

            } else {
                Utilities.customSnackBar(getActivity(), binding.mainRLID, registrationResponse.getMessage());
            }
        });
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Utilities.showSimpleProgressDialog(getActivity(), null, loadingMsg, false);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Utilities.removeSimpleProgressDialog();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {
        //
    }
}
