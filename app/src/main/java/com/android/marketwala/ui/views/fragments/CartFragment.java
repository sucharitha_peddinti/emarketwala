package com.android.marketwala.ui.views.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentCartBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.models.AddressData;
import com.android.marketwala.ui.models.CommonResponse;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.ConfirmOrderActivity;
import com.android.marketwala.ui.views.activities.ShowAddressActivity;
import com.android.marketwala.ui.views.activities.UserActivities;
import com.android.marketwala.ui.views.adapters.CartAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

public class CartFragment extends Fragment implements CartAdapter.ProductItemClick, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AppListener {

    FragmentCartBinding binding;
    CartViewModel viewModel;
    List<CartItems> cartItemsList = new ArrayList<>();
    UserLoginData loginData;
    Location location;
    List<CartItems> cartData = new ArrayList<>();
    String loginStatus;
    AppViewModel appVM;
    AddressData data;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private boolean isFood;

    public CartFragment getData(boolean isFood) {
        CartFragment fragment = new CartFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("key", isFood);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        isFood = bundle.getBoolean("key");
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        View view = binding.getRoot();
        appVM = new AppViewModel(this);
        loginData = Utilities.getUserData(getActivity());
        loginStatus = MySharedPreferences.getPreferences(getActivity(), AppConstants.LOGGED_IN_STATUS);
        data = Utilities.getAddress(getActivity());

        if (loginStatus.equals(AppConstants.LOGGED_IN_VALUE)) {
            getAddressDetails();
        } else {
            binding.locationCVID.setVisibility(View.GONE);
        }


        binding.btnPlaceOrder.setOnClickListener(v -> {
            if (loginStatus.equals(AppConstants.LOGGED_IN_VALUE)) {
                //AddressData data = Utilities.getAddress(getActivity());
                if (data != null) {
                    Intent intent = new Intent(getActivity(), ConfirmOrderActivity.class);
                    intent.putExtra("cart_type", isFood);
                    startActivity(intent);
                } else {
                    Utilities.showToast(getActivity(), "Add address");
                }
            } else {
                Intent intent = new Intent(getActivity(), UserActivities.class);
                intent.putExtra(AppConstants.SCREEN_FROM, AppConstants.SCREEN_LOGIN);
                startActivity(intent);
            }
        });


        binding.changeTVID.setOnClickListener(v -> startActivity(new Intent(getActivity(), ShowAddressActivity.class)));

        loadCartData(isFood);
      /*  binding.cartGroceryTVID.setOnClickListener(v -> {
            loadCartData(false);
            binding.cartGroceryTVID.setBackgroundResource(R.drawable.bg_btn_main);
            binding.cartFoodTVID.setBackgroundResource(R.color.white);

            binding.cartGroceryTVID.setTextColor(getResources().getColor(R.color.white));
            binding.cartFoodTVID.setTextColor(getResources().getColor(R.color.black));
        });


        binding.cartFoodTVID.setOnClickListener(v -> {
            loadCartData(true);
            binding.cartGroceryTVID.setBackgroundResource(R.color.white);
            binding.cartFoodTVID.setBackgroundResource(R.drawable.bg_btn_main);

            binding.cartGroceryTVID.setTextColor(getResources().getColor(R.color.black));
            binding.cartFoodTVID.setTextColor(getResources().getColor(R.color.white));
        });*/


       /* mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();*/



      /*  CartViewModel cartViewModel=new CartViewModel(getActivity());
        cartViewModel.typeBasedCartCount(Utilities.getDeviceID(getActivity()),AppConstants.PRODUCTS_REST);
        cartViewModel.getTypeBasedCartCount().observe(getActivity(),cartItems -> {
            if (cartItems.size()>0){
                binding.cartFoodTVID.setText("Food Cart ("+cartItems.size()+")");
            }else{
                binding.cartFoodTVID.setText("Food Cart");
            }
        });

        cartViewModel.typeBasedCartCount(Utilities.getDeviceID(getActivity()),AppConstants.PRODUCTS_CART);
        cartViewModel.getTypeBasedCartCount().observe(getActivity(),cartItems -> {
            if (cartItems.size()>0){
                binding.cartGroceryTVID.setText("Grocery Cart ("+cartItems.size()+")");
            }else{
                binding.cartGroceryTVID.setText("Grocery Cart");
            }
        });*/


        return view;
    }

    private void checkPinCodeAvailable() {
        try {


            CommonResponse commonResponse = new CommonResponse();
            commonResponse.setApi_key(AppConstants.API_TOKEN);
            commonResponse.setPincode(data.getPincode());
            appVM.onPinCodeSearch(commonResponse);
            appVM.getPinCodeSearch().observe(getActivity(), commonResponse1 -> {
                if (commonResponse1.isStatus()) {
                    binding.confirmFalseBtn.setVisibility(View.GONE);
                    binding.btnPlaceOrder.setVisibility(View.VISIBLE);
                    binding.noDeliveryLLID.setVisibility(View.GONE);
                } else {
                    binding.confirmFalseBtn.setVisibility(View.VISIBLE);
                    binding.btnPlaceOrder.setVisibility(View.GONE);
                    binding.noDeliveryLLID.setVisibility(View.VISIBLE);
                    binding.noDeliveryTVID.setText(commonResponse1.getMessage());
                }
            });

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void loadCartData(boolean isFood) {
        viewModel = new CartViewModel(getActivity(), isFood);
        viewModel.cartData();

        CartViewModel cartVM = new CartViewModel(getActivity(), false);
        cartVM.cartData();
        cartVM.getGetCartItems().observe(getActivity(), cartItems -> {
            cartData = cartItems;
        });


        viewModel.getGetCartItems().observe(getActivity(), cartItems -> {
            cartItemsList = cartItems;

            if (cartItems.size() > 0) {
                binding.itemCountTVID.setText("" + cartItems.size() + " Items");

                getTotalPrice(cartItemsList);

                binding.mainRLID.setVisibility(View.VISIBLE);
                binding.noDataTVID.setVisibility(View.GONE);
                CartAdapter adapter = new CartAdapter(getActivity(), cartItems, this, cartData);
                binding.cartRCID.setAdapter(adapter);
            } else {
                binding.mainRLID.setVisibility(View.GONE);
                binding.noDataTVID.setVisibility(View.VISIBLE);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getTotalPrice(List<CartItems> cartItemsList) {
        try {

            double sum = 0;
            for (int i = 0; i < cartItemsList.size(); i++) {
                sum += Double.parseDouble(cartItemsList.get(i).getPrice()) * Double.parseDouble(cartItemsList.get(i).getCartQty());
            }
            binding.totalPriceTVID.setText("\u20b9 " + sum);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.GONE), 200);
    }


    @Override
    public void onProductItemClick(CartItems itemsData) {
        if (!isFood) {
            ProductDetailsFragment fragment = new ProductDetailsFragment().getData(itemsData.getProduct_id(), true);
            FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_PRODUCT_DETAILS);
        }
    }

    @Override
    public void onAddToCartClicked(CartItems itemsData, String cartQty) {
        CartItems items = new CartItems(0, "0", Utilities.getDeviceID(getActivity()), itemsData.typeOfCart, itemsData.getItemName(), itemsData.getItemImage(), cartQty, String.valueOf(itemsData.getPrice()), String.valueOf(itemsData.getOffer_price()), itemsData.getProduct_id(), "0", itemsData.weight);
        CartViewModel viewModel = new CartViewModel(getActivity());
        viewModel.insert(items);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("location", "on connected");


       /* mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000 * 60 * 2); // Update location every 2 minutes
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location newLocation) {


                location = newLocation;
                if (location != null) {
                  //  getLocation();
                }

            }
        });*/
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.d("location", "on suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("location", "on failed");
    }

    @Override
    public void onStart() {
        super.onStart();
        // mGoogleApiClient.connect();
        Log.d("attendance", "connected");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //  mGoogleApiClient.disconnect();
        Log.d("attendance", "dis connected");
    }


    @Override
    public void onResume() {
        super.onResume();
        String refresh = MySharedPreferences.getPreferences(getActivity(), AppConstants.REFRESH);
        if (refresh.equalsIgnoreCase(AppConstants.YES)) {
            MySharedPreferences.setPreference(getActivity(), AppConstants.REFRESH, AppConstants.NO);
            getAddressDetails();
        }
    }

    @SuppressLint("SetTextI18n")
    private void getAddressDetails() {
        data = Utilities.getAddress(getActivity());
        if (data != null) {
            binding.userNameTVID.setText(data.getName());
            binding.addressTVID.setText(data.getMobile_number() + "," + data.getEmail_id() + "," + data.getAddress() + "," + data.getCity() + "," + data.getState() + "," + data.getPincode() + ".");

            checkPinCodeAvailable();

        } else {
            binding.locationCVID.setVisibility(View.VISIBLE);
            binding.userNameTVID.setText(loginData.getName());
            binding.addressTVID.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Utilities.showSimpleProgressDialog(getActivity(), null, loadingMsg, false);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Utilities.removeSimpleProgressDialog();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Utilities.showToast(getActivity(), msg);
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }
}
