package com.android.marketwala.ui.models;

import java.io.Serializable;
import java.util.ArrayList;

public class AddressListResponse implements Serializable {

    /*  "Status": true,
    "Message": "Success",
    "Response":*/

    private String user_id;
    private String api_key;


    private boolean Status;
    private String Message;
    private ArrayList<AddressData> Response = new ArrayList<>();

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<AddressData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<AddressData> response) {
        Response = response;
    }
}
