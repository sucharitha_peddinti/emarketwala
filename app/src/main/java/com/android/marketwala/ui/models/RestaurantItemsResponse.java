package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class RestaurantItemsResponse {


    /*  "Status": true,
    "Message": "Success",
    "Response": [
        {
            "item_id": "5",
            "res_id": "5",
            "item_name": " Chicken Biryani",
            "item_image": "1629355280chicken-biryani-recipe-735x490.jpg",
            "full_price": "90",
            "half_price": "NA",
            "full_price_offer": "80",
            "half_price_offer": "NA",
            "sdescription": "<p>Chicken Biryani is&nbsp;<strong>a savory chicken and rice dish</strong>&nbsp;that includes layers of chicken, rice, and aromatics that are steamed together. The bottom layer of rice absorbs all the chicken juices as it cooks, giving it a tender texture and rich flavor, while the top layer of rice turns out white and fluffy.</p>\r\n",
            "status": "1",
            "created_date": "2021-08-07",
            "updated_date": "2021-08-24 14:16:35"
        }*/

    private boolean Status;
    private String Message;
    private ArrayList<RestaurantsData>Response=new ArrayList<>();

    private String restaurant_id;
    private String api_key;

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<RestaurantsData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<RestaurantsData> response) {
        Response = response;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public static class RestaurantsData {

        private String item_id;
        private String res_id;
        private String item_name;
        private String item_image;
        private String full_price;
        private String half_price;
        private String full_price_offer;
        private String half_price_offer;
        private String final_price_;
        private String sdescription;
        private String status;
        private String created_date;
        private String updated_date;

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getRes_id() {
            return res_id;
        }

        public void setRes_id(String res_id) {
            this.res_id = res_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getItem_image() {
            return item_image;
        }

        public void setItem_image(String item_image) {
            this.item_image = item_image;
        }

        public String getFull_price() {
            return full_price;
        }

        public void setFull_price(String full_price) {
            this.full_price = full_price;
        }

        public String getHalf_price() {
            return half_price;
        }

        public void setHalf_price(String half_price) {
            this.half_price = half_price;
        }

        public String getFull_price_offer() {
            return full_price_offer;
        }

        public void setFull_price_offer(String full_price_offer) {
            this.full_price_offer = full_price_offer;
        }

        public String getHalf_price_offer() {
            return half_price_offer;
        }

        public void setHalf_price_offer(String half_price_offer) {
            this.half_price_offer = half_price_offer;
        }

        public String getSdescription() {
            return sdescription;
        }

        public void setSdescription(String sdescription) {
            this.sdescription = sdescription;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }

        public String getFinal_price_() {
            return final_price_;
        }

        public void setFinal_price_(String final_price_) {
            this.final_price_ = final_price_;
        }
    }
}
