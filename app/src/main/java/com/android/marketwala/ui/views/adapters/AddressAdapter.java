package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.AddressListItemBinding;
import com.android.marketwala.databinding.MainProductListItemsBinding;
import com.android.marketwala.ui.models.AddressData;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressVH> {

    ArrayList<AddressData> itemsData = new ArrayList<>();
    AddressClick click;

    public AddressAdapter(ArrayList<AddressData> itemsData, AddressClick click) {
        this.itemsData = itemsData;
        this.click = click;
    }

    @NonNull
    @Override
    public AddressVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddressVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.address_list_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AddressVH holder, int position) {

        AddressData data = itemsData.get(position);
        holder.binding.addressTVID.setText(data.getName() + " , " + data.getMobile_number() + ",\n"
                + data.getEmail_id() + ",\n" + data.getAddress() + ",\n" + data.getCity() + "," + data.getState() + "," + data.getPincode() + ".");

        holder.binding.editBtn.setOnClickListener(v -> click.onEditClick(data));
        holder.binding.useBtn.setOnClickListener(v -> click.onUseClick(data));
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public static class AddressVH extends RecyclerView.ViewHolder {

        AddressListItemBinding binding;
        public AddressVH(@NonNull AddressListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }



    public interface AddressClick {
        void onEditClick(AddressData itemsData);
        void onUseClick(AddressData itemsData);
    }
}
