package com.android.marketwala.ui.models;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HomeSlidersResponse {

    private String api_key;

    private boolean Status;
    private String Message;
    private ArrayList<SlidersData> Response = new ArrayList<>();

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<SlidersData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<SlidersData> response) {
        Response = response;
    }

    public static class SlidersData {

        /*"id": "9",
            "slider_image": "https://emarketwala.com/uploads/homesliders/1624709814slider-1.jpg",
            "status": "1",
            "created_date": "2021-06-26",
            "updated_date": "2021-06-26 08:16:54"*/

        private String id;
        private String slider_image;
        private String status;
        private String created_date;
        private String updated_date;

        @BindingAdapter("slider_image")
        public static void loadImage(AppCompatImageView image,String slider_image){
            Glide.with(image.getContext()).load(slider_image).into(image);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSlider_image() {
            return slider_image;
        }

        public void setSlider_image(String slider_image) {
            this.slider_image = slider_image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }

}
