package com.android.marketwala.ui.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentForgotPasswordBinding;
import com.android.marketwala.ui.models.ForgotPasswordResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.UserActivities;

public class ForgotPasswordFragment extends Fragment implements AppListener {

    FragmentForgotPasswordBinding binding;
    AppViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false);

        View view = binding.getRoot();
        binding.closeIMVID.setOnClickListener(v -> getActivity().onBackPressed());
        viewModel = new AppViewModel(this);

        binding.loginBtn.setOnClickListener(v -> {
            if (Utilities.isNetworkAvailable(getActivity())) {
                String email = binding.emailETID.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Utilities.showToast(getActivity(), "Enter email id");
                    return;
                }

                if (!Utilities.isValidEmail(email)) {
                    Utilities.showToast(getActivity(), "Enter valid email id");
                    return;
                }

                callForgotPassword(email);

            } else {
                Utilities.noInternet(getActivity());
            }
        });

        return view;
    }

    private void callForgotPassword(String email) {
        ForgotPasswordResponse response = new ForgotPasswordResponse();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setEmail_id(email);

        viewModel.onForgotPassword(response);
        viewModel.getForgotPasswordResponseLiveData().observe(getActivity(), response1 -> {
            if (response1 != null) {
                if (response1.isStatus()) {
                    Utilities.showToast(getActivity(), response1.getResponse());
                    Intent intent = new Intent(getActivity(), UserActivities.class);
                    intent.putExtra(AppConstants.SCREEN_FROM, AppConstants.SCREEN_LOGIN);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Utilities.showToast(getActivity(), "" + response1.getMessage());
                }
            } else {
                Utilities.showToast(getActivity(), "Unknown error from server");
            }
        });
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Utilities.showSimpleProgressDialog(getContext(), null, loadingMsg, false);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Utilities.removeSimpleProgressDialog();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Utilities.showToast(getActivity(), msg);
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }
}
