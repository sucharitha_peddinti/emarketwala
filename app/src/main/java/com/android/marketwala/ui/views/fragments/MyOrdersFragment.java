package com.android.marketwala.ui.views.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentMyOrdersBinding;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.models.CancelOrderResponse;
import com.android.marketwala.ui.models.CommonResponse;
import com.android.marketwala.ui.models.FoodMyOrdersPOJO;
import com.android.marketwala.ui.models.MyOrdersPOJO;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.MyFoodOrdersAdapter;
import com.android.marketwala.ui.views.adapters.MyOrdersAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

public class MyOrdersFragment extends Fragment implements AppListener, MyFoodOrdersAdapter.FoodOrdersClick, MyOrdersAdapter.GroceryOrdersClick {

    FragmentMyOrdersBinding binding;
    AppViewModel viewModel;
    UserLoginData data;

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);
        View view = binding.getRoot();
        MainActivity.isFood = false;

        data = Utilities.getUserData(getActivity());
        viewModel = new AppViewModel(this);


        binding.groceryRLID.setOnClickListener(v -> {
            binding.groceryTVID.setTextColor(R.color.green);
            binding.foodTVID.setTextColor(R.color.black);

            binding.groceryViewID.setVisibility(View.VISIBLE);
            binding.foodViewID.setVisibility(View.GONE);
            loadGroceryOrders();
        });

        binding.foodRLID.setOnClickListener(v -> {
            binding.groceryTVID.setTextColor(R.color.black);
            binding.foodTVID.setTextColor(R.color.green);

            binding.groceryViewID.setVisibility(View.GONE);
            binding.foodViewID.setVisibility(View.VISIBLE);
            loadFoodOrders();
        });

        loadGroceryOrders();

        return view;
    }

    private void loadGroceryOrders() {

        MyOrdersPOJO response = new MyOrdersPOJO();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setUser_id(data.getUser_id());
        viewModel.onMyOrders(response);
        viewModel.getMyOrdersPOJOLiveData().observe(getActivity(), myOrdersPOJO -> {
            if (myOrdersPOJO.isStatus()) {
                ArrayList<MyOrdersPOJO.MyOrdersResponse> products = myOrdersPOJO.getResponse();
                if (products.size() > 0) {
                    binding.productsRCID.setVisibility(View.VISIBLE);
                    binding.progressBar.setVisibility(View.GONE);
                    MyOrdersAdapter adapter = new MyOrdersAdapter(getActivity(), products, this);
                    binding.productsRCID.setAdapter(adapter);

                } else {
                    setErrorViews();
                }
            } else {
                setErrorViews();
            }
        });
    }

    private void loadFoodOrders() {
        FoodMyOrdersPOJO response = new FoodMyOrdersPOJO();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setUser_id(data.getUser_id());

        viewModel.onFoodMyOrders(response);
        viewModel.getFoodMyOrdersPOJOLiveData().observe(getActivity(), foodMyOrdersPOJO -> {

            if (foodMyOrdersPOJO.isStatus()) {
                ArrayList<FoodMyOrdersPOJO.FoodMyOrdersResponse> mainData = foodMyOrdersPOJO.getResponse();

                if (mainData.size() > 0) {
                    binding.productsRCID.setVisibility(View.VISIBLE);
                    binding.progressBar.setVisibility(View.GONE);

                    MyFoodOrdersAdapter adapter = new MyFoodOrdersAdapter(getActivity(), mainData, this);
                    binding.productsRCID.setAdapter(adapter);
                } else {
                    setErrorViews();
                }

            } else {
                setErrorViews();
            }

        });
    }

    private void setErrorViews() {
        binding.productsRCID.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
        binding.noData.noDataViewLLID.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
            new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {

        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.VISIBLE);

            binding.noData.noDataViewLLID.setVisibility(View.GONE);
            binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();
        } else {
            Utilities.showSimpleProgressDialog(getContext(), null, loadingMsg, false);
        }
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.GONE);
            binding.noData.noDataViewLLID.setVisibility(View.GONE);
            binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();
        } else {
            Utilities.removeSimpleProgressDialog();
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onFoodCancel(String id) {
        if (Utilities.isNetworkAvailable(getActivity())) {
            cancelOrder(id, AppConstants.API_CANCEL_FOOD_ORDER);
        } else {
            Utilities.noInternet(getActivity());
        }
    }

    @Override
    public void onGroceryCancel(String id) {

        if (Utilities.isNetworkAvailable(getActivity())) {
            cancelOrder(id, AppConstants.API_CANCEL_GROCERY_ORDER);
        } else {
            Utilities.noInternet(getActivity());
        }

    }

    private void cancelOrder(String orderNumber, int orderType) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setCancelable(false);
        builder1.setTitle(null);
        builder1.setMessage("Are you sure ?\nDo you want to cancel the order");
        builder1.setPositiveButton("Yes", (dialogInterface, i1) -> {

            CancelOrderResponse response = new CancelOrderResponse();
            response.setApi_key(AppConstants.API_TOKEN);
            response.setOrder_number(orderNumber);
            response.setUser_id(data.getUser_id());


            viewModel.onFoodOrderCancel(response, orderType);
            viewModel.getCancelFoodOrder().observe(this, response1 -> {

                if (response1!=null){
                    if (response1.isStatus()) {

                        if (orderType == AppConstants.API_CANCEL_FOOD_ORDER) {
                            loadFoodOrders();
                        } else if (orderType == AppConstants.API_CANCEL_GROCERY_ORDER) {
                            loadGroceryOrders();
                        }

                    } else {
                        Utilities.showToast(getActivity(), response1.getMessage());
                    }
                } else {
                    Utilities.showToast(getActivity(), "Server returns invalid response");
                }

            });
        });
        builder1.setNegativeButton("No", null);
        builder1.create().show();
    }
}
