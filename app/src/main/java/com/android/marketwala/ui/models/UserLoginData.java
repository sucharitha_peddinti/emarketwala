package com.android.marketwala.ui.models;

public class UserLoginData {

    /*"user_id": "95",
        "registration_number": null,
        "name": "Venkei",
        "mobile_number": "9618675201",
        "email_id": "venkei25@gmail.com"*/

    private String user_id;
    private String registration_number;
    private String name;
    private String mobile_number;
    private String email_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }
}
