package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.CartFinalListItemsBinding;
import com.android.marketwala.databinding.CartListItemBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class CartFinalAdapter extends RecyclerView.Adapter<CartFinalAdapter.ProductsVH> {

    List<CartItems> itemsData = new ArrayList<>();
    Activity activity;
    List<CartItems> cartData;

    public CartFinalAdapter(Activity activity, List<CartItems> itemsData) {
        this.itemsData = itemsData;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.cart_final_list_items, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {

        CartItems data = itemsData.get(position);
        holder.binding.productNameTVID.setText(data.getItemName());
        holder.binding.quantityTVID.setText(data.getCartQty() + " X " + data.getOffer_price());
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        CartFinalListItemsBinding binding;

        public ProductsVH(@NonNull CartFinalListItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
