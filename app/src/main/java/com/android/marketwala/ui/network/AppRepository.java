package com.android.marketwala.ui.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.domain.ApiInterface;
import com.android.marketwala.ui.models.AddressListResponse;
import com.android.marketwala.ui.models.AlertResponse;
import com.android.marketwala.ui.models.CancelOrderResponse;
import com.android.marketwala.ui.models.CommonResponse;
import com.android.marketwala.ui.models.FoodMyOrdersPOJO;
import com.android.marketwala.ui.models.ForgotPasswordResponse;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.HomeSlidersResponse;
import com.android.marketwala.ui.models.MyOrdersPOJO;
import com.android.marketwala.ui.models.OffersResponse;
import com.android.marketwala.ui.models.PostalCodeSearchResponse;
import com.android.marketwala.ui.models.ProductDetailsResponse;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.RestaurantItemsResponse;
import com.android.marketwala.ui.models.RestaurantsResponse;
import com.android.marketwala.ui.models.SaveAddressResponse;
import com.android.marketwala.ui.models.SaveOrderResponse;
import com.android.marketwala.ui.models.SubCategoriesResponse;
import com.android.marketwala.ui.models.UserResponse;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppRepository {
    ApiInterface apiInterface;
    Call<UserResponse> userCall;
    Call<ProductsResponse> productsCall;
    Call<CommonResponse> saveCall;
    Call<CancelOrderResponse> cancelOrderCall;

    public AppRepository() {
        apiInterface = Utilities.getApiClass();
    }

    public LiveData<HomeSlidersResponse> loadHomeSliders(AppListener listener, HomeSlidersResponse request) {
        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting banners...");
        MutableLiveData<HomeSlidersResponse> liveData = new MutableLiveData<>();
        apiInterface.homeSlidersAPI(request).enqueue(new Callback<HomeSlidersResponse>() {
            @Override
            public void onResponse(Call<HomeSlidersResponse> call, Response<HomeSlidersResponse> response) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Banners not getting \nError : " + response.message());

                }

            }

            @Override
            public void onFailure(Call<HomeSlidersResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Banners not getting \nError : " + t.getMessage());

            }
        });

        return liveData;
    }


    public LiveData<HomeCategoriesResponse> loadHomeCategories(AppListener listener, HomeSlidersResponse request) {
        listener.showLoading(AppConstants.CATEGORIES_ANIMATION, "getting categories...");
        MutableLiveData<HomeCategoriesResponse> liveData = new MutableLiveData<>();

        apiInterface.homeCategoriesAPI(request).enqueue(new Callback<HomeCategoriesResponse>() {
            @Override
            public void onResponse(Call<HomeCategoriesResponse> call, Response<HomeCategoriesResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.CATEGORIES_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.CATEGORIES_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "");
                }
            }

            @Override
            public void onFailure(Call<HomeCategoriesResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());
                liveData.setValue(null);
                listener.hideLoading(AppConstants.CATEGORIES_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Please check your internet..");
            }
        });

        return liveData;
    }


    public LiveData<OffersResponse> loadOffersData(AppListener listener, HomeSlidersResponse request) {
        listener.showLoading(AppConstants.OFFERS_ANIMATION, "getting categories...");
        MutableLiveData<OffersResponse> liveData = new MutableLiveData<>();
        apiInterface.homeOffersAPI(request).enqueue(new Callback<OffersResponse>() {
            @Override
            public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Offers not getting \nError : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<OffersResponse> call, Throwable t) {

                try {


                    Log.e("api==>", call.request().toString());
                    Log.e("response==>", t.getMessage());

                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Offers not getting \nError : " + t.getMessage());

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        return liveData;
    }

    //registration & login apis

    public LiveData<UserResponse> registrationData(AppListener listener, UserResponse user, String type) {

        MutableLiveData<UserResponse> liveData = new MutableLiveData<>();


        if (type.equals("1")) {
            listener.showLoading(AppConstants.OFFERS_ANIMATION, "Registration under process..");
            userCall = apiInterface.registrationAPI(user);
        } else {
            listener.showLoading(AppConstants.OFFERS_ANIMATION, "Login under process..");
            userCall = apiInterface.loginAPI(user);
        }

        userCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                if (response.body() != null && response.code() == 200) {
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.showMsg(AppConstants.MSG_ERROR, AppConstants.ACTION_STAY, "Registration failed \nError : " + response.message());
                }

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());
                listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                if (type.equals("1")) {
                    listener.showMsg(AppConstants.MSG_ERROR, AppConstants.ACTION_STAY, "Registration failed \nError : " + t.getMessage());
                    userCall = apiInterface.registrationAPI(user);
                } else {
                    listener.showMsg(AppConstants.MSG_ERROR, AppConstants.ACTION_STAY, "Invalid login credentials");
                    userCall = apiInterface.loginAPI(user);
                }


            }
        });
        return liveData;
    }


    public LiveData<ForgotPasswordResponse> forgotPassword(AppListener listener, ForgotPasswordResponse user) {
        listener.showLoading(AppConstants.CATEGORIES_ANIMATION, "Verifying email-id...");
        MutableLiveData<ForgotPasswordResponse> liveData = new MutableLiveData<>();
        apiInterface.forgotPasswordAPI(user).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                if (response.body() != null && response.code() == 200) {
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.showMsg(AppConstants.MSG_ERROR, AppConstants.ACTION_STAY, "Registration failed \nError : " + response.message());
                }

            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());
                listener.hideLoading(AppConstants.OFFERS_ANIMATION);
                listener.showMsg(AppConstants.MSG_ERROR, AppConstants.ACTION_STAY, "Registration failed \nError : " + t.getMessage());

            }
        });
        return liveData;
    }

    /*products api*/


    public LiveData<ProductsResponse> loadProducts(AppListener listener, ProductsResponse request, String api) {
        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting banners...");
        MutableLiveData<ProductsResponse> liveData = new MutableLiveData<>();

        if (api.equals("products")) {
            productsCall = apiInterface.categoryWisProductsAPI(request);
        } else {
            productsCall = apiInterface.productsSearchAPI(request);
        }


        productsCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Please check your internet..");

                }

            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                try {


                    Log.e("api==>", call.request().toString());
                    Log.e("response==>", t.getMessage());

                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Please check your internet..");
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        return liveData;
    }

    public LiveData<ProductDetailsResponse> loadProductDetails(AppListener listener, ProductDetailsResponse request) {
        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting banners...");
        MutableLiveData<ProductDetailsResponse> liveData = new MutableLiveData<>();

        apiInterface.productDetailsAPI(request).enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Details not getting \nError : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Details not getting \nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<RestaurantsResponse> restaurantsListDetails(AppListener listener, RestaurantsResponse request) {

        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting restaurants...");
        MutableLiveData<RestaurantsResponse> liveData = new MutableLiveData<>();
        apiInterface.restaurantsListAPI(request).enqueue(new Callback<RestaurantsResponse>() {
            @Override
            public void onResponse(Call<RestaurantsResponse> call, Response<RestaurantsResponse> response) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));

                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Please check your internet..");
                }
            }

            @Override
            public void onFailure(Call<RestaurantsResponse> call, Throwable t) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Please check your internet..");

            }
        });

        return liveData;
    }

    public LiveData<RestaurantItemsResponse> restItems(AppListener listener, RestaurantItemsResponse request) {

        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting restaurants...");
        MutableLiveData<RestaurantItemsResponse> liveData = new MutableLiveData<>();


        apiInterface.restaurantItemsListAPI(request).enqueue(new Callback<RestaurantItemsResponse>() {
            @Override
            public void onResponse(Call<RestaurantItemsResponse> call, Response<RestaurantItemsResponse> response) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Restaurants Details not getting \nError : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<RestaurantItemsResponse> call, Throwable t) {

                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Restaurants Details not getting \nError : " + t.getMessage());

            }
        });
        return liveData;
    }

    public LiveData<SubCategoriesResponse> subCategoryItems(AppListener listener, SubCategoriesResponse response) {

        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting restaurants...");
        MutableLiveData<SubCategoriesResponse> liveData = new MutableLiveData<>();

        apiInterface.subCategoriesListAPI(response).enqueue(new Callback<SubCategoriesResponse>() {
            @Override
            public void onResponse(Call<SubCategoriesResponse> call, Response<SubCategoriesResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Restaurants Details not getting \nError : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<SubCategoriesResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Restaurants Details not getting \nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<CommonResponse> saveAddress(AppListener listener, SaveAddressResponse saveAddressResponse) {


        listener.showLoading(AppConstants.SLIDER_ANIMATION, "Saving address...");
        MutableLiveData<CommonResponse> liveData = new MutableLiveData<>();


        apiInterface.saveAddressAPI(saveAddressResponse).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    Log.e("error==>", "" + response.toString());
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Error : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "\nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<AddressListResponse> getUserAddress(AppListener listener, AddressListResponse response) {

        listener.showLoading(AppConstants.SLIDER_ANIMATION, "getting address please...");
        MutableLiveData<AddressListResponse> liveData = new MutableLiveData<>();

        apiInterface.userAddressAPI(response).enqueue(new Callback<AddressListResponse>() {
            @Override
            public void onResponse(Call<AddressListResponse> call, Response<AddressListResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Restaurants Details not getting \nError : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AddressListResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Restaurants Details not getting \nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<CommonResponse> saveGroceryOrder(AppListener listener, SaveOrderResponse response, boolean cartType) {

        listener.showLoading(AppConstants.SLIDER_ANIMATION, "Placing Order...");
        MutableLiveData<CommonResponse> liveData = new MutableLiveData<>();

        if (cartType) {
            saveCall = apiInterface.placeFoodOrderAPI(response);
        } else {
            saveCall = apiInterface.placeGroceryOrderAPI(response);
        }

        Log.e("api==> " + cartType, saveCall.request().toString());

        saveCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    Log.e("error==>", "" + response.toString());
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Error : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "\nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<CommonResponse> pinCodeSearch(AppListener listener, CommonResponse pinCode) {
        listener.showLoading(AppConstants.SLIDER_ANIMATION, "Checking pincode...");
        MutableLiveData<CommonResponse> liveData = new MutableLiveData<>();


        apiInterface.postalCodeAvailability(pinCode).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    Log.e("error==>", "" + response.toString());
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Error : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "\nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<MyOrdersPOJO> myOrdersList(AppListener listener, MyOrdersPOJO response) {
        listener.showLoading(AppConstants.SLIDER_ANIMATION, "Loading my orders...");
        MutableLiveData<MyOrdersPOJO> liveData = new MutableLiveData<>();


        apiInterface.myOrdersAPI(response).enqueue(new Callback<MyOrdersPOJO>() {
            @Override
            public void onResponse(Call<MyOrdersPOJO> call, Response<MyOrdersPOJO> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    Log.e("error==>", "" + response.toString());
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Error : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<MyOrdersPOJO> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "\nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<FoodMyOrdersPOJO> myFoodOrders(AppListener listener, FoodMyOrdersPOJO response) {

        listener.showLoading(AppConstants.SLIDER_ANIMATION, "Loading my orders...");
        MutableLiveData<FoodMyOrdersPOJO> liveData = new MutableLiveData<>();

        apiInterface.myFoodOrdersAPI(response).enqueue(new Callback<FoodMyOrdersPOJO>() {
            @Override
            public void onResponse(Call<FoodMyOrdersPOJO> call, Response<FoodMyOrdersPOJO> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));


                if (response.body() != null && response.code() == 200) {
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                    Log.e("error==>", "" + response.toString());
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Error : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<FoodMyOrdersPOJO> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.SLIDER_ANIMATION);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "\nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<CancelOrderResponse> cancelOrder(AppListener listener, CancelOrderResponse response, int api) {

        listener.showLoading(AppConstants.LOADING_PROGRESS_DIALOG, "Cancel order...");
        MutableLiveData<CancelOrderResponse> liveData = new MutableLiveData<>();


        if (api == AppConstants.API_CANCEL_FOOD_ORDER) {
            cancelOrderCall = apiInterface.cancelFoodOrderAPI(response);
        } else if (api == AppConstants.API_CANCEL_GROCERY_ORDER) {
            cancelOrderCall = apiInterface.cancelGroceryOrderAPI(response);
        }

        Log.e("api==>", cancelOrderCall.request().toString());

        cancelOrderCall.enqueue(new Callback<CancelOrderResponse>() {
            @Override
            public void onResponse(Call<CancelOrderResponse> call, Response<CancelOrderResponse> response) {
                Log.e("response==>", new Gson().toJson(response.body()));
                listener.hideLoading(AppConstants.LOADING_PROGRESS_DIALOG);

                if (response.body() != null && response.code() == 200) {

                    liveData.setValue(response.body());
                } else {
                    liveData.setValue(null);
                    Log.e("error==>", "" + response.toString());
                    listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "Error : " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CancelOrderResponse> call, Throwable t) {
                Log.e("error==>", t.getMessage());

                liveData.setValue(null);
                listener.hideLoading(AppConstants.LOADING_PROGRESS_DIALOG);
                listener.showMsg(AppConstants.MSG_VIEWS_API_ERROR, AppConstants.ACTION_STAY, "\nError : " + t.getMessage());

            }
        });

        return liveData;
    }

    public LiveData<PostalCodeSearchResponse> onPostalCodeSearch(AppListener listener, PostalCodeSearchResponse response) {

        MutableLiveData<PostalCodeSearchResponse> liveData = new MutableLiveData<>();

        apiInterface.postalCodeSearch(response).enqueue(new Callback<PostalCodeSearchResponse>() {
            @Override
            public void onResponse(Call<PostalCodeSearchResponse> call, Response<PostalCodeSearchResponse> response) {
                Log.e("api==>", call.request().toString());
                Log.e("response==>", new Gson().toJson(response.body()));
                listener.showLoading(AppConstants.LOADING_PROGRESS_DIALOG, "Cancel order...");
                if (response.body() != null && response.code() == 200) {

                    liveData.setValue(response.body());
                } else {
                    listener.hideLoading(AppConstants.LOADING_PROGRESS_DIALOG);
                    liveData.setValue(null);
                    Log.e("error==>", "" + response.toString());
                }

            }

            @Override
            public void onFailure(Call<PostalCodeSearchResponse> call, Throwable t) {
                Log.e("api==>", call.request().toString());
                Log.e("error==>", t.getMessage());
                listener.hideLoading(AppConstants.LOADING_PROGRESS_DIALOG);
                liveData.setValue(null);
            }
        });

        return liveData;
    }

    public LiveData<AlertResponse> onInstructions(AppListener listener, AlertResponse response) {


        MutableLiveData<AlertResponse> liveData = new MutableLiveData<>();
        try {
            apiInterface.instructionsAPI(response).enqueue(new Callback<AlertResponse>() {
                @Override
                public void onResponse(Call<AlertResponse> call, Response<AlertResponse> response) {
                    Log.e("api==>", call.request().toString());
                    Log.e("response==>", new Gson().toJson(response.body()));
                    listener.showLoading(AppConstants.LOADING_PROGRESS_DIALOG, "Cancel order...");
                    if (response.body() != null && response.code() == 200) {

                        liveData.setValue(response.body());
                    } else {
                        listener.hideLoading(AppConstants.LOADING_PROGRESS_DIALOG);
                        liveData.setValue(null);
                        Log.e("error==>", "" + response.toString());
                    }

                }

                @Override
                public void onFailure(Call<AlertResponse> call, Throwable t) {
                    Log.e("api==>", call.request().toString());
                    Log.e("error==>", t.getMessage());
                    listener.hideLoading(AppConstants.LOADING_PROGRESS_DIALOG);
                    liveData.setValue(null);
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return liveData;

    }

}
