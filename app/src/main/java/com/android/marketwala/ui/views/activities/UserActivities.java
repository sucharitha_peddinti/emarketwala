package com.android.marketwala.ui.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ActivityUserActivitiesBinding;
import com.android.marketwala.ui.views.fragments.ForgotPasswordFragment;
import com.android.marketwala.ui.views.fragments.LoginFragment;
import com.android.marketwala.ui.views.fragments.RegisterFragment;

public class UserActivities extends AppCompatActivity {

    ActivityUserActivitiesBinding binding;
    String screenFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_activities);
        Utilities.startAnimation(this);
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                screenFrom = bundle.getString(AppConstants.SCREEN_FROM);
            }
        }
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        displayView();

    }

    @SuppressLint("SetTextI18n")
    private void displayView() {
        switch (screenFrom) {
            case AppConstants.SCREEN_LOGIN:
                callFragment(new LoginFragment(), screenFrom);
                break;

            case AppConstants.SCREEN_FORGOT:
                callFragment(new ForgotPasswordFragment(), screenFrom);
                break;

            case AppConstants.SCREEN_REGISTER:
                callFragment(new RegisterFragment(), screenFrom);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Log.e("" + AppConstants.TAG, "onBackPressed: ");
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
        Log.d(AppConstants.TAG, "Empty");

    }

    private void callFragment(Fragment fragment, String tag) {
        FragmentUtilities.replaceFragment(this, fragment, tag);
    }

}