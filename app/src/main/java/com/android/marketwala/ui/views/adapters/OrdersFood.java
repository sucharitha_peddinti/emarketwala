package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.databinding.MyOrderListItemBinding;
import com.android.marketwala.ui.models.FoodMyOrdersPOJO;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class OrdersFood extends RecyclerView.Adapter<OrdersFood.ProductsVH> {

    List<FoodMyOrdersPOJO.FoodMyOrdersData> itemsData = new ArrayList<>();
    Activity activity;
    ItemClick click;


    public OrdersFood(Activity activity, List<FoodMyOrdersPOJO.FoodMyOrdersData> itemsData, ItemClick click) {
        this.itemsData = itemsData;
        this.activity = activity;
        this.click = click;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.my_order_list_item, parent, false));
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        FoodMyOrdersPOJO.FoodMyOrdersData data = itemsData.get(position);
        holder.binding.nameTVID.setText(data.getItem_name());
        holder.binding.quantityTVID.setText("Quantity : " + data.getQuantity());
        holder.binding.priceTVID.setText("\u20b9 " + data.getPrice());
        holder.binding.createdDateTVID.setText(data.getCreated_date());
        holder.binding.deliveryStatusTVID.setText(data.getDelivery_status());




        Glide.with(activity).load(data.getItem_image()).error(R.drawable.logo_green)
                .placeholder(R.drawable.logo_green).
                into(holder.binding.imageID);

        if (position == itemsData.size() - 1) {
            holder.binding.viewID.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        MyOrderListItemBinding binding;

        public ProductsVH(@NonNull MyOrderListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


    public interface ItemClick {
        void onItemCancel(FoodMyOrdersPOJO.FoodMyOrdersData itemData);
    }
}
