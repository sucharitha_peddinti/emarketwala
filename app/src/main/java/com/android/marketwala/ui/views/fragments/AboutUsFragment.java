package com.android.marketwala.ui.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.databinding.FragmentAboutUsBinding;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.volley.ApiConnection;
import com.android.marketwala.ui.views.volley.Helper;
import com.android.volley.VolleyError;
import com.google.android.material.appbar.AppBarLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AboutUsFragment extends Fragment {

    FragmentAboutUsBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity.isFood = false;

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_us, container, false);

        View view = binding.getRoot();

        getaboutus();
        return view;
    }


    private void getaboutus() {


        Map<String, String> customproductDetails = new HashMap<>();
        customproductDetails.put("api_key", "25eeb7fd-d796-fe2a-359a-60507ad6ce6d");


        Log.e("customproductDetails", "" + customproductDetails);
        new ApiConnection(getActivity()).ParseVolleyJsonObject(AppConstants.BASE_URL + "/" + AppConstants.FRAGMENT_Get_ABOUT_US, customproductDetails, new Helper() {
            @Override
            public void backResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);

                    if (response != null) {
                        JSONObject res1 = res.getJSONObject("Response");


                        String content = res1.getString("content");
                        Log.e("res content", "" + content);
                        String plain = Html.fromHtml(content).toString();

                        binding.aboutusText.setText("" + plain);
                    } else {
                        // noRecordsLayout.setVisibility(View.VISIBLE);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                        builder.setTitle("Error");
                        builder.setMessage(res.getString("msg"));
                        builder.setPositiveButton("OK", null);
                        builder.create().show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void backErrorResponse(VolleyError response) {
                Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }

        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

}
