package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.MainProductListItemsBinding;
import com.android.marketwala.databinding.TypesListItemBinding;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.StaticData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsVH> {

    ArrayList<HomeCategoriesResponse.Products> itemsData = new ArrayList<>();

    ItemClick click;

    public ItemsAdapter(ArrayList<HomeCategoriesResponse.Products> itemsData, ItemClick click) {
        this.itemsData = itemsData;
        this.click = click;
    }

    @NonNull
    @Override
    public ItemsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.main_product_list_items, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemsVH holder, int position) {
        holder.binding.setProductsData(itemsData.get(position));
        holder.binding.getRoot().setOnClickListener(v -> click.onItemClick(itemsData.get(position)));

        Utilities.strikeText(holder.binding.amountTVID);


        String offer = itemsData.get(position).getOffer_percentage();
        if (offer != null && !offer.equals("")) {
            holder.binding.offerTVID.setVisibility(View.VISIBLE);
            holder.binding.offerTVID.setText(offer + "%");
        } else {
            holder.binding.offerTVID.setVisibility(View.GONE);
        }

        Glide
                .with(holder.binding.imageID.getContext())
                .load(itemsData.get(position).getProduct_image())
                .placeholder(R.drawable.logo_green)
                .error(R.drawable.logo_green)
                .into(holder.binding.imageID);

    }

    @Override
    public int getItemCount() {
        return Math.min(itemsData.size(), 3);
    }

    public static class ItemsVH extends RecyclerView.ViewHolder {

        MainProductListItemsBinding binding;

        public ItemsVH(@NonNull MainProductListItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface ItemClick {
        void onItemClick(HomeCategoriesResponse.Products itemsData);
    }
}
