package com.android.marketwala.ui.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentSubCategoriesBinding;
import com.android.marketwala.ui.cartDB.MarketWalaDB;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.SubCategoriesResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.ProductsAdapter;
import com.android.marketwala.ui.views.adapters.SubCategoriesAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

public class SubCategoriesFragment extends Fragment implements AppListener, SubCategoriesAdapter.ItemClick {


    FragmentSubCategoriesBinding binding;

    AppViewModel viewModel;
    String categoryID;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    public SubCategoriesFragment getData(String categoriesData) {
        SubCategoriesFragment fragment = new SubCategoriesFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", categoriesData);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryID =  bundle.getString("key");
        }
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sub_categories, container, false);
        View view = binding.getRoot();
        MainActivity.isFood = false;




        viewModel = new AppViewModel(this);

        SubCategoriesResponse response = new SubCategoriesResponse();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setCategory_id(categoryID);



        viewModel.subCategoriesData(response);
        viewModel.getSubCategories().observe(getActivity(),subCategoriesResponse -> {
            if (subCategoriesResponse.isStatus()){
                ArrayList<SubCategoriesResponse.SubCategoriesData>data=subCategoriesResponse.getResponse();
                if (data.size()>0){
                    binding.productsRCID.setVisibility(View.VISIBLE);
                    binding.subcatProgress.setVisibility(View.GONE);
                    SubCategoriesAdapter adapter=new SubCategoriesAdapter(data,this);
                    binding.productsRCID.setAdapter(adapter);
                }else{
                    setErrorViews();
                }
            }else{
                setErrorViews();
            }
        });

        return view;
    }

    private void setErrorViews() {
        binding.productsRCID.setVisibility(View.GONE);
        binding.subcatProgress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {

        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.subcatProgress.setVisibility(View.VISIBLE);
          //  binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();
        }
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.subcatProgress.setVisibility(View.GONE);
           // binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }


    @Override
    public void onItemClick(SubCategoriesResponse.SubCategoriesData itemsData) {
        ProductsFragment fragment = new ProductsFragment().getData(itemsData.getSubcat_id(),itemsData.getSubcat_name());
        FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_PRODUCTS);
    }
}
