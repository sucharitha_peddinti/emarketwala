package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class FoodMyOrdersPOJO {

    /*
    "api_key":"25eeb7fd-d796-fe2a-359a-60507ad6ce6d",
"user_id":"186
    "Status": true,
    "Message": "Success",
    "Response"*/

    private String api_key;
    private String user_id;
    private String Message;
    private boolean Status;
    private ArrayList<FoodMyOrdersResponse> Response=new ArrayList<>();

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public ArrayList<FoodMyOrdersResponse> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<FoodMyOrdersResponse> response) {
        Response = response;
    }

    public static class FoodMyOrdersResponse {
        private String id;
        private String payment_type;
        private String order_number;
        private String order_amount;
        private String email_id;
        private String mobile_number;
        private String captured;
        private ArrayList<FoodMyOrdersData> product_details = new ArrayList<>();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getOrder_number() {
            return order_number;
        }

        public void setOrder_number(String order_number) {
            this.order_number = order_number;
        }

        public String getOrder_amount() {
            return order_amount;
        }

        public void setOrder_amount(String order_amount) {
            this.order_amount = order_amount;
        }

        public String getEmail_id() {
            return email_id;
        }

        public void setEmail_id(String email_id) {
            this.email_id = email_id;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getCaptured() {
            return captured;
        }

        public void setCaptured(String captured) {
            this.captured = captured;
        }

        public ArrayList<FoodMyOrdersData> getProduct_details() {
            return product_details;
        }

        public void setProduct_details(ArrayList<FoodMyOrdersData> product_details) {
            this.product_details = product_details;
        }
    }

    public static class FoodMyOrdersData {


        private String id;
        private String order_number;
        private String user_id;
        private String item_id;
        private String item_name;
        private String item_image;
        private String quantity;
        private String item_type;
        private String price;
        private String address;
        private String payment_status;
        private String delivery_status;
        private String cancelled_by;
        private String delivery_date;
        private String created_date;
        private String updated_date;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_number() {
            return order_number;
        }

        public void setOrder_number(String order_number) {
            this.order_number = order_number;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getItem_image() {
            return item_image;
        }

        public void setItem_image(String item_image) {
            this.item_image = item_image;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getItem_type() {
            return item_type;
        }

        public void setItem_type(String item_type) {
            this.item_type = item_type;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getDelivery_status() {
            return delivery_status;
        }

        public void setDelivery_status(String delivery_status) {
            this.delivery_status = delivery_status;
        }

        public String getCancelled_by() {
            return cancelled_by;
        }

        public void setCancelled_by(String cancelled_by) {
            this.cancelled_by = cancelled_by;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public void setDelivery_date(String delivery_date) {
            this.delivery_date = delivery_date;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
