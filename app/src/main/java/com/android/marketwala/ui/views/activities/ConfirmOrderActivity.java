package com.android.marketwala.ui.views.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ActivityConfirmOrderBinding;
import com.android.marketwala.domain.ApiConnection;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.models.AddressData;
import com.android.marketwala.ui.models.SaveOrderResponse;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.adapters.CartFinalAdapter;
import com.android.marketwala.ui.views.volley.Helper;
import com.android.volley.VolleyError;
import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.paymentparamhelper.PaymentParams;
import com.payu.paymentparamhelper.PostData;
import com.payu.payuui.Activity.PayUBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfirmOrderActivity extends AppCompatActivity implements AppListener {

    double sum = 0;
    double oldsum = 0;
    String disc_amount;
    String min_cart_value;
    String coupon_code;
    ActivityConfirmOrderBinding binding;
    CartViewModel viewModel;
    List<CartItems> cartData = new ArrayList<>();
    List<CartItems> cartItemsList = new ArrayList<>();
    AppViewModel appVM;
    StringBuilder productIDSB;
    StringBuilder quantitySB;
    UserLoginData loginData;
    AddressData data;
    int removeamount;
    boolean cart_type;
    String paymentType = "cod";
    // These will hold all the payment parameters
    private PaymentParams mPaymentParams;
    // This sets the configuration
    private PayuConfig payuConfig;
    private PayUChecksum checksum;
    private String subventionHash;
    private String coupon;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_order);
        Utilities.startAnimation(this);
        data = Utilities.getAddress(this);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                cart_type = bundle.getBoolean("cart_type");
            }
        }

        binding.headerID.headerTitleTVID.setText("Checkout");
        binding.headerID.backImageID.setOnClickListener(v -> Utilities.finishAnimation(this));
        getAddressDetails();
        loadCartData(cart_type);
        Log.e("cartype", "" + cart_type);

        appVM = new AppViewModel(this);
        loginData = Utilities.getUserData(this);

        binding.confirmBtn.setOnClickListener(v -> {

            int selectedId = binding.paymentGroupID.getCheckedRadioButtonId();
            RadioButton radioButton = findViewById(selectedId);

            if (Utilities.isNetworkAvailable(this)) {
                switch (radioButton.getText().toString()) {
                    case "Cash On Delivery":
                        paymentType = "cod";
                        getData(false, "0");
                        break;

                    case "Online":

                        paymentType = "online";
                        payUMoneySetUp();
                        break;
                }
            } else {
                Utilities.noInternet(this);
            }
        });

        binding.applycouponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coupon = binding.applycouponEdittext.getText().toString();

                if (cart_type == true) {
                    coupons_food();

                    Log.e("food cart-->", "" + cart_type);

                } else {
                    Log.e("grocery cart-->", "" + cart_type);

                    coupons_grocery();
                }

            }
        });

        Log.e("type_of_cart-->", "" + cart_type + " , " + loginData.getUser_id());

    }

    //coupons list food


    private void coupons_food() {

        Map<String, String> coponslist = new HashMap<>();
        coponslist.put("api_key", AppConstants.API_TOKEN);
        coponslist.put("coupon_code", coupon);

        new ApiConnection(getApplicationContext()).ParseVolleyJsonObject(AppConstants.BASE_URL + AppConstants.FOOD_COUPONS_LIST, coponslist, new Helper() {
            @Override
            public void backResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);

                    if (response != null) {
                        JSONObject res1 = res.getJSONObject("Response");


                        disc_amount = res1.getString("disc_amount");
                        min_cart_value = res1.getString("min_cart_value");
                        coupon_code = res1.getString("coupon_code");

                        int dd = (int) (sum - Integer.parseInt(disc_amount));
                        removeamount = (int) (sum + Integer.parseInt(disc_amount));
                        Log.e("removeamount", "" + removeamount);
                        Log.e("min_cart_value", "" + min_cart_value);

                        if (sum >= Integer.parseInt(min_cart_value)) {
                            Toast.makeText(getApplicationContext(), "Coupon Applied Sucessfully", Toast.LENGTH_SHORT).show();

                            binding.couponTVID.setText("\u20b9 " + disc_amount);
                            binding.coupontext.setText("Coupon Applied Sucessfully");
                            binding.totalPriceTVID.setText("\u20b9 " + dd);
                            binding.coupon.setText("Coupon " + "(" + coupon_code + ")");
                            binding.coupontext.setTextColor(getResources().getColor(R.color.green));

                            binding.couponRLID.setVisibility(View.VISIBLE);
                            binding.coupontext.setVisibility(View.VISIBLE);
                            Log.e("dd", "" + dd);
                        } else {

                            binding.couponRLID.setVisibility(View.GONE);
                            binding.coupontext.setVisibility(View.VISIBLE);
                            binding.coupontext.setText("Minimum cart amount should be " + min_cart_value);
                            binding.coupontext.setTextColor(getResources().getColor(R.color.red));
                            binding.totalPriceTVID.setText("\u20b9 " + oldsum);
                            Toast.makeText(getApplicationContext(), "Minimum cart amount should be " + min_cart_value, Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        // noRecordsLayout.setVisibility(View.VISIBLE);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getApplicationContext());
                        builder.setTitle("Error");
                        builder.setMessage(res.getString("msg"));
                        builder.setPositiveButton("OK", null);
                        builder.create().show();
                        binding.couponRLID.setVisibility(View.GONE);
                        binding.coupontext.setVisibility(View.VISIBLE);
                        binding.coupontext.setText("Invalid Coupon");
                        binding.coupontext.setTextColor(getResources().getColor(R.color.red));
                        binding.totalPriceTVID.setText("\u20b9 " + oldsum);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    binding.couponRLID.setVisibility(View.GONE);
                    binding.coupontext.setVisibility(View.VISIBLE);
                    binding.coupontext.setText("Invalid Coupon");
                    binding.coupontext.setTextColor(getResources().getColor(R.color.red));
                    binding.totalPriceTVID.setText("\u20b9 " + oldsum);
                    Log.e("old am", "" + oldsum);
                    Toast.makeText(getApplicationContext(), "Invalid Coupon", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void backErrorResponse(VolleyError response) {
                Toast.makeText(getApplicationContext(), "Invalid Coupon", Toast.LENGTH_SHORT).show();
                binding.couponRLID.setVisibility(View.GONE);
                binding.coupontext.setVisibility(View.GONE);
                binding.coupontext.setText("Invalid Coupon");
                binding.totalPriceTVID.setText("\u20b9 " + oldsum);


                binding.coupontext.setTextColor(getResources().getColor(R.color.red));

            }

        });

    }

    private void coupons_grocery() {

        Map<String, String> coponslist = new HashMap<>();
        coponslist.put("api_key", AppConstants.API_TOKEN);
        coponslist.put("coupon_code", coupon);

        new ApiConnection(getApplicationContext()).ParseVolleyJsonObject(AppConstants.BASE_URL + AppConstants.COUPONS_LIST, coponslist, new Helper() {
            @Override
            public void backResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);

                    if (response != null) {
                        JSONObject res1 = res.getJSONObject("Response");

                        disc_amount = res1.getString("disc_amount");
                        min_cart_value = res1.getString("min_cart_value");
                        coupon_code = res1.getString("coupon_code");

                        int dd = (int) (sum - Integer.parseInt(disc_amount));
                        removeamount = (int) (sum + Integer.parseInt(disc_amount));

                        Log.e("removeamount", "" + removeamount);
                        Log.e("min_cart_value", "" + min_cart_value);

                        if (sum >= Integer.parseInt(min_cart_value)) {
                            Toast.makeText(getApplicationContext(), "Coupon Applied Sucessfully", Toast.LENGTH_SHORT).show();

                            binding.couponTVID.setText("\u20b9 " + disc_amount);
                            binding.coupontext.setText("Coupon Applied Sucessfully");
                            binding.totalPriceTVID.setText("\u20b9 " + dd);
                            binding.coupon.setText("Coupon " + "(" + coupon_code + ")");
                            binding.coupontext.setTextColor(getResources().getColor(R.color.green));

                            binding.couponRLID.setVisibility(View.VISIBLE);
                            binding.coupontext.setVisibility(View.VISIBLE);
                            Log.e("dd", "" + dd);
                        } else {

                            binding.couponRLID.setVisibility(View.GONE);
                            binding.coupontext.setVisibility(View.VISIBLE);
                            binding.coupontext.setText("Minimum cart amount should be " + min_cart_value);
                            binding.coupontext.setTextColor(getResources().getColor(R.color.red));
                            binding.totalPriceTVID.setText("\u20b9 " + oldsum);
                            Toast.makeText(getApplicationContext(), "Minimum cart amount should be " + min_cart_value, Toast.LENGTH_SHORT).show();

                        }

                    } else {

                        // noRecordsLayout.setVisibility(View.VISIBLE);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getApplicationContext());
                        builder.setTitle("Error");
                        builder.setMessage(res.getString("msg"));
                        builder.setPositiveButton("OK", null);
                        builder.create().show();
                        binding.couponRLID.setVisibility(View.GONE);
                        binding.coupontext.setVisibility(View.VISIBLE);
                        binding.coupontext.setText("Invalid Coupon");
                        binding.coupontext.setTextColor(getResources().getColor(R.color.red));
                        binding.totalPriceTVID.setText("\u20b9 " + oldsum);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    binding.couponRLID.setVisibility(View.GONE);
                    binding.coupontext.setVisibility(View.VISIBLE);
                    binding.coupontext.setText("Invalid Coupon");
                    binding.coupontext.setTextColor(getResources().getColor(R.color.red));
                    binding.totalPriceTVID.setText("\u20b9 " + oldsum);
                    Log.e("old am", "" + oldsum);

                    Toast.makeText(getApplicationContext(), "Invalid Coupon", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void backErrorResponse(VolleyError response) {
                Toast.makeText(getApplicationContext(), "Invalid Coupon", Toast.LENGTH_SHORT).show();
                binding.couponRLID.setVisibility(View.GONE);
                binding.coupontext.setVisibility(View.GONE);
                binding.coupontext.setText("Invalid Coupon");
                binding.totalPriceTVID.setText("\u20b9 " + oldsum);

                binding.coupontext.setTextColor(getResources().getColor(R.color.red));

            }

        });

    }

    private void payUMoneySetUp() {
        Log.e("payment==>", "called");
        int environment = PayuConstants.PRODUCTION_ENV;


        //String amount = binding.totalPriceTVID.getText().toString().replace("\u20b9 ", "");
        String amount = "1";
        String email = loginData.getEmail_id();


        String merchantKey = AppConstants.PAY_U_MERCHANT;
        String saltKey = AppConstants.PAY_U_SALT;
        String userCredentials = merchantKey + ":" + email;

        //TODO Below are mandatory params for hash genetation
        mPaymentParams = new PaymentParams();


        /**
         * For Test Environment, merchantKey = please contact mobile.integration@payu.in with your app name and registered email id

         */
        mPaymentParams.setKey(merchantKey);
        mPaymentParams.setAmount(amount);
        mPaymentParams.setProductInfo("product_info");
        mPaymentParams.setFirstName(loginData.getName());
        mPaymentParams.setEmail(loginData.getEmail_id());
        mPaymentParams.setPhone(loginData.getMobile_number());

        mPaymentParams.setSubventionAmount(amount);
        mPaymentParams.setSubventionEligibility("all");

        /*
         * Transaction Id should be kept unique for each transaction.
         * */
        mPaymentParams.setTxnId("" + System.currentTimeMillis());
        // mPaymentParams.setTxnId("1587113659761");

        /**
         * Surl --> Success url is where the transaction response is posted by PayU on successful transaction
         * Furl --> Failre url is where the transaction response is posted by PayU on failed transaction
         */
        // mPaymentParams.setSurl(" https://www.fitternity.com/paymentsuccessandroid");
        mPaymentParams.setSurl("https://payuresponse.firebaseapp.com/success");
        mPaymentParams.setFurl("https://payuresponse.firebaseapp.com/failure");
        //  mPaymentParams.setFurl("https://www.fitternity.com/paymentsuccessandroid");
        mPaymentParams.setNotifyURL(mPaymentParams.getSurl());  //for lazy pay

        mPaymentParams.setUdf1("udf1");
        mPaymentParams.setUdf2("udf2");
        mPaymentParams.setUdf3("udf3");
        mPaymentParams.setUdf4("udf4");
        mPaymentParams.setUdf5("udf5");


        /**
         * These are used for store card feature. If you are not using it then user_credentials = "default"
         * user_credentials takes of the form like user_credentials = "merchant_key : user_id"
         * here merchant_key = your merchant key,
         * user_id = unique id related to user like, email, phone number, etc.
         * */
        mPaymentParams.setUserCredentials(userCredentials);

        //TODO Pass this param only if using offer key
        // mPaymentParams.setOfferKey("YONOYSF@6445");

        //TODO Sets the payment environment in PayuConfig object
        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(environment);
        //TODO It is recommended to generate hash from server only. Keep your key and salt in server side hash generation code.
        // generateHashFromServer(mPaymentParams);


        generateHashFromSDK(mPaymentParams, saltKey);


    }

    public void generateHashFromSDK(PaymentParams mPaymentParams, String salt) {
        PayuHashes payuHashes = new PayuHashes();
        PostData postData = new PostData();

        checksum = null;
        checksum = new PayUChecksum();
        checksum.setAmount(mPaymentParams.getAmount());
        checksum.setKey(mPaymentParams.getKey());
        checksum.setTxnid(mPaymentParams.getTxnId());
        checksum.setEmail(mPaymentParams.getEmail());
        checksum.setSalt(salt);
        checksum.setProductinfo(mPaymentParams.getProductInfo());
        checksum.setFirstname(mPaymentParams.getFirstName());
        checksum.setUdf1(mPaymentParams.getUdf1());
        checksum.setUdf2(mPaymentParams.getUdf2());
        checksum.setUdf3(mPaymentParams.getUdf3());
        checksum.setUdf4(mPaymentParams.getUdf4());
        checksum.setUdf5(mPaymentParams.getUdf5());
        postData = checksum.getHash();


        if (postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setPaymentHash(postData.getResult());
        }

        if (mPaymentParams.getSubventionAmount() != null && !mPaymentParams.getSubventionAmount().isEmpty()) {
            subventionHash = calculateHash("" + mPaymentParams.getKey() + "|" + mPaymentParams.getTxnId() + "|" + mPaymentParams.getAmount() + "|" + mPaymentParams.getProductInfo() + "|" + mPaymentParams.getFirstName() + "|" + mPaymentParams.getEmail() + "|" + mPaymentParams.getUdf1() + "|" + mPaymentParams.getUdf2() + "|" + mPaymentParams.getUdf3() + "|" + mPaymentParams.getUdf4() + "|" + mPaymentParams.getUdf5() + "||||||" + salt + "|" + mPaymentParams.getSubventionAmount());
        }

        String var1 = mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials();
        String key = mPaymentParams.getKey();

        if ((postData = calculateHash(key, PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // Assign post data first then check for success
            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(postData.getResult());
        //vas
        if ((postData = calculateHash(key, PayuConstants.VAS_FOR_MOBILE_SDK, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setVasForMobileSdkHash(postData.getResult());

        // getIbibocodes
        if ((postData = calculateHash(key, PayuConstants.GET_MERCHANT_IBIBO_CODES, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setMerchantIbiboCodesHash(postData.getResult());

        if (!var1.contentEquals(PayuConstants.DEFAULT)) {
            // get user card
            if ((postData = calculateHash(key, PayuConstants.GET_USER_CARDS, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // todo rename storedc ard
                payuHashes.setStoredCardsHash(postData.getResult());
            // save user card
            if ((postData = calculateHash(key, PayuConstants.SAVE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setSaveCardHash(postData.getResult());
            // delete user card
            if ((postData = calculateHash(key, PayuConstants.DELETE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setDeleteCardHash(postData.getResult());
            // edit user card
            if ((postData = calculateHash(key, PayuConstants.EDIT_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setEditCardHash(postData.getResult());
        }

        if (mPaymentParams.getOfferKey() != null) {
            postData = calculateHash(key, PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey(), salt);
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                payuHashes.setCheckOfferStatusHash(postData.getResult());
            }
        }

        if (mPaymentParams.getOfferKey() != null && (postData = calculateHash(key, PayuConstants.CHECK_OFFER_STATUS, mPaymentParams.getOfferKey(), salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setCheckOfferStatusHash(postData.getResult());
        }

        // we have generated all the hases now lest launch sdk's ui
        launchSdkUI(payuHashes);

    }

    /******************************
     * Client hash generation
     ***********************************/
    // Do not use this, you may use this only for testing.
    // lets generate hashes.
    // This should be done from server side..
    // Do not keep salt anywhere in app.
    private PostData calculateHash(String key, String command, String var1, String salt) {
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setKey(key);
        checksum.setCommand(command);
        checksum.setVar1(var1);
        checksum.setSalt(salt);
        return checksum.getHash();
    }

    private String calculateHash(String hashString) {
        try {
            StringBuilder hash = new StringBuilder();
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(hashString.getBytes());
            byte[] mdbytes = messageDigest.digest();
            for (byte hashByte : mdbytes) {
                hash.append(Integer.toString((hashByte & 0xff) + 0x100, 16).substring(1));
            }
            return hash.toString();
        } catch (Exception e) {
            return "ERROR";
        }
    }

    /**
     * This method adds the Payuhashes and other required params to intent and launches the PayuBaseActivity.java
     *
     * @param payuHashes it contains all the hashes generated from merchant server
     */
    public void launchSdkUI(PayuHashes payuHashes) {

        Log.e("payment==>", "called set");


        Intent intent = new Intent(this, PayUBaseActivity.class);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
        intent.putExtra(PayuConstants.SALT, AppConstants.PAY_U_SALT);

        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);

        /*Intent intent= new Intent(this, PayUBaseActivity.class);
intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
intent.putExtra(PayuConstants.SALT, salt);*/
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                Log.e("payment==>", "onActivityResult  " + PayuConstants.PAYU_REQUEST_CODE);
                /**
                 * Here, data.getStringExtra("payu_response") ---> Implicit response sent by PayU
                 * data.getStringExtra("result") ---> Response received from merchant's Surl/Furl
                 *
                 * PayU sends the same response to merchant server and in app. In response check the value of key "status"
                 * for identifying status of transaction. There are two possible status like, success or failure
                 * */

                Log.e("payment_data==>", "" + data.getStringExtra("payu_response"));

                getData(true, data.getStringExtra("payu_response"));

            } else {
                Toast.makeText(this, getString(R.string.could_not_receive_data), Toast.LENGTH_LONG).show();
            }
        } else {
            Utilities.showToast(this, "Payment failed");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        String refresh = MySharedPreferences.getPreferences(this, AppConstants.REFRESH);
        if (refresh.equalsIgnoreCase(AppConstants.YES)) {
            MySharedPreferences.setPreference(this, AppConstants.REFRESH, AppConstants.NO);
            getAddressDetails();
        }
    }


    private void getData(boolean orderPaymentType, String paymentDetails) {

        quantitySB = new StringBuilder();
        productIDSB = new StringBuilder();

        for (CartItems eachstring : cartItemsList) {
            productIDSB.append(eachstring.getProduct_id()).append("##");
            quantitySB.append(eachstring.getCartQty()).append("##");
        }

        String productIDStr = productIDSB.toString();
        String productQtyStr = quantitySB.toString();


        if (productIDStr.length() > 0)
            productIDStr = productIDStr.substring(0, productIDStr.length() - 2);


        if (productQtyStr.length() > 0)
            productQtyStr = productQtyStr.substring(0, productQtyStr.length() - 2);


        Log.e("quantity==>", "" + productIDStr);
        Log.e("ids==>", "" + productQtyStr);

        SaveOrderResponse orderResponse = new SaveOrderResponse();
        orderResponse.setApi_key(AppConstants.API_TOKEN);
        orderResponse.setAddress_id(data.getAddress_id());


        Log.e("address--->", data.getAddress_id() + " , " + paymentType);

        if (cart_type) {
            orderResponse.setItem_ids(productIDStr);
        } else {
            orderResponse.setProduct_ids(productIDStr);
        }

        orderResponse.setOrder_type(paymentType);
        orderResponse.setQuantities(productQtyStr);
        orderResponse.setUser_id(loginData.getUser_id());
        orderResponse.setTotal_amount(binding.totalPriceTVID.getText().toString().replace("\u20b9 ", ""));

        if (orderPaymentType) {


            /*{"id":14287535325,"mode":"UPI","status":"success","unmappedstatus":"captured",
            "key":"QBCYId","txnid":"1637480999588","transaction_fee":"1.00",
            "amount":"1.00","discount":"0.00","addedon":"2021-11-21 13:20:38",
            "productinfo":"product_info","firstname":"venkei",
            "email":"test@gmail.com","phone":"1234567890",
            "udf1":"udf1","udf2":"udf2","udf3":"udf3","udf4":"udf4","udf5":"udf5",
            "hash":"fe76191fc7b3bd21a3ffe351aeab7eea3af8ee4192ec95e242283c20ecf36e3a673799fc4b9f09d92fbd6f734cfa1c490221cf9ce0d927f705bfd98f3a023301",
            "field1":"9618675200@ybl","field2":"126858","field3":"9618675200@ybl",
            "field4":"MODUMPARAPU VENKANNA BABU","field5":"payumoney.payu@indus",
            "field6":"INDBD1487A6730D23EACE053307C180ACF9",
            "field7":"APPROVED OR COMPLETED SUCCESSFULLY|00",
            "field9":"Success|Completed Using Callback","payment_source":"payu",
            "PG_TYPE":"UPI-PG","bank_ref_no":"132542353224","ibibo_code":"UPI",
            "error_code":"E000","Error_Message":"No Error","is_seamless":1,
            "surl":"https://payuresponse.firebaseapp.com/success",
            "furl":"https://payuresponse.firebaseapp.com/failure"}*/

            try {
                JSONObject paymentOBJ = new JSONObject(paymentDetails);
                orderResponse.setPayuMoneyId(paymentOBJ.getString("id"));
                orderResponse.setHash(paymentOBJ.getString("hash"));
                orderResponse.setAddedon(paymentOBJ.getString("addedon"));
                orderResponse.setError_reason(paymentOBJ.getString("Error_Message"));
                orderResponse.setError(paymentOBJ.getString("error_code"));
                orderResponse.setProductinfo(paymentOBJ.getString("productinfo"));
                Log.e("payment_data==>", "api : " + paymentOBJ.getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        appVM.onPlaceOrder(orderResponse, cart_type);

        appVM.getPlaceOrderResponse().observe(this, commonResponse -> {
            if (commonResponse.isStatus()) {

                Utilities.showToast(this, "Order Placed successfully");
                startActivity(new Intent(this, MainActivity.class));
                finish();
                CartViewModel viewModel = new CartViewModel(this);
                viewModel.delete(cartItemsList.get(0), cart_type, true);

            } else {
                Utilities.showToast(this, commonResponse.getMessage());
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utilities.finishAnimation(this);
    }

    private void loadCartData(boolean isFood) {
        viewModel = new CartViewModel(this, isFood);
        viewModel.cartData();

        CartViewModel cartVM = new CartViewModel(this, false);
        cartVM.cartData();
        cartVM.getGetCartItems().observe(this, cartItems -> {
            cartData = cartItems;
        });


        viewModel.getGetCartItems().observe(this, cartItems -> {
            cartItemsList = cartItems;
            if (cartItems.size() > 0) {
                getTotalPrice(cartItemsList);
                CartFinalAdapter adapter = new CartFinalAdapter(this, cartItems);
                binding.productsRCID.setAdapter(adapter);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getTotalPrice(List<CartItems> cartItemsList) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            sum += Double.parseDouble(cartItemsList.get(i).getPrice()) * Double.parseDouble(cartItemsList.get(i).getCartQty());
            oldsum = sum;
        }

        if (sum < 200) {
            sum = sum + 25;
            binding.shippingRLID.setVisibility(View.VISIBLE);
            binding.shippingTVID.setText("\u20b9 25");
            binding.shippingTVID.setTextColor(getResources().getColor(R.color.black));
        } else {
            binding.shippingTVID.setText("Free");
            binding.shippingTVID.setTextColor(getResources().getColor(R.color.purple_700));
        }

        binding.totalPriceTVID.setText("\u20b9 " + sum);
    }


    @SuppressLint("SetTextI18n")
    private void getAddressDetails() {

        if (data != null) {
            binding.addressTVID.setText(data.getMobile_number() + "," + data.getEmail_id() + "," + data.getAddress() + "," + data.getCity() + "," + data.getState() + "," + data.getPincode() + ".");
        } else {
            binding.locationCVID.setVisibility(View.VISIBLE);
            binding.addressTVID.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Utilities.showSimpleProgressDialog(this, null, loadingMsg, false);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Utilities.removeSimpleProgressDialog();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Utilities.showToast(this, msg);
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }
}