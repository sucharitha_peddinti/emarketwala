package com.android.marketwala.ui.views.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentProfileBinding;
import com.android.marketwala.ui.models.StaticData;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.viewModels.StaticViewModel;
import com.android.marketwala.ui.views.activities.AddAddressActivity;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.activities.ShowAddressActivity;
import com.android.marketwala.ui.views.adapters.ProfileAdapter;
import com.google.android.material.appbar.AppBarLayout;


public class ProfileFragment extends Fragment implements ProfileAdapter.ItemClick {


    FragmentProfileBinding binding;
    ProfileAdapter adapter;
    StaticViewModel viewModel;
    UserLoginData loginResponse;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        View view = binding.getRoot();
        MainActivity.isFood = false;
        loginResponse = Utilities.getUserData(getActivity());

        binding.signInTVID.setText(loginResponse.getName());
        binding.nameID.setText(loginResponse.getName());
        binding.emailTVID.setText(loginResponse.getMobile_number());

        viewModel = new StaticViewModel();
        viewModel.loadProfileMenu();
        viewModel.getProfileData().observe(getActivity(), dataModels -> {
            adapter = new ProfileAdapter(dataModels, this);
            binding.dataRCID.setAdapter(adapter);
        });
        return view;
    }

    @Override
    public void onItemClick(StaticData itemsData) {
        if (itemsData.getId() == 0) {
            logout();
        } else if (itemsData.getId() == 1) {
            callFragment(new CartFragment().getData(MainActivity.isFood), AppConstants.FRAGMENT_CART);
        } else if (itemsData.getId() == 2) {
            callFragment(new MyOrdersFragment(), AppConstants.FRAGMENT_MY_ORDERS);
        } else if (itemsData.getId() == 3) {
            //callFragment(new MyOrdersFragment(), AppConstants.FRAGMENT_MY_ORDERS);

              getActivity().startActivity(new Intent(getActivity(), ShowAddressActivity.class));
        }
    }


    private void callFragment(Fragment fragment, String tag) {
        FragmentUtilities.replaceFragment(getActivity(), fragment, tag);
    }

    private void logout() {
        final Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.alert_logout);

        AppCompatButton alertNoBtn = dialog.findViewById(R.id.noBtn);
        AppCompatButton alertYesBtn = dialog.findViewById(R.id.yesBtn);
        alertNoBtn.setOnClickListener(v -> dialog.dismiss());

        alertYesBtn.setOnClickListener(v -> {
            dialog.dismiss();
            MySharedPreferences.clearPreferences(getActivity());
            Intent newIntent = new Intent(getActivity(), MainActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newIntent);
            Utilities.finishAnimation(getActivity());
        });

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
