package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.SideMenuListItemBinding;
import com.android.marketwala.ui.models.StaticData;

import java.util.ArrayList;

public class SideMenuAdapter extends RecyclerView.Adapter<SideMenuAdapter.SideMenuVH>{

    ArrayList<StaticData>sideMenuData=new ArrayList<>();

    SideMenuClick click;

    public SideMenuAdapter(ArrayList<StaticData>sideMenuData,SideMenuClick click){
        this.sideMenuData=sideMenuData;
        this.click=click;
    }

    @NonNull
    @Override
    public SideMenuVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SideMenuVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.side_menu_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull SideMenuVH holder, int position) {
        holder.binding.setSideMenuData(sideMenuData.get(position));
        holder.binding.getRoot().setOnClickListener(v ->click.onSideMenuItemClick(sideMenuData.get(position)));
    }

    @Override
    public int getItemCount() {
        return sideMenuData.size();
    }

    public static class SideMenuVH extends RecyclerView.ViewHolder {

        SideMenuListItemBinding binding;
        public SideMenuVH(@NonNull SideMenuListItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }

    public interface SideMenuClick{
        void onSideMenuItemClick(StaticData sideMenuData);
    }
}
