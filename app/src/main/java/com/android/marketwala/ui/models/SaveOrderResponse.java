package com.android.marketwala.ui.models;

public class SaveOrderResponse {

    String api_key;
    String user_id;
    String product_ids;
    String quantities;
    String total_amount;
    String item_ids;
    String address_id;


    String order_type;
    String payuMoneyId;
    String hash;
    String addedon;
    String error_reason;
    String error;
    String productinfo;


    /*txnid,payuMoneyId,hash,addedon,error_reason,error,productinfo*/


    public String getItem_ids() {
        return item_ids;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public void setItem_ids(String item_ids) {
        this.item_ids = item_ids;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_ids() {
        return product_ids;
    }

    public void setProduct_ids(String product_ids) {
        this.product_ids = product_ids;
    }

    public String getQuantities() {
        return quantities;
    }

    public void setQuantities(String quantities) {
        this.quantities = quantities;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getPayuMoneyId() {
        return payuMoneyId;
    }

    public void setPayuMoneyId(String payuMoneyId) {
        this.payuMoneyId = payuMoneyId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getAddedon() {
        return addedon;
    }

    public void setAddedon(String addedon) {
        this.addedon = addedon;
    }

    public String getError_reason() {
        return error_reason;
    }

    public void setError_reason(String error_reason) {
        this.error_reason = error_reason;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getProductinfo() {
        return productinfo;
    }

    public void setProductinfo(String productinfo) {
        this.productinfo = productinfo;
    }
}
