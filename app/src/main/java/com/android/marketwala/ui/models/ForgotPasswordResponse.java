package com.android.marketwala.ui.models;

public class ForgotPasswordResponse {

    /* "Status": true,
    "Message": "Success",
    "Response": "Reset Link sent Successfully to Registered Email(test@gmail.com).",
    "code": 1*/

    private boolean Status;
    private String Message;
    private String Response;
    private String code;

    private String api_key;
    private String email_id;

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }
}
