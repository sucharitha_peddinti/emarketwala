package com.android.marketwala.ui.models;

public class ContactusResponse {

    /*"Status": true,
    "Message": "Success",
    "Response":*/

    private boolean Status;
    private String Message;
    private UserLoginData Response;


    private String name;
    private String api_key;
    private String email;
    private String subject;
    private String message;


    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public UserLoginData getResponse() {
        return Response;
    }

    public void setResponse(UserLoginData response) {
        Response = response;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
