package com.android.marketwala.ui.views.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentProductDetailsBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.models.ProductDetailsResponse;
import com.android.marketwala.ui.models.StaticData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.adapters.BannersAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class ProductDetailsFragment extends Fragment implements BannersAdapter.BannersClickListener, AppListener {


    FragmentProductDetailsBinding binding;
    String productID;
    AppViewModel viewModel;

    ProductDetailsResponse.ProductDetailsData detailsDataResponse;


    ProductDetailsResponse detailsResponse = new ProductDetailsResponse();

    ArrayList<String> bannersData = new ArrayList<>();
    BannersAdapter adapter;
    String pic;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false);
        View view = binding.getRoot();
        viewModel = new AppViewModel(this);
        //MainActivity.isFood = false;
        detailsResponse.setApi_key(AppConstants.API_TOKEN);
        detailsResponse.setProduct_id(productID);

        viewModel.productDetailsData(detailsResponse);
        viewModel.getProductDetailsResponseLiveData().observe(getActivity(), productDetailsResponse -> {
            if (productDetailsResponse != null) {
                if (productDetailsResponse.isStatus()) {


                    binding.loadingPB.setVisibility(View.GONE);
                    binding.mainSCVID.setVisibility(View.VISIBLE);
                    binding.noDataTVID.setVisibility(View.GONE);

                    ProductDetailsResponse.ProductDetailsData data = productDetailsResponse.getResponse();
                    detailsDataResponse = data;
                    pic = data.getProduct_image();
                    bannersData = data.getSlider_images();

                    binding.productNameTVID.setText(data.getProduct_name());
                    binding.quantityTVID.setText(data.getQuanty() + " ");
                    try {

                        if (data.getWeight().equals("") || (data.getWeight() == null) || (data.getWeight() == "")) {
                            binding.weighTVID.setText(" ");
                            binding.weightLLID.setVisibility(View.GONE);

                        } else {
                            binding.weighTVID.setText("" + data.getWeight());
                            binding.weightLLID.setVisibility(View.VISIBLE);

                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    binding.priceTVID.setText("\u20B9 " + data.getOffer_price());
                    binding.mrpTVID.setText("\u20B9 " + data.getTotal_price());


                    if (data.getOffer_percentage() != null && !data.getOffer_percentage().isEmpty() && !data.getOffer_percentage().equals("")) {
                        binding.offTVID.setText(data.getOffer_percentage() + "%");
                        binding.offTVID.setVisibility(View.VISIBLE);
                    } else {
                        binding.offTVID.setVisibility(View.GONE);
                    }

                    Utilities.strikeText(binding.mrpTVID);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.descTVID.setText(Html.fromHtml(data.getShort_description() + "\n\n" + data.getFull_description(), Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        binding.descTVID.setText(Html.fromHtml(data.getShort_description() + "\n\n" + data.getFull_description()));
                    }

                    loadBanners(bannersData);
                } else {
                    noDataViews();
                }
            } else {
                noDataViews();
            }
        });


        final int[] cartQty = {Integer.parseInt(binding.cartQty.getText().toString())};

        if (binding.cartQty.getText().toString().equals("0")) {
            binding.addToBagBtn.setText("Add to bag");
        } else {
            binding.addToBagBtn.setText("Update");
        }

        binding.cartIncBtn.setOnClickListener(v -> {
            cartQty[0]++;
            binding.cartQty.setText("" + cartQty[0]);

        });

        binding.cartDecBtn.setOnClickListener(v -> {
            if (cartQty[0] > 0) {
                cartQty[0]--;
            }
            binding.cartQty.setText("" + cartQty[0]);
        });

        binding.addToBagBtn.setOnClickListener(v -> {
            String carstQty = binding.cartQty.getText().toString();

            if (carstQty.equals("0")) {
                Utilities.showToast(getActivity(), "Select quantity..");
            } else {

                addToBag(carstQty);
                // click.onAddToCartClicked(data, carstQty);
            }
        });

        checkItemAddedInCart();
        return view;
    }

    private void checkItemAddedInCart() {
        CartViewModel cartVM = new CartViewModel(getActivity(), false);
        cartVM.getProductCartDetails(productID, getActivity());
        cartVM.getProductDetails().observe(getActivity(), cartItems -> {

            Log.e("data==>", new Gson().toJson(cartItems));

            if (cartItems.size() > 0) {
                binding.cartQty.setText("" + cartItems.get(0).getCartQty());
            } else {
                binding.cartQty.setText("0");
            }

        });
    }

    private void addToBag(String quantity) {
        CartItems items = new CartItems(0, "0", Utilities.getDeviceID(getActivity()), AppConstants.PRODUCTS_CART, detailsDataResponse.getProduct_name(), detailsDataResponse.getProduct_image(), quantity, String.valueOf(detailsDataResponse.getFinal_amount()), String.valueOf(detailsDataResponse.getOffer_price()), detailsDataResponse.getProduct_id(), productID, detailsDataResponse.getWeight());
        CartViewModel viewModel = new CartViewModel(getActivity());
        viewModel.insert(items);
        Utilities.customSnackBar(getActivity(), binding.mainSCVID, "Item added to cart successfully");
    }

    private void noDataViews() {
        binding.loadingPB.setVisibility(View.GONE);
        binding.mainSCVID.setVisibility(View.GONE);
        binding.noDataTVID.setVisibility(View.VISIBLE);
    }

    private void loadBanners(ArrayList<String> bannerData) {

        if (bannersData.size() > 0) {
            bannersData = bannerData;
        } else {
            bannersData.add(pic);
        }

        adapter = new BannersAdapter(bannersData, this);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        binding.imageSlider.setIndicatorSelectedColor(Color.WHITE);
        binding.imageSlider.setIndicatorUnselectedColor(Color.GRAY);
        binding.imageSlider.setScrollTimeInSec(3);
        binding.imageSlider.setAutoCycle(true);
        binding.imageSlider.startAutoCycle();
        binding.imageSlider.setSliderAdapter(adapter);

    }


    /* */

    public ProductDetailsFragment getData(String id, boolean typeOfItem) {
        ProductDetailsFragment fragment = new ProductDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("single_product_data", id);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productID = bundle.getString("single_product_data");
        }

    }

    @Override
    public void onBannersClickListener(StaticData data) {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);

    }


    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.loadingPB.setVisibility(View.VISIBLE);
            binding.mainSCVID.setVisibility(View.GONE);
            binding.noDataTVID.setVisibility(View.GONE);
        }

    }

    @Override
    public void hideLoading(int typeOfLoading) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.loadingPB.setVisibility(View.GONE);
            binding.mainSCVID.setVisibility(View.VISIBLE);
            binding.noDataTVID.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            noDataViews();
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }
}
