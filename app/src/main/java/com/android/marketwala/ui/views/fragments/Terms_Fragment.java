package com.android.marketwala.ui.views.fragments;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.databinding.FragmentTermsBinding;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.google.android.material.appbar.AppBarLayout;

public class Terms_Fragment extends Fragment {

    FragmentTermsBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity.isFood = false;

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms, container, false);

        View view = binding.getRoot();

        binding.termsWebview.getSettings().setJavaScriptEnabled(true);
        binding.termsWebview.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        binding.termsWebview.loadUrl("https://emarketwala.com/app-terms-conditions");

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

}
