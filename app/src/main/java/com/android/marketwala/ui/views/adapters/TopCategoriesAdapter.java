package com.android.marketwala.ui.views.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.TopCatHeaderBinding;
import com.android.marketwala.databinding.TypesListItemBinding;
import com.android.marketwala.ui.models.HomeCategoriesResponse.Categories;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class TopCategoriesAdapter extends RecyclerView.Adapter<TopCategoriesAdapter.TypesVH> {

    ArrayList<Categories> categoriesData = new ArrayList<>();
    TypesClick click;

    private boolean selectedPosition = true;
    int row_index = -1;
    Activity activity;


    public TopCategoriesAdapter(Activity activity, ArrayList<Categories> categoriesData, TypesClick click) {
        this.categoriesData = categoriesData;
        this.click = click;
        this.activity = activity;
    }

    @NonNull
    @Override
    public TypesVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TypesVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.top_cat_header, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TypesVH holder, int position) {
        holder.binding.setTypesData(categoriesData.get(position));
        holder.binding.getRoot().setOnClickListener(v -> {

            row_index = position;
            notifyDataSetChanged();
            selectedPosition = false;

            click.onTopCategoryItemClick(categoriesData.get(position));
        });

        if (row_index == position) {
            holder.binding.vegTVID.setBackgroundColor(activity.getResources().getColor(R.color.purple_700));
            holder.binding.vegTVID.setTextColor(activity.getResources().getColor(R.color.white));
        } else {
            holder.binding.vegTVID.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
            holder.binding.vegTVID.setTextColor(activity.getResources().getColor(R.color.black));
        }

        if (position == 0) {
            if (selectedPosition) {
                click.onTopCategoryItemClick(categoriesData.get(position));
                holder.binding.vegTVID.setBackgroundColor(activity.getResources().getColor(R.color.purple_700));
                holder.binding.vegTVID.setTextColor(activity.getResources().getColor(R.color.white));
            }
        }

    }

    @Override
    public int getItemCount() {
        return Math.min(categoriesData.size(), 3);
    }

    public static class TypesVH extends RecyclerView.ViewHolder {

        TopCatHeaderBinding binding;

        public TypesVH(@NonNull TopCatHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface TypesClick {
        void onTopCategoryItemClick(Categories categoriesData);
    }
}
