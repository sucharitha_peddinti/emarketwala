package com.android.marketwala.ui.models;

import java.io.Serializable;

public class AddressData implements Serializable {

    /*"address_id": "120",
            "user_id": "12",
            "name": "fdvdf",
            "mobile_number": "4578777",
            "email_id": "gbfgf",
            "pincode": "57878",
            "address": "cddfdfdfdf",
            "city": "ddddf",
            "state": "dfdfdfdf",
            "created_date": "2021-09-21",
            "updated_date": "2021-09-21 12:47:16"*/

    private String address_id;
    private String id;
    private String user_id;
    private String name;
    private String mobile_number;
    private String email_id;
    private String pincode;
    private String address;
    private String city;
    private String state;
    private String created_date;
    private String updated_date;

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
