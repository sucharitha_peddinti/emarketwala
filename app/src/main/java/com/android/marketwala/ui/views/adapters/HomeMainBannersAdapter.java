package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.android.marketwala.R;
import com.android.marketwala.databinding.BannersListItemBinding;
import com.android.marketwala.databinding.HomeBannersListItemBinding;
import com.android.marketwala.ui.models.HomeSlidersResponse;
import com.android.marketwala.ui.models.StaticData;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class HomeMainBannersAdapter extends SliderViewAdapter<HomeMainBannersAdapter.HomeBanners> {

    ArrayList<HomeSlidersResponse.SlidersData>bannersPOJOS=new ArrayList<>();
    BannersClickListener clickListener;


    public HomeMainBannersAdapter(ArrayList<HomeSlidersResponse.SlidersData>bannersPOJOS, BannersClickListener clickListener){
        this.bannersPOJOS=bannersPOJOS;
        this.clickListener=clickListener;
    }


    @Override
    public HomeBanners onCreateViewHolder(ViewGroup parent) {
        return new HomeBanners(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.home_banners_list_item,parent,false));

    }

    @Override
    public void onBindViewHolder(@NonNull HomeBanners holder, int position) {
        holder.binding.setBannersData(bannersPOJOS.get(position));
        holder.binding.getRoot().setOnClickListener(v -> clickListener.onBannersClickListener(bannersPOJOS.get(position)));
    }


    @Override
    public int getCount() {
        return bannersPOJOS.size();
    }

    public  class HomeBanners extends SliderViewAdapter.ViewHolder {
        HomeBannersListItemBinding binding;
        public HomeBanners(@NonNull HomeBannersListItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }

    public interface BannersClickListener{
        void onBannersClickListener(HomeSlidersResponse.SlidersData data);
    }
}
