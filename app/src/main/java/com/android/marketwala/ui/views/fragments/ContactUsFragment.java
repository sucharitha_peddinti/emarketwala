package com.android.marketwala.ui.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentContactUsBinding;
import com.android.marketwala.domain.ApiConnection;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.volley.Helper;
import com.android.volley.VolleyError;
import com.google.android.material.appbar.AppBarLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ContactUsFragment extends Fragment {

    FragmentContactUsBinding binding;
    AppViewModel viewModel;
    String name;
    String email;
    String description;
    String subject;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity.isFood = false;
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);

        View view = binding.getRoot();

        binding.submitContactbtn.setOnClickListener(v -> {
            if (Utilities.isNetworkAvailable(getActivity())) {
                callApi();
            } else {
                Utilities.noInternet(getActivity());
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    private void callApi() {


        name = binding.fullNameETID.getText().toString();
        email = binding.mobileETID.getText().toString();
        subject = binding.emailETID.getText().toString();
        description = binding.descriptionETID.getText().toString();


        if (TextUtils.isEmpty(name)) {
            Utilities.customSnackBar(getActivity(), binding.fullNameETID, "Enter FullName");
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Utilities.customSnackBar(getActivity(), binding.emailETID, "Enter Email");
            return;
        }
        if (TextUtils.isEmpty(subject)) {
            Utilities.customSnackBar(getActivity(), binding.mobileETID, "Enter Subject");
            return;
        }
        if (TextUtils.isEmpty(description)) {
            Utilities.customSnackBar(getActivity(), binding.descriptionETID, "Enter Description");
            return;
        }

        Map<String, String> coponslist = new HashMap<>();
        coponslist.put("api_key", AppConstants.API_TOKEN);
        coponslist.put("name", name);
        coponslist.put("email", email);
        coponslist.put("subject", subject);
        coponslist.put("message", description);
        new ApiConnection(getContext()).ParseVolleyJsonObject(AppConstants.BASE_URL + AppConstants.ContactUS, coponslist, new Helper() {
            @Override
            public void backResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);

                    if (response != null) {
                        JSONObject res1 = res.getJSONObject("Response");


                        Toast.makeText(getContext(), "Thanks for contacting..we will get back to you..", Toast.LENGTH_SHORT).show();


                        binding.fullNameETID.setText("");
                        binding.emailETID.setText("");
                        binding.mobileETID.setText("");
                        binding.descriptionETID.setText("");
                    } else {

                        // noRecordsLayout.setVisibility(View.VISIBLE);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
                        builder.setTitle("Error");
                        builder.setMessage(res.getString("msg"));
                        builder.setPositiveButton("OK", null);
                        builder.create().show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void backErrorResponse(VolleyError response) {


            }

        });
    }


}
