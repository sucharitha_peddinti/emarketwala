package com.android.marketwala.ui.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ActivityShowAddressBinding;
import com.android.marketwala.ui.models.AddressData;
import com.android.marketwala.ui.models.AddressListResponse;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.adapters.AddressAdapter;

import java.util.ArrayList;

public class ShowAddressActivity extends AppCompatActivity implements AppListener, AddressAdapter.AddressClick {


    ActivityShowAddressBinding binding;
    AppViewModel viewModel;
    UserLoginData loginData;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_address);
        Utilities.startAnimation(this);
        binding.addBtn.setOnClickListener(v -> {
            Intent intent = new Intent(this, AddAddressActivity.class);
            intent.putExtra("type", "create");
            startActivity(intent);
        });
        binding.headerID.backImageID.setOnClickListener(v -> Utilities.finishAnimation(this));
        binding.headerID.headerTitleTVID.setText("Address Book");

        loginData = Utilities.getUserData(this);
        viewModel = new AppViewModel(this);
        getUserAddress();
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    private void getUserAddress() {
        AddressListResponse response = new AddressListResponse();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setUser_id(loginData.getUser_id());

        viewModel.onAddress(response);
        viewModel.getAddressListResponseLiveData().observe(this, response1 -> {

            if (response1 != null) {
                if (response1.isStatus()) {

                    binding.addressRCID.setVisibility(View.VISIBLE);
                    binding.noDataTVID.setVisibility(View.GONE);
                    binding.loadingPB.setVisibility(View.GONE);
                    ArrayList<AddressData> data = response1.getResponse();
                    if (data.size() > 0) {
                        AddressAdapter addressAdapter = new AddressAdapter(data, this);
                        binding.addressRCID.setAdapter(addressAdapter);
                    } else {
                        setErrorViews();
                    }
                } else {
                    setErrorViews();
                }
            } else {
                setErrorViews();
            }

        });
    }

    private void setErrorViews() {
        binding.addressRCID.setVisibility(View.VISIBLE);
        binding.noDataTVID.setVisibility(View.VISIBLE);
        binding.loadingPB.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String refresh = MySharedPreferences.getPreferences(this, AppConstants.REFRESH);
        if (refresh.equalsIgnoreCase(AppConstants.YES)) {
            MySharedPreferences.setPreference(this, AppConstants.REFRESH, AppConstants.NO);
            getUserAddress();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utilities.finishAnimation(this);
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        binding.addressRCID.setVisibility(View.GONE);
        binding.noDataTVID.setVisibility(View.GONE);
        binding.loadingPB.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        binding.addressRCID.setVisibility(View.VISIBLE);
        binding.noDataTVID.setVisibility(View.GONE);
        binding.loadingPB.setVisibility(View.GONE);
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        setErrorViews();
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onEditClick(AddressData itemsData) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra("type", "edit");
        intent.putExtra("data", itemsData);
        intent.putExtra("address_id", itemsData.getAddress_id());
        startActivity(intent);
    }

    @Override
    public void onUseClick(AddressData itemsData) {
        Utilities.finishAnimation(this);
        Utilities.saveAddress(this, itemsData);
        MySharedPreferences.setPreference(this, AppConstants.REFRESH, AppConstants.YES);
    }
}