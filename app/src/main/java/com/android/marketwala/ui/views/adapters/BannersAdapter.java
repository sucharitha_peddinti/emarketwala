package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.android.marketwala.R;
import com.android.marketwala.databinding.BannersListItemBinding;
import com.android.marketwala.ui.models.StaticData;
import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class BannersAdapter extends SliderViewAdapter<BannersAdapter.BannersVH> {

    ArrayList<String> bannersPOJOS = new ArrayList<>();
    BannersClickListener clickListener;


    public BannersAdapter(ArrayList<String> bannersPOJOS, BannersClickListener clickListener) {
        this.bannersPOJOS = bannersPOJOS;
        this.clickListener = clickListener;
    }


    @Override
    public BannersVH onCreateViewHolder(ViewGroup parent) {
        return new BannersVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.banners_list_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull BannersVH holder, int position) {
        Glide.with(holder.binding.bannerImageID.getContext()).load(bannersPOJOS.get(position)).into(holder.binding.bannerImageID);
        //holder.binding.getRoot().setOnClickListener(v -> clickListener.onBannersClickListener(bannersPOJOS.get(position)));
    }


    @Override
    public int getCount() {
        return bannersPOJOS.size();
    }

    public static class BannersVH extends SliderViewAdapter.ViewHolder {
        BannersListItemBinding binding;

        public BannersVH(@NonNull BannersListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface BannersClickListener {
        void onBannersClickListener(StaticData data);
    }
}
