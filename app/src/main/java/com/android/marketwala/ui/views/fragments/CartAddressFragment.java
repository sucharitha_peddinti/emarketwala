package com.android.marketwala.ui.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.databinding.FragmentCartAddressBinding;
import com.android.marketwala.databinding.FragmentCartBinding;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.google.android.material.appbar.AppBarLayout;

public class CartAddressFragment extends Fragment {

    FragmentCartAddressBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding= DataBindingUtil.inflate(inflater, R.layout.fragment_cart_address,container,false);

        View view=binding.getRoot();
        MainActivity. isFood=false;
        binding.btnContinue.setOnClickListener(v-> FragmentUtilities.replaceFragment(getActivity(), new CartPaymentFragment(), AppConstants.FRAGMENT_CART_PAYMENT));

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.GONE), 200);
    }


}
