package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.databinding.MyOrderListItemBinding;
import com.android.marketwala.databinding.OrdersMainListItemBinding;
import com.android.marketwala.ui.models.FoodMyOrdersPOJO;
import com.android.marketwala.ui.models.MyOrdersPOJO;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class MyFoodOrdersAdapter extends RecyclerView.Adapter<MyFoodOrdersAdapter.ProductsVH> implements OrdersFood.ItemClick {

    List<FoodMyOrdersPOJO.FoodMyOrdersResponse> itemsData = new ArrayList<>();
    Activity activity;
    FoodOrdersClick click;


    public MyFoodOrdersAdapter(Activity activity, List<FoodMyOrdersPOJO.FoodMyOrdersResponse> itemsData, FoodOrdersClick click) {
        this.itemsData = itemsData;
        this.activity = activity;
        this.click = click;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.orders_main_list_item, parent, false));
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        FoodMyOrdersPOJO.FoodMyOrdersResponse data = itemsData.get(position);

        holder.binding.orderNumberTVID.setText(data.getOrder_number());
        holder.binding.orderAmountTVID.setText("\u20b9 " + data.getOrder_amount());
        holder.binding.orderPaymentTVID.setText(data.getPayment_type());

        OrdersFood ad = new OrdersFood(activity, data.getProduct_details(), this);
        holder.binding.dataRCID.setAdapter(ad);

        String delStatus = data.getProduct_details().get(0).getDelivery_status();

        if (delStatus.equals("1") || delStatus.equals("0")) {
            holder.binding.cancelBtn.setVisibility(View.VISIBLE);
            holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_cancel));
            holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.red));
            holder.binding.cancelBtn.setText("Cancel");
        } else if (delStatus.equals(AppConstants.ORDER_SHIPPED)) {
            holder.binding.cancelBtn.setVisibility(View.VISIBLE);
            holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_shipped));
            holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.yellow));
            holder.binding.cancelBtn.setText("Shipped");
        } else if (delStatus.equals(AppConstants.ORDER_CANCELLED)) {
            holder.binding.cancelBtn.setVisibility(View.VISIBLE);
            holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_cancelled));
            holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.white));
            holder.binding.cancelBtn.setText("Cancelled");
        } else if (delStatus.equals(AppConstants.ORDER_DELIVERED)) {
            holder.binding.cancelBtn.setVisibility(View.VISIBLE);
            holder.binding.cancelBtn.setBackground(activity.getResources().getDrawable(R.drawable.bg_deliverd));
            holder.binding.cancelBtn.setTextColor(activity.getResources().getColor(R.color.white));
            holder.binding.cancelBtn.setText("Delivered");
        }


        holder.binding.cancelBtn.setOnClickListener(v -> {
            if (delStatus.equals("1") || delStatus.equals("0")) {
                click.onFoodCancel(data.getProduct_details().get(0).getOrder_number());
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    @Override
    public void onItemCancel(FoodMyOrdersPOJO.FoodMyOrdersData itemData) {
        click.onFoodCancel(itemData.getOrder_number());
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        OrdersMainListItemBinding binding;

        public ProductsVH(@NonNull OrdersMainListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


    public interface FoodOrdersClick {
        void onFoodCancel(String id);
    }

}
