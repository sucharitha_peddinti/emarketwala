package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ProductsListItemBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.models.ProductsResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsVH> {

    ArrayList<ProductsResponse.ProductsData> itemsData = new ArrayList<>();
    ProductItemClick click;
    List<CartItems> cartData;

    public ProductsAdapter(ArrayList<ProductsResponse.ProductsData> itemsData, ProductItemClick click, List<CartItems> cartData) {
        this.itemsData = itemsData;
        this.click = click;
        this.cartData = cartData;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.products_list_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {


        ProductsResponse.ProductsData data = itemsData.get(position);


        //check user cart qty
        for (int j = 0; j < cartData.size(); j++) {
            if (cartData.get(j).getProduct_id().equals(data.getProduct_id())) {
                holder.binding.cartQty.setText("" + cartData.get(j).getCartQty());
                holder.binding.addToCartBtn.setText("Update");
            } else {
                holder.binding.addToCartBtn.setText("Add");
            }

        }

        final int[] cartQty = {Integer.parseInt(holder.binding.cartQty.getText().toString())};
        Log.e("item_cart_qty==>", "" + cartQty[0]);

        holder.binding.getRoot().setOnClickListener(v -> click.onProductItemClick(itemsData.get(position)));
        holder.binding.itemTitleTVID.setText(data.getProduct_name());
        holder.binding.itemQtyTVID.setText("Price per : " + " \u20b9 " + data.getTotal_price());
        holder.binding.totalTVID.setText("\u20b9 " + data.getTotal_price());
        holder.binding.price.setText("\u20b9 " + data.getOffer_price());
        Utilities.strikeText(holder.binding.totalTVID);

        holder.binding.itemWeightTVID.setText(data.getWeight());
        holder.binding.offerProduct.setText(data.getOffer_percentage() + "%");
        holder.binding.cartIncBtn.setOnClickListener(v -> {
            cartQty[0]++;
            holder.binding.cartQty.setText("" + cartQty[0]);

        });

        holder.binding.cartDecBtn.setOnClickListener(v -> {
            if (cartQty[0] > 0) {
                cartQty[0]--;
            }
            holder.binding.cartQty.setText("" + cartQty[0]);
        });

        holder.binding.addToCartBtn.setOnClickListener(v -> {
            String carstQty = holder.binding.cartQty.getText().toString();

            if (carstQty.equals("0")) {
                Utilities.showToast((Activity) holder.binding.cartQty.getContext(), "Select quantity..");
            } else {
                click.onAddToCartClicked(data, carstQty);
            }
        });


        Glide.with(holder.binding.productImageID.getContext())
                .load(data.getProduct_image())
                .into(holder.binding.productImageID);
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public interface ProductItemClick {
        void onProductItemClick(ProductsResponse.ProductsData itemsData);

        void onAddToCartClicked(ProductsResponse.ProductsData itemsData, String cartQty);
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        ProductsListItemBinding binding;

        public ProductsVH(@NonNull ProductsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
