package com.android.marketwala.ui.views.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ActivitySearchBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.cartDB.MarketWalaDB;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.RestaurantsResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.adapters.ProductsAdapter;
import com.android.marketwala.ui.views.adapters.RestaurantsAdapter;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements AppListener, RestaurantsAdapter.RestaurantsItemClick, ProductsAdapter.ProductItemClick {

    boolean isSearchFood = false;
    ActivitySearchBinding binding;
    AppViewModel viewModel;
    MarketWalaDB marketWalaDB;
    List<CartItems> cartData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                isSearchFood = bundle.getBoolean("key");
            }
        }
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        CartViewModel cartVM = new CartViewModel(this, isSearchFood);
        cartVM.cartData();
        cartVM.getGetCartItems().observe(this, cartItems -> {
            cartData = cartItems;
        });


        Utilities.startAnimation(this);

        binding.backImageID.setOnClickListener(v -> Utilities.finishAnimation(this));

        viewModel = new AppViewModel(this);

        if (isSearchFood) {
            binding.searchETID.setHint("Enter your delivery pincode");
        } else {
            binding.searchETID.setHint("What are you looking for");
        }


        binding.searchETID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 2) {
                    binding.loadingPB.setVisibility(View.VISIBLE);
                    callApiBasedOnSearch(s.toString());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utilities.finishAnimation(this);
    }

    private void callApiBasedOnSearch(String search) {


        if (isSearchFood) {

            RestaurantsResponse response = new RestaurantsResponse();
            response.setPincode(search);
            response.setApi_key(AppConstants.API_TOKEN);
            viewModel.restaurantsListData(response);
            viewModel.getRestaurantsResponseLiveData().observe(this, restaurantsResponse -> {

                if (restaurantsResponse.isStatus()) {
                    ArrayList<RestaurantsResponse.RestaurantsData> data = restaurantsResponse.getResponse();
                    if (data.size() > 0) {
                        binding.searchRCID.setVisibility(View.VISIBLE);
                        binding.loadingPB.setVisibility(View.GONE);
                        RestaurantsAdapter adapter = new RestaurantsAdapter(data, this);
                        binding.searchRCID.setAdapter(adapter);
                    } else {
                        setErrorViews();
                    }
                } else {
                    setErrorViews();
                }
            });
        } else {
            try {


                ProductsResponse response = new ProductsResponse();
                response.setApi_key(AppConstants.API_TOKEN);
                response.setSearch_word(search);

                viewModel.productsData(response, "search");
                viewModel.getProductsResponseLiveData().observe(this, productsResponse -> {

                    if (productsResponse.isStatus()==true) {
                        ArrayList<ProductsResponse.ProductsData> data = productsResponse.getResponse();
                        if (data.size() > 0) {
                            binding.searchRCID.setVisibility(View.VISIBLE);
                            binding.loadingPB.setVisibility(View.GONE);
                            ProductsAdapter adapter = new ProductsAdapter(data, this, cartData);
                            binding.searchRCID.setAdapter(adapter);
                        } else {
                            binding.searchRCID.setVisibility(View.GONE);
                            binding.loadingPB.setVisibility(View.GONE);
                            binding.noDataserTVID.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.searchRCID.setVisibility(View.GONE);
                        binding.loadingPB.setVisibility(View.GONE);
                        binding.noDataserTVID.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_LONG).show();


                    }
                });


            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private void setErrorViews() {
        binding.searchRCID.setVisibility(View.GONE);
        binding.loadingPB.setVisibility(View.GONE);
        binding.noDataserTVID.setVisibility(View.VISIBLE);
    }


    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        binding.searchRCID.setVisibility(View.GONE);
        binding.loadingPB.setVisibility(View.VISIBLE);
        binding.noDataserTVID.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        binding.searchRCID.setVisibility(View.VISIBLE);
        binding.loadingPB.setVisibility(View.GONE);
        binding.noDataserTVID.setVisibility(View.GONE);
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Utilities.showToast(this, msg);
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onRestaurantsItemClick(RestaurantsResponse.RestaurantsData itemsData) {

    }

    @Override
    public void onProductItemClick(ProductsResponse.ProductsData itemsData) {

    }

    @Override
    public void onAddToCartClicked(ProductsResponse.ProductsData itemsData, String cartQty) {

        CartItems items = new CartItems(0, "0", Utilities.getDeviceID(this), AppConstants.PRODUCTS_CART, itemsData.getProduct_name(), itemsData.getProduct_image(), cartQty, String.valueOf(itemsData.getTotal_price()), String.valueOf(itemsData.getOffer_price()), itemsData.getProduct_id(), "", itemsData.getWeight());
        CartViewModel viewModel = new CartViewModel(this);
        viewModel.insert(items);
        Utilities.customSnackBar(this, binding.mainID, "Item added to cart successfully");
    }
}