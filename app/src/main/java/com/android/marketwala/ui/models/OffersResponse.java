package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class OffersResponse {


    /*  "Status": true,
    "Message": "Success",
    "Response"*/

    private boolean Status;
    private String Message;
    private ArrayList<OffersData> Response = new ArrayList<>();

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<OffersData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<OffersData> response) {
        Response = response;
    }

    public static class OffersData {

        /*"id": "7",
            "offer_image": "https://emarketwala.com/uploads/offers/1624708598shipping-2.png",
            "status": "1",
            "created_date": "2021-05-04",
            "updated_date": "2021-06-26 07:56:38"*/

        private String id;
        private String offer_image;
        private String status;
        private String created_date;
        private String updated_date;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOffer_image() {
            return offer_image;
        }

        public void setOffer_image(String offer_image) {
            this.offer_image = offer_image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
