package com.android.marketwala.ui.viewModels;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.LiveData;

import com.android.marketwala.domain.ApiRepository;
import com.android.marketwala.ui.models.StaticData;

import java.util.ArrayList;

public class StaticViewModel extends BaseObservable {

    ApiRepository repository;
    private LiveData<ArrayList<StaticData>>sideMenuLiveData;
    private LiveData<ArrayList<StaticData>>profileData;


    private LiveData<ArrayList<StaticData>>subData;

    private LiveData<ArrayList<StaticData>>typesData;

    private LiveData<ArrayList<StaticData>>itemsData;

    public StaticViewModel(){
        repository=new ApiRepository();
    }

    public LiveData<ArrayList<StaticData>> getSideMenuLiveData() {
        return sideMenuLiveData;
    }

    public void sideMenuData(boolean isLoggedIn){
        sideMenuLiveData=repository.sideMenu(isLoggedIn);
    }

    public LiveData<ArrayList<StaticData>> getTypesData() {
        return typesData;
    }

    public void loadProfileMenu() {
        profileData=repository.profileData();
    }

    public LiveData<ArrayList<StaticData>> getProfileData() {
        return profileData;
    }
}
