package com.android.marketwala.ui.views.volley;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.Map;

public class ApiConnection {
    private final Context context;
    private final int MY_SOCKET_TIMEOUT_MS = 5000;
    public String encrypt;
    ProgressBar progressBar;

    public ApiConnection(Context context) {
        this.context = context;
    }


    public void ParseVolleyJsonObject(String url, final Map<String, String> parms, final Helper postResponse) {


        if (NetworkUtil.getConnectivityStatus(context)) {


            JsonObjectRequest jos = new JsonObjectRequest(url, new JSONObject(parms), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("RESPONSE", response + " ");
                    postResponse.backResponse(response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    postResponse.backErrorResponse(error);
                    Log.d("LISTING", "response error", error);

                }
            }) {


            };

            jos.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            VolleySingleton.getInstance(context).addToRequestQueue(jos);
        } else {
            Toast.makeText(context, "No Internet Connection ", Toast.LENGTH_SHORT).show();
        }
    }


    public void ParseVolleyJsonObjectGET(String url, final Helper postResponse) {


        if (NetworkUtil.getConnectivityStatus(context)) {

            StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    Log.d("RESPONSE", response + " ");
                    postResponse.backResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    postResponse.backErrorResponse(error);
                    Log.d("LISTING", "response error", error);

                }
            }) {


            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        } else {
            Toast.makeText(context, "No Internet Connection ", Toast.LENGTH_SHORT).show();
        }
    }

}