package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class RestaurantsResponse {

    private String api_key;
    private String pincode;
    private boolean Status;
    private String Message;
    private ArrayList<RestaurantsData> Response;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<RestaurantsData> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<RestaurantsData> response) {
        Response = response;
    }

    public static class RestaurantsData {
        private String id;
        private String name;
        private String title;
        private String res_image;
        private String banner_image;
        private String location;
        private String pincode;
        private String status;
        private String created_date;
        private String updated_date;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getRes_image() {
            return res_image;
        }

        public void setRes_image(String res_image) {
            this.res_image = res_image;
        }

        public String getBanner_image() {
            return banner_image;
        }

        public void setBanner_image(String banner_image) {
            this.banner_image = banner_image;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
