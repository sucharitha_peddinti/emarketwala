package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.TypesListItemBinding;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.HomeCategoriesResponse.Categories;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class TypesAdapter extends RecyclerView.Adapter<TypesAdapter.TypesVH> {

    ArrayList<HomeCategoriesResponse.Categories> typesData = new ArrayList<>();

    TypesClick click;

    public TypesAdapter(ArrayList<HomeCategoriesResponse.Categories> typesData, TypesClick click) {
        this.typesData = typesData;
        this.click = click;
    }

    @NonNull
    @Override
    public TypesVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TypesVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.types_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TypesVH holder, int position) {
        holder.binding.setTypesData(typesData.get(position));
        holder.binding.getRoot().setOnClickListener(v -> click.onCategoryItemClick(typesData.get(position)));

        Glide
                .with(holder.binding.imageID.getContext())
                .load(typesData.get(position).getCat_image())
                .placeholder(R.drawable.logo_green)
                .error(R.drawable.logo_green)
                .into(holder.binding.imageID);
    }

    @Override
    public int getItemCount() {
        return typesData.size();
    }

    public static class TypesVH extends RecyclerView.ViewHolder {

        TypesListItemBinding binding;

        public TypesVH(@NonNull TypesListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface TypesClick {
        void onCategoryItemClick(HomeCategoriesResponse.Categories typesData);
    }
}
