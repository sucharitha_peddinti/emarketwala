package com.android.marketwala.ui.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.CartListItemBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ProductsVH> {

    List<CartItems> itemsData = new ArrayList<>();
    ProductItemClick click;
    Activity activity;
    List<CartItems> cartData;

    public CartAdapter(Activity activity, List<CartItems> itemsData, ProductItemClick click, List<CartItems> cartData) {
        this.itemsData = itemsData;
        this.click = click;
        this.activity = activity;
        this.cartData = cartData;
    }

    @NonNull
    @Override
    public ProductsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.cart_list_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsVH holder, int position) {
        CartItems data = itemsData.get(position);

        holder.binding.cartQty.setText("" + data.cartQty);

        final int[] cartQty = {Integer.parseInt(holder.binding.cartQty.getText().toString())};
        Log.e("item_cart_qty==>", "" + cartQty[0]);
        holder.binding.getRoot().setOnClickListener(v -> click.onProductItemClick(itemsData.get(position)));
        holder.binding.nameTVID.setText(data.getItemName());
        holder.binding.qtyTVID.setText(data.cartQty + "");
//        holder.binding.priceTVID.setText("Total : \u20b9 " + data.getFinal_price());

        try {
            int final_amt = Integer.parseInt(data.getPrice()) * Integer.parseInt(data.cartQty);
            holder.binding.priceTVID.setText("Total : \u20b9 " + final_amt);
            Log.e("item_price", "" + final_amt);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        holder.binding.weightVID.setText("" + data.weight);


        holder.binding.removeTVID.setOnClickListener(v -> {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
            builder1.setCancelable(false);
            builder1.setTitle(null);
            builder1.setMessage("Are you sure \nDo you want to delete this item from cart");
            builder1.setPositiveButton("Yes", (dialogInterface, i1) -> {
                CartViewModel viewModel = new CartViewModel(activity);
                viewModel.delete(data, false, false);
            });
            builder1.setNegativeButton("No", null);
            builder1.create().show();
        });

        Glide.with(holder.binding.imageID.getContext())
                .load(data.getItemImage())
                .placeholder(R.drawable.logo_green)
                .into(holder.binding.imageID);

        holder.binding.cartIncBtn.setOnClickListener(v -> {
            cartQty[0]++;
            holder.binding.cartQty.setText("" + cartQty[0]);

        });

        holder.binding.addToCartBtn.setText("Update");

        holder.binding.cartDecBtn.setOnClickListener(v -> {
            if (cartQty[0] > 1) {
                cartQty[0]--;
            }
            holder.binding.cartQty.setText("" + cartQty[0]);
        });

        holder.binding.addToCartBtn.setOnClickListener(v -> {
            String carstQty = holder.binding.cartQty.getText().toString();

            if (carstQty.equals("0")) {
                Utilities.showToast((Activity) holder.binding.cartQty.getContext(), "Select quantity..");
            } else {
                click.onAddToCartClicked(data, carstQty);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public interface ProductItemClick {
        void onProductItemClick(CartItems itemsData);

        void onAddToCartClicked(CartItems itemsData, String cartQty);
    }

    public static class ProductsVH extends RecyclerView.ViewHolder {

        CartListItemBinding binding;

        public ProductsVH(@NonNull CartListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
