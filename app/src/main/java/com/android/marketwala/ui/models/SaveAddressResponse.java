package com.android.marketwala.ui.models;

public class SaveAddressResponse {

    /*"api_key":"25eeb7fd-d796-fe2a-359a-60507ad6ce6d",
"user_id":"12",
"name":"fdvdf",
"mobile_number":"4578777",
"email_id":"gbfgf",
"pincode":"5777878",
"address":"cddfdfdfdf",
"city":"ddddf",
"state":"dfdfdfdf"*/

    private String id;
    private String api_key;
    private String user_id;
    private String name;
    private String mobile_number;
    private String email_id;
    private String pincode;
    private String address;
    private String city;
    private String state;


    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
