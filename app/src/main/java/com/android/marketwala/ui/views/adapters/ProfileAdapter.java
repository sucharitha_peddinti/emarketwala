package com.android.marketwala.ui.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.databinding.ProfileDataBinding;
import com.android.marketwala.ui.models.StaticData;

import java.util.ArrayList;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ItemsVH> {

    ArrayList<StaticData> itemsData = new ArrayList<>();

    ItemClick click;

    public ProfileAdapter(ArrayList<StaticData> itemsData, ItemClick click) {
        this.itemsData = itemsData;
        this.click = click;
    }

    @NonNull
    @Override
    public ItemsVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemsVH(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.profile_data, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsVH holder, int position) {
        holder.binding.setMenuItem(itemsData.get(position));
        holder.binding.getRoot().setOnClickListener(v -> click.onItemClick(itemsData.get(position)));
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public static class ItemsVH extends RecyclerView.ViewHolder {

        ProfileDataBinding binding;

        public ItemsVH(@NonNull ProfileDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface ItemClick {
        void onItemClick(StaticData itemsData);
    }
}
