package com.android.marketwala.ui.views.volley;

import com.android.volley.VolleyError;

public interface Helper {
     void backResponse(String response);

     void backErrorResponse(VolleyError response);
}
