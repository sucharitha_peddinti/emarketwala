package com.android.marketwala.ui.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentProductsBinding;
import com.android.marketwala.ui.cartDB.CartItems;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.cartDB.MarketWalaDB;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.ProductsAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

public class ProductsFragment extends Fragment implements AppListener, ProductsAdapter.ProductItemClick {

    FragmentProductsBinding binding;
    AppViewModel viewModel;

    String subCatID, title;

    MarketWalaDB marketWalaDB;

    List<CartItems> cartData = new ArrayList<>();

    public ProductsFragment getData(String subCatID, String title) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("key", subCatID);
        bundle.putSerializable("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            subCatID = bundle.getString("key");
            title = bundle.getString("title");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false);
        View view = binding.getRoot();
        MainActivity.isFood = false;
        binding.titleTVID.setText(title);

        marketWalaDB = MarketWalaDB.getInstance(getActivity());

        viewModel = new AppViewModel(this);

        ProductsResponse response = new ProductsResponse();
        response.setApi_key(AppConstants.API_TOKEN);
        response.setSubcat_id(subCatID);


        CartViewModel cartVM = new CartViewModel(getActivity(), false);
        cartVM.cartData();
        cartVM.getGetCartItems().observe(getActivity(), cartItems -> {
            cartData = cartItems;
        });

        viewModel.productsData(response, "products");
        viewModel.getProductsResponseLiveData().observe(getActivity(), productsResponse -> {
            if (productsResponse.isStatus()) {
                ArrayList<ProductsResponse.ProductsData> data = productsResponse.getResponse();
                if (data.size() > 0) {
                    binding.productsRCID.setVisibility(View.VISIBLE);
                    binding.loadingRLID.setVisibility(View.GONE);


                    binding.noData.noDataViewLLID.setVisibility(View.GONE);
                    binding.mainLLID.setVisibility(View.VISIBLE);


                    ProductsAdapter adapter = new ProductsAdapter(data, this, cartData);
                    binding.productsRCID.setAdapter(adapter);
                } else {
                    setErrorViews();
                }
            } else {
                setErrorViews();
            }
        });

        return view;
    }

    private void setErrorViews() {
        binding.productsRCID.setVisibility(View.GONE);
        binding.progressBarProduct.setVisibility(View.GONE);
        binding.noData.noDataViewLLID.setVisibility(View.VISIBLE);
        binding.mainLLID.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {

        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.progressBarProduct.setVisibility(View.VISIBLE);
            binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();


            binding.noData.noDataViewLLID.setVisibility(View.GONE);
            binding.mainLLID.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.progressBarProduct.setVisibility(View.GONE);
            // binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();

            binding.noData.noDataViewLLID.setVisibility(View.GONE);
            binding.mainLLID.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onProductItemClick(ProductsResponse.ProductsData itemsData) {
        ProductDetailsFragment fragment = new ProductDetailsFragment().getData(itemsData.getProduct_id(), true);
        FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_PRODUCT_DETAILS);
    }

    @Override
    public void onAddToCartClicked(ProductsResponse.ProductsData itemsData, String cartQty) {

        CartItems items = new CartItems(0, "0", Utilities.getDeviceID(getActivity()), AppConstants.PRODUCTS_CART, itemsData.getProduct_name(), itemsData.getProduct_image(), cartQty, String.valueOf(itemsData.getOffer_price()), String.valueOf(itemsData.getOffer_price()), itemsData.getProduct_id(), subCatID, itemsData.getWeight());
        CartViewModel viewModel = new CartViewModel(getActivity());
        viewModel.insert(items);
        Utilities.customSnackBar(getActivity(), binding.mainID, "Item added to cart successfully");
    }
}
