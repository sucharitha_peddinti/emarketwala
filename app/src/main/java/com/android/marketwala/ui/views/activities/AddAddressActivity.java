package com.android.marketwala.ui.views.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ActivityAddressBinding;
import com.android.marketwala.ui.models.AddressData;
import com.android.marketwala.ui.models.SaveAddressResponse;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;


public class AddAddressActivity extends AppCompatActivity implements AppListener {

    ActivityAddressBinding binding;
    String name, mobile, email, pinCode, address, city, state, viewType;
    UserLoginData loginData;
    AppViewModel viewModel;
    String id;

    AddressData addressData;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_address);
        Utilities.startAnimation(this);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                viewType = bundle.getString("type");
                if (viewType.equalsIgnoreCase("edit")) {
                    addressData = (AddressData) bundle.getSerializable("data");
                    id = bundle.getString("address_id");
                    Log.e("id",""+id);
                }
            }
        }


        binding.headerID.backImageID.setOnClickListener(v -> Utilities.finishAnimation(this));

        try {


            if (viewType.equalsIgnoreCase("edit")) {
                binding.headerID.headerTitleTVID.setText("Edit Address");
            } else {
                binding.headerID.headerTitleTVID.setText("Add Address");
            }

            loginData = Utilities.getUserData(this);
            viewModel = new AppViewModel(this);

            if (viewType.equalsIgnoreCase("create")) {
                binding.nameETID.setText(loginData.getName());
                binding.emailETID.setText(loginData.getEmail_id());
                binding.numberETID.setText(loginData.getMobile_number());
            } else {
                binding.nameETID.setText(addressData.getName());
                binding.emailETID.setText(addressData.getEmail_id());
                binding.numberETID.setText(addressData.getMobile_number());

                binding.zipCodeETID.setText(addressData.getPincode());
                binding.addressETID.setText(addressData.getAddress());
                binding.cityETID.setText(addressData.getCity());
                binding.stateETID.setText(addressData.getState());
            }

            binding.saveBtn.setOnClickListener(v -> {
                if (Utilities.isNetworkAvailable(this)) {
                    validations();
                } else {
                    Utilities.noInternet(this);
                }
            });

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void show() {

    }

    /* private fun showOnLockScreenAndTurnScreenOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            )
        }
    }*/
    private void validations() {
        name = binding.nameETID.getText().toString();
        mobile = binding.numberETID.getText().toString();
        email = binding.emailETID.getText().toString();
        pinCode = binding.zipCodeETID.getText().toString();
        address = binding.addressETID.getText().toString();
        city = binding.cityETID.getText().toString();
        state = binding.stateETID.getText().toString();

        if (TextUtils.isEmpty(name)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter name..!");
            return;
        }

        if (TextUtils.isEmpty(mobile)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter mobile..!");
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter email..!");
            return;
        }

      /*  if (!Utilities.isValidEmail(email)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter valid email..!");
            return;
        }*/

        if (TextUtils.isEmpty(pinCode)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter pincode..!");
            return;
        }

        if (TextUtils.isEmpty(address)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter address..!");
            return;
        }

        if (TextUtils.isEmpty(city)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter city..!");
            return;
        }

        if (TextUtils.isEmpty(state)) {
            Utilities.customSnackBar(this, binding.mainRLID, "Enter state..!");
            return;
        }

        saveData();

    }

    private void saveData() {
        SaveAddressResponse response = new SaveAddressResponse();
        /*if (viewType.equalsIgnoreCase("edit")) {
            response.setId(addressData.getAddress_id());
        }*/

        response.setApi_key(AppConstants.API_TOKEN);
        response.setUser_id(loginData.getUser_id());
        response.setName(name);
        response.setId(id);
        response.setMobile_number(mobile);
        response.setEmail_id(email);
        response.setPincode(pinCode);
        response.setAddress(address);
        response.setCity(city);
        response.setState(state);


        viewModel.onSaveAddress(response);
        viewModel.getSaveAddressResponseLiveData().observe(this, saveAddressResponse -> {


            if (saveAddressResponse != null) {
                if (saveAddressResponse.isStatus()) {
                    Utilities.showToast(this, saveAddressResponse.getMessage());
                    Utilities.finishAnimation(this);
                    MySharedPreferences.setPreference(this, AppConstants.REFRESH, AppConstants.YES);
                } else {
                    Utilities.showToast(this, saveAddressResponse.getMessage());
                }
            } else {
                Utilities.showToast(this, "Server returns invalid response");
            }


        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utilities.finishAnimation(this);
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Utilities.showSimpleProgressDialog(this, null, loadingMsg, false);
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Utilities.removeSimpleProgressDialog();
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Utilities.showToast(this, msg);
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }
}