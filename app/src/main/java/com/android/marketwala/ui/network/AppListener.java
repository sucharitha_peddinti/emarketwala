package com.android.marketwala.ui.network;

public interface AppListener {

    void showLoading(int typeOfLoading, String loadingMsg);

    void hideLoading(int typeOfLoading);

    void showMsg(int typeOfMsg, int typeOfAction, String msg);

    void moveToAnotherScreen(String anyData);

}
