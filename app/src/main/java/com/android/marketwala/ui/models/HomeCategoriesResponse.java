package com.android.marketwala.ui.models;

import java.io.Serializable;
import java.util.ArrayList;

public class HomeCategoriesResponse implements Serializable {

    /* "Status": true,
    "Message": "Success",
    "Response":*/

    private boolean Status;
    private String Message;
    private ArrayList<Categories> Response = new ArrayList<>();

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<Categories> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<Categories> response) {
        Response = response;
    }

    public static class Categories implements Serializable {


        /* "category_id": "2",
            "category_name": "Fruits",
            "cat_image": "1624715719fruits.png",
            "status": "1",
            "created_date": "2021-04-02",
            "updated_date": "2021-06-26 09:55:19",
            "products"*/

        private String category_id;
        private String category_name;
        private String cat_image;
        private int status;
        private String created_date;
        private String updated_date;
        private ArrayList<Products> products = new ArrayList<>();


        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getCat_image() {
            return cat_image;
        }

        public void setCat_image(String cat_image) {
            this.cat_image = cat_image;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }

        public ArrayList<Products> getProducts() {
            return products;
        }

        public void setProducts(ArrayList<Products> products) {
            this.products = products;
        }
    }

    public static class Products implements Serializable {


        /*  "product_id": "1",
                    "product_num": "P4759",
                    "category_id": "2",
                    "subcat_id": "2",
                    "product_name": "Apples",
                    "short_description": "<p>Buttom mushrooms are very small sized mushrooms with smooth round caps and short stems. They have a mild flavour with a good texture that becomes more fragrant and meaty when cooked.<br />\r\nDo not forget to check out our delicious recipe</p>\r\n",
                    "full_description": "<p>Storage and Uses</p>\r\n\r\n<p>Do not store them in a plastic bag as they tend to deteriorate. Refrigerate them in a paper towel or a paper bag.<br />\r\nButton mushrooms can be sliced and added to soups, pizzas and pastas. They can also be added in stews, rice, noodles and can be stir-fried with seafood and chicken.</p>\r\n",
                    "product_image": "1621258230Apple_turkey.jpg",
                    "slider_images": "16281708611621258230Apple_turkey.jpg#1621258230Apple_turkey1.jpg",
                    "quanty": "1 pack",
                    "weight": "Approx .180g - 200 g",
                    "total_price": "220",
                    "offer_price": "200",
                    "offer_percentage": "",
                    "status": "1",
                    "created_date": "2021-04-05",
                    "updated_date": "2021-08-05 09:41:01"

                   */


        private String product_id;
        private String product_num;
        private String category_id;
        private String subcat_id;
        private String product_name;
        private String short_description;
        private String full_description;
        private String product_image;
        private String slider_images;
        private String quanty;
        private String weight;
        private String total_price;
        private String offer_price;
        private String offer_percentage;
        private String status;
        private String created_date;
        private String updated_date;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_num() {
            return product_num;
        }

        public void setProduct_num(String product_num) {
            this.product_num = product_num;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getSubcat_id() {
            return subcat_id;
        }

        public void setSubcat_id(String subcat_id) {
            this.subcat_id = subcat_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getFull_description() {
            return full_description;
        }

        public void setFull_description(String full_description) {
            this.full_description = full_description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getSlider_images() {
            return slider_images;
        }

        public void setSlider_images(String slider_images) {
            this.slider_images = slider_images;
        }

        public String getQuanty() {
            return quanty;
        }

        public void setQuanty(String quanty) {
            this.quanty = quanty;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getOffer_percentage() {
            return offer_percentage;
        }

        public void setOffer_percentage(String offer_percentage) {
            this.offer_percentage = offer_percentage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
