package com.android.marketwala.ui.models;

import java.util.ArrayList;

public class MyOrdersPOJO {

    /* "Status": true,
    "Message": "Success",
    "Response":*/


    private String api_key;
    private String user_id;

    private boolean Status;
    private String Message;
    private ArrayList<MyOrdersResponse> Response = new ArrayList<>();

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<MyOrdersResponse> getResponse() {
        return Response;
    }

    public void setResponse(ArrayList<MyOrdersResponse> response) {
        Response = response;
    }

    public static class MyOrdersResponse {

        /*{
            "id": "311",
            "payment_type": "cod",
            "order_number": "310875",
            "order_amount": "123",
            "email_id": "muyeed123hussian@gmail.com",
            "mobile_number": "8296067062",
            "captured": "1",*/

        private String id;
        private String payment_type;
        private String order_number;
        private String order_amount;
        private String email_id;
        private String mobile_number;
        private String captured;
        private ArrayList<MyOrderProducts> product_details = new ArrayList<>();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getOrder_number() {
            return order_number;
        }

        public void setOrder_number(String order_number) {
            this.order_number = order_number;
        }

        public String getOrder_amount() {
            return order_amount;
        }

        public void setOrder_amount(String order_amount) {
            this.order_amount = order_amount;
        }

        public String getEmail_id() {
            return email_id;
        }

        public void setEmail_id(String email_id) {
            this.email_id = email_id;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getCaptured() {
            return captured;
        }

        public void setCaptured(String captured) {
            this.captured = captured;
        }

        public ArrayList<MyOrderProducts> getProduct_details() {
            return product_details;
        }

        public void setProduct_details(ArrayList<MyOrderProducts> product_details) {
            this.product_details = product_details;
        }
    }

    public static class MyOrderProducts {
        /* "order_id": "1160",
                    "order_number": "310875",
                    "user_id": "156",
                    "product_id": "8",
                    "name": "Biganpalli mango",
                    "image": "1621258685Mango_Biganpalli.jpg",
                    "price": "60",
                    "quantity": "2",
                    "pquantity": "1 kg",
                    "pweight": "kg",
                    "address": "Muyeed Hussian,Teachers colony, near Shilpa school\r\nTank bund road\r\nCHINTAMANI Taluk\r\nChickaballapur district,Kolar,KARNATAKA,563125",
                    "payment_status": "1",
                    "cancelled_by": "admin",
                    "delivery_status": "4",
                    "delivery_date": null,
                    "created_date": "2021-09-25",
                    "updated_date": "2021-09-25 09:16:42"*/

        private String order_id;
        private String order_number;
        private String user_id;
        private String product_id;
        private String name;
        private String image;
        private String price;
        private String quantity;
        private String pquantity;
        private String pweight;
        private String address;
        private String payment_status;
        private String cancelled_by;
        private String delivery_status;
        private String delivery_date;
        private String created_date;
        private String updated_date;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_number() {
            return order_number;
        }

        public void setOrder_number(String order_number) {
            this.order_number = order_number;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getPquantity() {
            return pquantity;
        }

        public void setPquantity(String pquantity) {
            this.pquantity = pquantity;
        }

        public String getPweight() {
            return pweight;
        }

        public void setPweight(String pweight) {
            this.pweight = pweight;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getCancelled_by() {
            return cancelled_by;
        }

        public void setCancelled_by(String cancelled_by) {
            this.cancelled_by = cancelled_by;
        }

        public String getDelivery_status() {
            return delivery_status;
        }

        public void setDelivery_status(String delivery_status) {
            this.delivery_status = delivery_status;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public void setDelivery_date(String delivery_date) {
            this.delivery_date = delivery_date;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }
    }
}
