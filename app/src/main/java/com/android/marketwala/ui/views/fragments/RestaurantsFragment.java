package com.android.marketwala.ui.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentRestaurantsListBinding;
import com.android.marketwala.ui.models.RestaurantsResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.RestaurantsAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

public class RestaurantsFragment extends Fragment implements AppListener, RestaurantsAdapter.RestaurantsItemClick {


    FragmentRestaurantsListBinding binding;
    AppViewModel viewModel;
    RestaurantsResponse response = new RestaurantsResponse();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_restaurants_list, container, false);
        View view = binding.getRoot();
        viewModel = new AppViewModel(this);
        MainActivity.isFood = true;
        response.setApi_key(AppConstants.API_TOKEN);

        viewModel.restaurantsListData(response);
        viewModel.getRestaurantsResponseLiveData().observe(getActivity(), restaurantsResponse -> {
            try {


                if (restaurantsResponse.isStatus()) {
                    ArrayList<RestaurantsResponse.RestaurantsData> data = restaurantsResponse.getResponse();
                    if (data.size() > 0) {
                        binding.restaurantsRCID.setVisibility(View.VISIBLE);
                        binding.progressResta.setVisibility(View.GONE);
                        RestaurantsAdapter adapter = new RestaurantsAdapter(data, this);
                        binding.restaurantsRCID.setAdapter(adapter);
                    } else {
                        setErrorViews();
                    }
                } else {
                    setErrorViews();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });

        return view;
    }

    private void setErrorViews() {
        binding.restaurantsRCID.setVisibility(View.GONE);
        binding.progressResta.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
            new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {

        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.restaurantsRCID.setVisibility(View.GONE);
            binding.progressResta.setVisibility(View.VISIBLE);
            // binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();
        }
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        if (typeOfLoading == AppConstants.SLIDER_ANIMATION) {
            binding.restaurantsRCID.setVisibility(View.GONE);
            binding.progressResta.setVisibility(View.GONE);
            // binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            Utilities.showToast(getActivity(), msg);
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }

    @Override
    public void onRestaurantsItemClick(RestaurantsResponse.RestaurantsData itemsData) {

        if (itemsData.getStatus().equals("1")) {
            RestaurantItemsFragment fragment = new RestaurantItemsFragment().getData(itemsData.getId());
            FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_RESTAURANTS_ITEMS);
        } else {
            Utilities.showToast(getActivity(), "Restaurant closed now ....!");
        }
    }
}
