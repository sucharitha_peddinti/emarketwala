package com.android.marketwala.ui.network;

import androidx.lifecycle.LiveData;

import com.android.marketwala.ui.models.AddressListResponse;
import com.android.marketwala.ui.models.AlertResponse;
import com.android.marketwala.ui.models.CancelOrderResponse;
import com.android.marketwala.ui.models.CommonResponse;
import com.android.marketwala.ui.models.FoodMyOrdersPOJO;
import com.android.marketwala.ui.models.ForgotPasswordResponse;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.HomeSlidersResponse;
import com.android.marketwala.ui.models.MyOrdersPOJO;
import com.android.marketwala.ui.models.OffersResponse;
import com.android.marketwala.ui.models.PostalCodeSearchResponse;
import com.android.marketwala.ui.models.ProductDetailsResponse;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.RestaurantItemsResponse;
import com.android.marketwala.ui.models.RestaurantsResponse;
import com.android.marketwala.ui.models.SaveAddressResponse;
import com.android.marketwala.ui.models.SaveOrderResponse;
import com.android.marketwala.ui.models.SubCategoriesResponse;
import com.android.marketwala.ui.models.UserResponse;

public class AppViewModel {

    AppRepository repository;
    AppListener listener;

    private LiveData<HomeSlidersResponse> homeSlidersResponse;
    private LiveData<HomeCategoriesResponse> homeCategoriesResponseLiveData;

    private LiveData<OffersResponse> offersResponseLiveData;

    private LiveData<UserResponse> userResponseLiveData;
    private LiveData<ProductsResponse> productsResponseLiveData;
    private LiveData<ProductDetailsResponse> productDetailsResponseLiveData;
    private LiveData<RestaurantsResponse> restaurantsResponseLiveData;
    private LiveData<RestaurantItemsResponse> restaurantItemsResponseLiveData;

    private LiveData<SubCategoriesResponse> subCategories;
    private LiveData<ForgotPasswordResponse> forgotPasswordResponseLiveData;



    private LiveData<CommonResponse> saveAddressResponseLiveData;
    private LiveData<AddressListResponse> addressListResponseLiveData=null;


    private LiveData<CommonResponse> placeOrderResponse;
    private LiveData<CommonResponse> pinCodeSearch;
    private LiveData<MyOrdersPOJO> myOrdersPOJOLiveData;
    private LiveData<FoodMyOrdersPOJO> foodMyOrdersPOJOLiveData;

    private LiveData<CancelOrderResponse> cancelFoodOrder;
    private LiveData<CancelOrderResponse> cancelGroceryOrder;
    private LiveData<PostalCodeSearchResponse> postalCodeSearchResponseLiveData;
    private LiveData<AlertResponse> alertResponseLiveData;

    public AppViewModel(AppListener listener) {
        repository = new AppRepository();
        this.listener = listener;
    }


    public LiveData<HomeSlidersResponse> getHomeSlidersResponse() {
        return homeSlidersResponse;
    }

    public void homeSliders(HomeSlidersResponse request) {
        homeSlidersResponse = repository.loadHomeSliders(listener, request);
    }

    public LiveData<HomeCategoriesResponse> getHomeCategoriesResponseLiveData() {
        return homeCategoriesResponseLiveData;
    }

    public void homeCategoriesResponse(HomeSlidersResponse request) {
        homeCategoriesResponseLiveData = repository.loadHomeCategories(listener, request);
    }

    public LiveData<OffersResponse> getOffersResponseLiveData() {
        return offersResponseLiveData;
    }

    public void homeOffers(HomeSlidersResponse request) {
        offersResponseLiveData = repository.loadOffersData(listener, request);
    }


    //registration

    public LiveData<UserResponse> getUserResponseLiveData() {
        return userResponseLiveData;
    }

    public void registration(UserResponse user, String type) {
        userResponseLiveData = repository.registrationData(listener, user, type);
    }

    //products - category wise

    public LiveData<ProductsResponse> getProductsResponseLiveData() {
        return productsResponseLiveData;
    }

    public void productsData(ProductsResponse request,String api) {
        productsResponseLiveData = repository.loadProducts(listener, request,api);
    }

    public LiveData<ProductDetailsResponse> getProductDetailsResponseLiveData() {
        return productDetailsResponseLiveData;
    }

    public void productDetailsData(ProductDetailsResponse request) {
        productDetailsResponseLiveData = repository.loadProductDetails(listener, request);
    }

    public LiveData<RestaurantsResponse> getRestaurantsResponseLiveData() {
        return restaurantsResponseLiveData;
    }

    public void restaurantsListData(RestaurantsResponse request) {
        restaurantsResponseLiveData = repository.restaurantsListDetails(listener, request);
    }

    public LiveData<RestaurantItemsResponse> getRestaurantItemsResponseLiveData() {
        return restaurantItemsResponseLiveData;
    }

    public void restaurantItemsData(RestaurantItemsResponse response){
        restaurantItemsResponseLiveData=repository.restItems(listener,response);
    }

    public LiveData<SubCategoriesResponse> getSubCategories() {
        return subCategories;
    }

    public void subCategoriesData(SubCategoriesResponse response){
        subCategories=repository.subCategoryItems(listener,response);
    }

    public LiveData<ForgotPasswordResponse> getForgotPasswordResponseLiveData() {
        return forgotPasswordResponseLiveData;
    }

    public void onForgotPassword(ForgotPasswordResponse response){
        forgotPasswordResponseLiveData=repository.forgotPassword(listener,response);
    }

    public LiveData<CommonResponse> getSaveAddressResponseLiveData() {
        return saveAddressResponseLiveData;
    }

    public void onSaveAddress(SaveAddressResponse saveAddressResponse){
        saveAddressResponseLiveData=repository.saveAddress(listener,saveAddressResponse);
    }

    public LiveData<AddressListResponse> getAddressListResponseLiveData() {
        return addressListResponseLiveData;
    }

    public void onAddress(AddressListResponse response){
        addressListResponseLiveData=repository.getUserAddress(listener,response);
    }

    public LiveData<CommonResponse> getPlaceOrderResponse() {
        return placeOrderResponse;
    }

    public void onPlaceOrder(SaveOrderResponse response,boolean cartType) {
        placeOrderResponse=repository.saveGroceryOrder(listener,response,cartType);
    }

    public LiveData<CommonResponse> getPinCodeSearch() {
        return pinCodeSearch;
    }

    public void onPinCodeSearch(CommonResponse pinCode){
        pinCodeSearch=repository.pinCodeSearch(listener,pinCode);
    }

    public LiveData<MyOrdersPOJO> getMyOrdersPOJOLiveData() {
        return myOrdersPOJOLiveData;
    }

    public void onMyOrders(MyOrdersPOJO response){
        myOrdersPOJOLiveData=repository.myOrdersList(listener,response);
    }

    public LiveData<FoodMyOrdersPOJO> getFoodMyOrdersPOJOLiveData() {
        return foodMyOrdersPOJOLiveData;
    }

    public void onFoodMyOrders(FoodMyOrdersPOJO response){
        foodMyOrdersPOJOLiveData=repository.myFoodOrders(listener,response);
    }


    public LiveData<CancelOrderResponse> getCancelFoodOrder() {
        return cancelFoodOrder;
    }

    public void onFoodOrderCancel(CancelOrderResponse response, int api){
        cancelFoodOrder=repository.cancelOrder(listener,response,api);
    }

    public LiveData<CancelOrderResponse> getCancelGroceryOrder() {
        return cancelGroceryOrder;
    }

    public void onGroceryOrderCancel(CancelOrderResponse response,int api){
        cancelGroceryOrder=repository.cancelOrder(listener,response,api);
    }

    public LiveData<PostalCodeSearchResponse> getPostalCodeSearchResponseLiveData() {
        return postalCodeSearchResponseLiveData;
    }

    public void onPostalCodeSearch(PostalCodeSearchResponse response){
        postalCodeSearchResponseLiveData=repository.onPostalCodeSearch(listener,response);
    }

    public LiveData<AlertResponse> getAlertResponseLiveData() {
        return alertResponseLiveData;
    }

    public void onAlertResponse(AlertResponse response){
        alertResponseLiveData=repository.onInstructions(listener,response);
    }
}
