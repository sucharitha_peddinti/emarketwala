package com.android.marketwala.ui.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.FragmentSubCategoriesBinding;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.HomeSlidersResponse;
import com.android.marketwala.ui.models.SubCategoriesResponse;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.views.activities.MainActivity;
import com.android.marketwala.ui.views.adapters.CategoriesAdapter;
import com.android.marketwala.ui.views.adapters.SubCategoriesAdapter;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

public class CategoriesFragment extends Fragment implements AppListener, SubCategoriesAdapter.ItemClick, CategoriesAdapter.ItemClick {


    FragmentSubCategoriesBinding binding;

    AppViewModel viewModel;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AppBarLayout headerViewID = getActivity().findViewById(R.id.bottomBar);
        new Handler().postDelayed(() -> headerViewID.setVisibility(View.VISIBLE), 200);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sub_categories, container, false);
        View view = binding.getRoot();
        MainActivity.isFood = false;


        viewModel = new AppViewModel(this);
        HomeSlidersResponse response = new HomeSlidersResponse();
        response.setApi_key(AppConstants.API_TOKEN);

        viewModel.homeCategoriesResponse(response);


        viewModel.homeCategoriesResponse(response);
        viewModel.getHomeCategoriesResponseLiveData().observe(getActivity(), homeCategoriesResponse -> {
            if (homeCategoriesResponse!=null){
                if (homeCategoriesResponse.isStatus()) {
                    ArrayList<HomeCategoriesResponse.Categories> categories = homeCategoriesResponse.getResponse();

                    binding.productsRCID.setVisibility(View.VISIBLE);
                    binding.subcatProgress.setVisibility(View.GONE);
                    if (categories.size()>0){
                        CategoriesAdapter adapter=new CategoriesAdapter(categories,this);
                        binding.productsRCID.setAdapter(adapter);
                    }else{
                        setErrorViews();
                    }


                }else{
                    setErrorViews();
                }
            }else{
                setErrorViews();
            }
        });


        return view;
    }

    private void setErrorViews() {
        Log.e("categories==>","Error");
        binding.productsRCID.setVisibility(View.GONE);
        binding.subcatProgress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {
        Log.e("categories==>","Loading");
        if (typeOfLoading == AppConstants.CATEGORIES_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.subcatProgress.setVisibility(View.VISIBLE);
            binding.loadingID.shimmerLoadingLLID.startShimmerAnimation();
        }
    }

    @Override
    public void hideLoading(int typeOfLoading) {
        Log.e("categories==>","Hide");
        if (typeOfLoading == AppConstants.CATEGORIES_ANIMATION) {
            binding.productsRCID.setVisibility(View.GONE);
            binding.subcatProgress.setVisibility(View.GONE);
            //binding.loadingID.shimmerLoadingLLID.stopShimmerAnimation();
        }
    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {
        Log.e("categories==>","Error msg");
        if (typeOfMsg == AppConstants.MSG_VIEWS_API_ERROR) {
            Utilities.showToast(getActivity(), msg);
            setErrorViews();
        }
    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }


    @Override
    public void onItemClick(SubCategoriesResponse.SubCategoriesData itemsData) {
        ProductsFragment fragment = new ProductsFragment().getData(itemsData.getSubcat_id(),itemsData.getSubcat_name());
        FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_PRODUCTS);
    }

    @Override
    public void onItemClick(HomeCategoriesResponse.Categories itemsData) {
        SubCategoriesFragment fragment = new SubCategoriesFragment().getData(itemsData.getCategory_id());
        FragmentUtilities.replaceFragment(getActivity(), fragment, AppConstants.FRAGMENT_SUB_CATEGORIES);
    }
}
