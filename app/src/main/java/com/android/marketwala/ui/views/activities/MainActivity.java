package com.android.marketwala.ui.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.marketwala.R;
import com.android.marketwala.data.AppConstants;
import com.android.marketwala.data.FragmentUtilities;
import com.android.marketwala.data.MySharedPreferences;
import com.android.marketwala.data.Utilities;
import com.android.marketwala.databinding.ActivityMainBinding;
import com.android.marketwala.databinding.AlertLocationCheckBinding;
import com.android.marketwala.domain.ApiInterface;
import com.android.marketwala.ui.cartDB.CartViewModel;
import com.android.marketwala.ui.models.AlertResponse;
import com.android.marketwala.ui.models.PostalCodeSearchResponse;
import com.android.marketwala.ui.models.StaticData;
import com.android.marketwala.ui.models.UserLoginData;
import com.android.marketwala.ui.network.AppListener;
import com.android.marketwala.ui.network.AppViewModel;
import com.android.marketwala.ui.viewModels.StaticViewModel;
import com.android.marketwala.ui.views.adapters.SideMenuAdapter;
import com.android.marketwala.ui.views.fragments.AboutUsFragment;
import com.android.marketwala.ui.views.fragments.CartFragment;
import com.android.marketwala.ui.views.fragments.CategoriesFragment;
import com.android.marketwala.ui.views.fragments.ContactUsFragment;
import com.android.marketwala.ui.views.fragments.HomeFragment;
import com.android.marketwala.ui.views.fragments.MyOrdersFragment;
import com.android.marketwala.ui.views.fragments.ProfileFragment;
import com.android.marketwala.ui.views.fragments.RestaurantsFragment;
import com.android.marketwala.ui.views.fragments.Terms_Fragment;
import com.android.marketwala.ui.views.volley.ApiConnection;
import com.android.marketwala.ui.views.volley.Helper;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements SideMenuAdapter.SideMenuClick, AppListener {

    public static String CURRENT_FRAGMENT = AppConstants.FRAGMENT_HOME;
    public static boolean isFood = false;
    ApiInterface apiInterface;

    ActivityMainBinding binding;
    StaticViewModel viewModel;
    String loginStatus;
    AppViewModel appVM;
    AlertLocationCheckBinding alertBinding;
    Dialog dialog;
    String mobilenum;
    String url =
            "https://api.whatsapp.com/send?phone=" + mobilenum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Utilities.startAnimation(this);
        loginStatus = MySharedPreferences.getPreferences(this, AppConstants.LOGGED_IN_STATUS);
        binding.headerViewsID.menuIMVID.setOnClickListener(v -> openDrawer());
        appVM = new AppViewModel(this);

        binding.navigationID.profilePicID.setOnClickListener(v -> {
            closeDrawer();
            if (!loginStatus.equals(AppConstants.LOGGED_IN_VALUE)) {
                Intent intent = new Intent(this, UserActivities.class);
                intent.putExtra(AppConstants.SCREEN_FROM, AppConstants.SCREEN_LOGIN);
                startActivity(intent);
            }
        });


        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding.navigationID.myOrdersLLID.setOnClickListener(v -> {
            closeDrawer();
            if (loginStatus.equals(AppConstants.LOGGED_IN_VALUE)) {
                isFood = false;
                showSearch();
                callFragment(new MyOrdersFragment(), AppConstants.FRAGMENT_MY_ORDERS);
            } else {
                Utilities.showToast(this, "You have not logged in yet..!");
            }
        });

        binding.navigationID.cartLLID.setOnClickListener(v -> {
            CURRENT_FRAGMENT = AppConstants.FRAGMENT_CART;
            closeDrawer();
            loadCartCount(!isFood);
            showSearch();
            callFragment(new CartFragment().getData(isFood), AppConstants.FRAGMENT_CART);
        });

        binding.bottomViewID.contactUsLLID.setOnClickListener(v -> {
            CURRENT_FRAGMENT = AppConstants.FRAGMENT_CONTACT_US;
            showSearch();
            callFragment(new ContactUsFragment(), AppConstants.FRAGMENT_CONTACT_US);
            isFood = false;
            loadCartCount(true);

        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://api.whatsapp.com/send?phone=+91" + mobilenum));
                startActivity(i);
            }
        });
        binding.bottomViewID.homeLLID.setOnClickListener(v -> {
            CURRENT_FRAGMENT = AppConstants.FRAGMENT_HOME;
            isFood = false;
            loadCartCount(true);
            showSearch();
            callFragment(new HomeFragment(), AppConstants.FRAGMENT_HOME);


        });

        binding.bottomViewID.shopLLID.setOnClickListener(v -> {
            isFood = false;
            loadCartCount(true);
            showSearch();
            callFragment(new CategoriesFragment(), AppConstants.FRAGMENT_CATEGORIES);
        });


        binding.bottomViewID.profileLLID.setOnClickListener(v -> {
            if (loginStatus.equals(AppConstants.LOGGED_IN_VALUE)) {
                isFood = false;
                loadCartCount(true);
                showSearch();
                callFragment(new ProfileFragment(), AppConstants.FRAGMENT_PROFILE);
            } else {
                Utilities.showToast(this, "You have not logged in yet..>!");
            }
        });

        binding.bottomViewID.foodLLID.setOnClickListener(v -> {
            isFood = true;
            hideSearch();
            loadCartCount(false);
            callFragment(new RestaurantsFragment(), AppConstants.FRAGMENT_RESTAURANTS);
        });

        binding.headerViewsID.cartIMVID.setOnClickListener(v -> {
            loadCartCount(!isFood);
            showSearch();
            callFragment(new CartFragment().getData(isFood), AppConstants.FRAGMENT_CART);
        });


        binding.headerViewsID.locationIMVID.setOnClickListener(v -> displayLocationCheckAlert());
        loadHome();
        getContacts();
        setUpData();

        binding.headerViewsID.searchBgID.setOnClickListener(v -> {
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra("key", isFood);
            startActivity(intent);
        });

        setInstruction();

        binding.headerViewsID.phonenoIMVID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + mobilenum));//change the number

                Log.e("mobilenum", "" + mobilenum);
                if (Build.VERSION.SDK_INT > 23) {

                    startActivity(callIntent);
                } else {

                    if (ActivityCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(MainActivity.this, "Permission Not Granted ", Toast.LENGTH_SHORT).show();
                    } else {
                        final String[] PERMISSIONS_STORAGE = {Manifest.permission.CALL_PHONE};
                        ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS_STORAGE, 9);

                        startActivity(callIntent);
                    }
                }
            }
        });

    }

    private void setInstruction() {
        AlertResponse alertResponse = new AlertResponse();
        alertResponse.setApi_key(AppConstants.API_TOKEN);
        appVM.onAlertResponse(alertResponse);

        appVM.getAlertResponseLiveData().observe(this, alertResponse1 -> {
            try {


                if (alertResponse1.getResponse().getStatus().equals("1")) {
                    Log.e("res alert", "alert" + alertResponse1.getResponse().getContent());
                    binding.bottomViewID.alertTVID.setVisibility(View.VISIBLE);
                    binding.bottomViewID.alertTVID.setText("" + alertResponse1.getResponse().getContent());
                    binding.bottomViewID.alertTVID.setSelected(true);
                } else {
                    binding.bottomViewID.alertTVID.setVisibility(View.GONE);

                }
                // binding.bottomViewID.alertTVID.setVisibility(View.VISIBLE);


            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });


    }

    private void showSearch() {
        binding.headerViewsID.searchBgID.setVisibility(View.VISIBLE);
        binding.headerViewsID.searchTVVID.setVisibility(View.VISIBLE);
        binding.headerViewsID.searchIMVID.setVisibility(View.VISIBLE);
    }

    private void hideSearch() {
        binding.headerViewsID.searchBgID.setVisibility(View.GONE);
        binding.headerViewsID.searchTVVID.setVisibility(View.GONE);
        binding.headerViewsID.searchIMVID.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadCartCount(true);
    }


    private void getContacts() {


        Map<String, String> customproductDetails = new HashMap<>();
        customproductDetails.put("api_key", "25eeb7fd-d796-fe2a-359a-60507ad6ce6d");


        Log.e("customproductDetails", "" + customproductDetails);
        new ApiConnection(this).ParseVolleyJsonObject(AppConstants.BASE_URL + "/" + AppConstants.FRAGMENT_Get_CONTACT_US, customproductDetails, new Helper() {
            @Override
            public void backResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);

                    if (response != null) {
                        JSONObject res1 = res.getJSONObject("Response");


                        mobilenum = res1.getString("header_mobile");
                        Log.e("res mobile", "" + mobilenum);
                    } else {
                        // noRecordsLayout.setVisibility(View.VISIBLE);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getApplicationContext());
                        builder.setTitle("Error");
                        builder.setMessage(res.getString("msg"));
                        builder.setPositiveButton("OK", null);
                        builder.create().show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    // Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void backErrorResponse(VolleyError response) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }

        });
    }


    @SuppressLint("SetTextI18n")
    private void loadCartCount(boolean isHome) {
        CartViewModel viewModel = new CartViewModel(this);

        viewModel.cartCount(isHome);
        viewModel.getCartCount().observe(this, cartItems -> {

            if (cartItems.size() > 0) {
                binding.headerViewsID.cartCountTVID.setVisibility(View.VISIBLE);
                binding.headerViewsID.cartCountTVID.setText("" + cartItems.size());
            } else {
                binding.headerViewsID.cartCountTVID.setVisibility(View.GONE);
            }
        });
    }

    private void loadHome() {
        isFood = false;
        loadCartCount(true);
        runOnUiThread(() -> callFragment(new HomeFragment(), AppConstants.FRAGMENT_HOME));
    }

    @SuppressLint("SetTextI18n")
    private void setUpData() {
        viewModel = new StaticViewModel();
        if (loginStatus.equals(AppConstants.LOGGED_IN_VALUE)) {
            viewModel.sideMenuData(true);
            UserLoginData loginData = Utilities.getUserData(this);
            binding.navigationID.userTitle.setText(loginData.getName());
            Log.d("USER_DATA", "" + loginData.getUser_id());
        } else {
            viewModel.sideMenuData(false);
            binding.navigationID.userTitle.setText("Login");
        }

        viewModel.getSideMenuLiveData().observe(this, staticData -> {
            SideMenuAdapter adapter = new SideMenuAdapter(staticData, this);
            binding.navigationID.sideMenuRCID.setAdapter(adapter);
        });
    }

    private void callFragment(Fragment fragment, String tag) {
        FragmentUtilities.replaceFragment(this, fragment, tag);
    }

    public void openDrawer() {
        binding.drawerLVID.openDrawer(binding.sideMenuLayoutID);
    }

    public void closeDrawer() {
        binding.drawerLVID.closeDrawer(binding.sideMenuLayoutID);
    }

    @Override
    public void onSideMenuItemClick(StaticData sideMenuData) {
        closeDrawer();
        switch (sideMenuData.getId()) {
            case 1:
                loadHome();
                break;

            case 3:
                callFragment(new ProfileFragment(), AppConstants.FRAGMENT_PROFILE);
                break;

            case 8:
                callFragment(new MyOrdersFragment(), AppConstants.FRAGMENT_MY_ORDERS);
                break;

            case 4:
                callFragment(new AboutUsFragment(), AppConstants.FRAGMENT_ABOUT_US);
                break;
            case 5:
                callFragment(new Terms_Fragment(), AppConstants.FRAGMENT_TERMS);
                break;

            case 6:
                callFragment(new ContactUsFragment(), AppConstants.FRAGMENT_CONTACT_US);
                break;

            case 0:
                logout();
                break;
        }
    }

    private void logout() {
        final Dialog dialog;
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.alert_logout);

        AppCompatButton alertNoBtn = dialog.findViewById(R.id.noBtn);
        AppCompatButton alertYesBtn = dialog.findViewById(R.id.yesBtn);
        alertNoBtn.setOnClickListener(v -> dialog.dismiss());

        alertYesBtn.setOnClickListener(v -> {
            dialog.dismiss();
            MySharedPreferences.clearPreferences(this);
            Intent newIntent = new Intent(this, MainActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newIntent);
            Utilities.finishAnimation(this);
        });

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onBackPressed() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Log.e("" + AppConstants.TAG, "onBackPressed: ");
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
        Log.d(AppConstants.TAG, "Empty");

    }

    public void displayLocationCheckAlert() {
        dialog = new Dialog(this);
        alertBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.alert_location_check, null, false);
        dialog.setContentView(alertBinding.getRoot());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        alertBinding.closeIMVID.setOnClickListener(v -> dialog.dismiss());

        alertBinding.searchBtn.setOnClickListener(v -> {
            String search = alertBinding.pinCodeETID.getText().toString();
            if (TextUtils.isEmpty(search)) {
                Utilities.showToast(this, "Enter Postal code");
                return;
            }
            if (alertBinding.searchBtn.getText().toString().equalsIgnoreCase("Search")) {
                callSearchAPI(alertBinding, search, dialog);

            } else {
                dialog.dismiss();
            }

        });
    }

    private void callSearchAPI(AlertLocationCheckBinding alertBinding, String search, Dialog dialog) {
        alertBinding.loadingPB.setVisibility(View.VISIBLE);


        PostalCodeSearchResponse re = new PostalCodeSearchResponse();
        re.setApi_key(AppConstants.API_TOKEN);
        re.setPincode(search);

        appVM.onPostalCodeSearch(re);
        appVM.getPostalCodeSearchResponseLiveData().observe(this, response -> {
            if (response != null) {
                alertBinding.noDeliveryTVID.setText(response.getResponse());
                setViews(alertBinding, response.isStatus(), dialog);

            } else {
                setViews(alertBinding, false, dialog);

            }
        });


    }

    @SuppressLint("SetTextI18n")
    private void setViews(AlertLocationCheckBinding alertBinding, boolean type, Dialog dialog) {
        alertBinding.loadingPB.setVisibility(View.GONE);
        alertBinding.noDeliveryLLID.setVisibility(View.VISIBLE);
        alertBinding.searchBtn.setText("Continue");
        if (type) {
            alertBinding.typeOfViewID.setBackground(getResources().getDrawable(R.drawable.bg_green_circle));
            alertBinding.typeOfViewID.setImageResource(R.drawable.ic_check_white);
            alertBinding.noDeliveryTVID.setTextColor(getResources().getColor(R.color.purple_700));
        } else {
            alertBinding.typeOfViewID.setBackground(getResources().getDrawable(R.drawable.bg_cart_circle));
            alertBinding.typeOfViewID.setImageResource(R.drawable.ic_close);
            alertBinding.noDeliveryTVID.setTextColor(getResources().getColor(R.color.red));
        }

        alertBinding.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "Thank you for choosing your Location,Please continue orders..", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showLoading(int typeOfLoading, String loadingMsg) {

    }

    @Override
    public void hideLoading(int typeOfLoading) {

    }

    @Override
    public void showMsg(int typeOfMsg, int typeOfAction, String msg) {

    }

    @Override
    public void moveToAnotherScreen(String anyData) {

    }
}