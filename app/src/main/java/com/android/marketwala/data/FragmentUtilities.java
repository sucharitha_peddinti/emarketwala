package com.android.marketwala.data;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.marketwala.R;

public class FragmentUtilities {

    public static void replaceFragment(FragmentActivity context, Fragment fragment, String currentFragment) {

        String fragmentTag = fragment.getClass().getName();
        FragmentManager manager = context.getSupportFragmentManager();


        boolean fragmentPopped = manager.popBackStackImmediate (fragmentTag, 0);


        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            Utilities.nextFragmentAnimation(ft);
            ft.replace(R.id.frame_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(currentFragment);


            ft.commit();
        } else {
            FragmentTransaction ft = manager.beginTransaction();
            Utilities.nextFragmentAnimation(ft);
            ft.replace(R.id.frame_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);

            ft.detach(fragment);
            ft.attach(fragment);
            ft.commit();
        }

    }

}
