package com.android.marketwala.data;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.android.marketwala.R;
import com.android.marketwala.domain.ApiClient;
import com.android.marketwala.domain.ApiInterface;
import com.android.marketwala.ui.models.AddressData;
import com.android.marketwala.ui.models.UserLoginData;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Created By Venkei
 * On 08-04-2021
 */
public class Utilities {

    private static String TAG = "Utilities";
    private static ProgressDialog mProgressDialog;
    private static KProgressHUD hud;
    private static ProgressBar progressBar;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            // Toast.makeText(ctx, "Internet is connected", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "isNetworkAvailable: Internet is connected");
            return true;
        } else {
            Log.i(TAG, "isNetworkAvailable: NO INTERNET");
            return false;
        }
    }

    public static void startAnimation(Activity activity) {
        activity.overridePendingTransition(R.anim.act_pull_in_right, R.anim.act_push_out_left);
    }

    public static void finishAnimation(Activity activity) {
        activity.finish();
        activity.overridePendingTransition(R.anim.act_pull_in_left, R.anim.act_push_out_right);
    }

    public static void showToast(Activity activity, String msg) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, activity.findViewById(R.id.custom_toast_container));

        TextView text = layout.findViewById(R.id.text);
        text.setText(msg);

        Toast toast = new Toast(activity.getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 40);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showSimpleProgressDialog(Context context, String title, String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void customStatusBar(Activity _activity) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                View decor = _activity.getWindow().getDecorView();
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                _activity.getWindow().setStatusBarColor(_activity.getResources().getColor(R.color.edit_f2));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    _activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    _activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    _activity.getWindow().setStatusBarColor(_activity.getResources().getColor(R.color.edit_f2));
                }
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void whiteStatusBar(Activity _activity) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                View decor = _activity.getWindow().getDecorView();
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                _activity.getWindow().setStatusBarColor(_activity.getResources().getColor(R.color.white));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    _activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    _activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    _activity.getWindow().setStatusBarColor(_activity.getResources().getColor(R.color.white));
                }
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setRecyclerViewProperly(RecyclerView recyclerView, int spanValue) {
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view); // item position
                int spanCount = spanValue;
                int spacing = 10;//spacing between views in grid

                if (position >= 0) {
                    int column = position % spanCount; // item column

                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect.top = spacing;
                    }
                    outRect.bottom = spacing; // item bottom
                } else {
                    outRect.left = 0;
                    outRect.right = 0;
                    outRect.top = 0;
                    outRect.bottom = 0;
                }
            }
        });
    }


    public static void customSnackBar(Activity activity, View view, String msg) {
        final Snackbar snackbar = Snackbar.make(view, "", 2500);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        // Inflate your custom view with an Edit Text
        LayoutInflater objLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View snackView = objLayoutInflater.inflate(R.layout.custom_snac_layout, null);

        TextView textTVID = snackView.findViewById(R.id.textTVID);
        textTVID.setText(msg);

        layout.addView(snackView, 0);
        snackbar.show();
    }

    public static void staticAlert(Context activity, String tittle, String message) {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.custom_static_alert);
        dialog.show();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tittleTVID = dialog.findViewById(R.id.tittleTVID);
        TextView messageTVID = dialog.findViewById(R.id.messageTVID);
        TextView okBtn = dialog.findViewById(R.id.okBtn);
        //RelativeLayout cancelRLID = dialog.findViewById(R.id.cancelRLID);
        tittleTVID.setText(tittle);
        messageTVID.setText(message);
        okBtn.setOnClickListener(view -> dialog.dismiss());
    }

    public static void noInternet(Context activity) {
        staticAlert(activity, "No Internet....", "Make sure your device is connected to internet");
    }

    public static String time() {
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.format(new Date(System.currentTimeMillis()));
        String time = formatter.format(new Date(System.currentTimeMillis()));
        return time;
    }

    public static void saveUserData(Activity activity, UserLoginData detailsResponse) {
        SharedPreferences prefs = activity.getSharedPreferences("market_wala", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(detailsResponse);
        prefsEditor.putString(AppConstants.LOGGED_IN_USER_DATA, json);
        prefsEditor.apply();
    }

    public static UserLoginData getUserData(Activity activity) {
        //values
        SharedPreferences prefs = activity.getSharedPreferences("market_wala", Context.MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = prefs.getString(AppConstants.LOGGED_IN_USER_DATA, "");
        UserLoginData userDetails = gson.fromJson(json, UserLoginData.class);
        return userDetails;
    }




    public static void saveAddress(Activity activity, AddressData detailsResponse) {
        SharedPreferences prefs = activity.getSharedPreferences("market_wala", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(detailsResponse);
        prefsEditor.putString(AppConstants.SAVED_ADDRESS_DATA, json);
        prefsEditor.apply();
    }

    public static AddressData getAddress(Activity activity) {
        //values
        SharedPreferences prefs = activity.getSharedPreferences("market_wala", Context.MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = prefs.getString(AppConstants.SAVED_ADDRESS_DATA, "");
        AddressData userDetails = gson.fromJson(json, AddressData.class);
        return userDetails;
    }



    public static ApiInterface getApiClass() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public static String setDateInFormat() {
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat month_date = new SimpleDateFormat("dd - MMMM - yyyy");
        String ma = month_date.format(calendar.getTime());
        return ma;
    }


    public static void showProgressDialog(Activity activity,String msg) {
        if (hud == null) {
            hud = KProgressHUD.create(activity)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel(msg)
                    .setDetailsLabel("Please wait")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();
        }
    }

    public static void dismissProgressDialog() {
        if (hud != null && hud.isShowing()) {
            hud.dismiss();
        }
    }

    public static void nextFragmentAnimation(FragmentTransaction ft) {
        ft.setCustomAnimations(R.anim.slide_right, R.anim.slide_left, R.anim.slide_out_from_left, R.anim.slide_in_to_left);
    }


    public static void showProgressBar(ProgressBar pb){
        pb.setVisibility(View.VISIBLE);
    }
    public static void hideProgressBar(ProgressBar pb){
        pb.setVisibility(View.GONE);
    }

    public static void strikeText(AppCompatTextView tv){
        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static String getDeviceID(Activity activity){
     return Settings.Secure.getString(activity.getContentResolver(),
             Settings.Secure.ANDROID_ID);
    }
}
