package com.android.marketwala.domain;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.marketwala.R;
import com.android.marketwala.ui.models.StaticData;

import java.util.ArrayList;

public class ApiRepository {
    public LiveData<ArrayList<StaticData>> sideMenu(boolean isLoggedIn) {
        MutableLiveData<ArrayList<StaticData>> listMutableLiveData = new MutableLiveData<>();
        ArrayList<StaticData> data = new ArrayList<>();
        data.add(new StaticData(1, "Home", R.mipmap.ic_home));

        //data.add(new StaticData(3, "Chat with Us", R.mipmap.ic_home));

        data.add(new StaticData(4, "About Us", R.drawable.ic_baseline_phone_in_talk_24));
        data.add(new StaticData(5, "Terms & Privacy", R.mipmap.ic_categories));
        data.add(new StaticData(6, "Contact Us", R.mipmap.ic_explore));
        if (isLoggedIn) {
            data.add(new StaticData(3, "My Profile", R.mipmap.ic_profile));
            //data.add(new StaticData(8, "Order History", R.mipmap.ic_profile));
            data.add(new StaticData(0, "Logout", R.drawable.ic_baseline_power_settings_new_24));
        }


        //data.add(new StaticData(7, "Reviews Us", R.mipmap.ic_categories));
        //data.add(new StaticData(8, "Share with Friends", R.mipmap.ic_location));
        listMutableLiveData.setValue(data);
        return listMutableLiveData;
    }


    public LiveData<ArrayList<StaticData>> profileData() {
        MutableLiveData<ArrayList<StaticData>> listMutableLiveData = new MutableLiveData<>();
        ArrayList<StaticData> data = new ArrayList<>();
        data.add(new StaticData(1, "My Cart", R.drawable.cart_empty));
        data.add(new StaticData(2, "My Orders", R.mipmap.ic_profile));
        data.add(new StaticData(3, "My Address", R.drawable.ic_baseline_location_on_24));
        data.add(new StaticData(0, "Logout", R.drawable.logout));
        listMutableLiveData.setValue(data);
        return listMutableLiveData;
    }
}
