package com.android.marketwala.domain;

import com.android.marketwala.data.AppConstants;
import com.android.marketwala.ui.models.AddressListResponse;
import com.android.marketwala.ui.models.AlertResponse;
import com.android.marketwala.ui.models.CancelOrderResponse;
import com.android.marketwala.ui.models.CommonResponse;
import com.android.marketwala.ui.models.FoodMyOrdersPOJO;
import com.android.marketwala.ui.models.ForgotPasswordResponse;
import com.android.marketwala.ui.models.Get_ContactData;
import com.android.marketwala.ui.models.HomeCategoriesResponse;
import com.android.marketwala.ui.models.HomeSlidersResponse;
import com.android.marketwala.ui.models.MyOrdersPOJO;
import com.android.marketwala.ui.models.OffersResponse;
import com.android.marketwala.ui.models.PostalCodeSearchResponse;
import com.android.marketwala.ui.models.ProductDetailsResponse;
import com.android.marketwala.ui.models.ProductsResponse;
import com.android.marketwala.ui.models.RestaurantItemsResponse;
import com.android.marketwala.ui.models.RestaurantsResponse;
import com.android.marketwala.ui.models.SaveAddressResponse;
import com.android.marketwala.ui.models.SaveOrderResponse;
import com.android.marketwala.ui.models.SubCategoriesResponse;
import com.android.marketwala.ui.models.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST(AppConstants.HOME_SLIDERS)
    Call<HomeSlidersResponse> homeSlidersAPI(@Body HomeSlidersResponse dataRequestPOJO);


    @POST(AppConstants.HOME_CATEGORIES)
    Call<HomeCategoriesResponse> homeCategoriesAPI(@Body HomeSlidersResponse dataRequestPOJO);

    @POST(AppConstants.HOME_OFFERS)
    Call<OffersResponse> homeOffersAPI(@Body HomeSlidersResponse dataRequestPOJO);

    @POST(AppConstants.REGISTRATION)
    Call<UserResponse> registrationAPI(@Body UserResponse userResponse);

    @POST(AppConstants.LOGIN)
    Call<UserResponse> loginAPI(@Body UserResponse userResponse);


    @POST(AppConstants.FORGOT_PASSWORD)
    Call<ForgotPasswordResponse> forgotPasswordAPI(@Body ForgotPasswordResponse userResponse);


    @POST(AppConstants.CATEGORY_WISE_PRODUCTS)
    Call<ProductsResponse> categoryWisProductsAPI(@Body ProductsResponse dataRequestPOJO);

    @POST(AppConstants.PRODUCTS_SEARCH_API)
    Call<ProductsResponse> productsSearchAPI(@Body ProductsResponse dataRequestPOJO);


    @POST(AppConstants.PRODUCT_FULL_DETAILS)
    Call<ProductDetailsResponse> productDetailsAPI(@Body ProductDetailsResponse dataRequestPOJO);


    @POST(AppConstants.RESTAURANTS_LIST)
    Call<RestaurantsResponse> restaurantsListAPI(@Body RestaurantsResponse dataRequestPOJO);

    @POST(AppConstants.RESTAURANTS_BASED_ITEMS)
    Call<RestaurantItemsResponse> restaurantItemsListAPI(@Body RestaurantItemsResponse dataRequestPOJO);


    @POST(AppConstants.SUB_CATEGORIES)
    Call<SubCategoriesResponse> subCategoriesListAPI(@Body SubCategoriesResponse dataRequestPOJO);


    @POST(AppConstants.SAVE_ADDRESS)
    Call<CommonResponse> saveAddressAPI(@Body SaveAddressResponse dataRequestPOJO);

    @POST(AppConstants.ADDRESS_LIST)
    Call<AddressListResponse> userAddressAPI(@Body AddressListResponse dataRequestPOJO);


    @POST(AppConstants.PLACE_GROCERY_ORDER)
    Call<CommonResponse> placeGroceryOrderAPI(@Body SaveOrderResponse orderResponse);

    @POST(AppConstants.PLACE_FOOD_ORDER)
    Call<CommonResponse> placeFoodOrderAPI(@Body SaveOrderResponse orderResponse);

    @POST(AppConstants.POSTAL_AVAILABILITY)
    Call<CommonResponse> postalCodeAvailability(@Body CommonResponse orderResponse);

    @POST(AppConstants.MY_ORDERS)
    Call<MyOrdersPOJO> myOrdersAPI(@Body MyOrdersPOJO orderResponse);

    @POST(AppConstants.MY_FOOD_ORDERS)
    Call<FoodMyOrdersPOJO> myFoodOrdersAPI(@Body FoodMyOrdersPOJO orderResponse);

    @POST(AppConstants.CANCEL_FOOD_ORDER)
    Call<CancelOrderResponse> cancelFoodOrderAPI(@Body CancelOrderResponse response);

    @POST(AppConstants.CANCEL_GROCERY_ORDER)
    Call<CancelOrderResponse> cancelGroceryOrderAPI(@Body CancelOrderResponse response);


    @POST(AppConstants.POSTAL_CODE_SEARCH)
    Call<PostalCodeSearchResponse> postalCodeSearch(@Body PostalCodeSearchResponse response);


    @POST(AppConstants.RAIN_ALERT)
    Call<AlertResponse> instructionsAPI(@Body AlertResponse response);


    @POST(AppConstants.FRAGMENT_Get_CONTACT_US)
    Call<Get_ContactData> GetContactsAPI(@Body Get_ContactData response);
}
